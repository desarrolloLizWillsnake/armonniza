/**
 * Created by Lizbeth on 27/08/15.
 */
$("#actualizar_info").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        url: '/administrador/insertar_datos_empresa',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_actualizacion").html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

function submitFile() {

    var formData = new FormData($('#imagen_archivo')[0]);
    $(":input").each(function() {
        formData.append($(this).attr("name"), $(this).val());
    });
    $.ajax({
            url: "/administrador/do_upload",
            type: 'POST',
            dataType: 'json',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function(data, textSatus, jqXHR){
                //console.log(data);
                //console.log(textSatus);
                //console.log(jqXHR);
                if(data.status == 200) {
                    $('#subirArchivo').modal('hide');
                } else {
                    $('#mensaje_resultado_imagen').html(data.mensaje);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                //console.log(jqXHR);
                //console.log(textStatus);
                //console.log(errorThrown);
                $('#mensaje_resultado_imagen').html(jqXHR.responseText);
            }
        });
}

$(document).ready(function() {
    $('#refresh').click(function() {
        // Recargo la p�gina
        location.reload();
    });
});

