$(document).ready(function() {

    var tipo = $("#tipo_precompromiso").text();
    if(tipo == "Fondo Revolvente" || tipo == "Gastos a Comprobar") {
        habilitar_fondos();
    } else if(tipo == "Viáticos Nacionales" || tipo == "Viáticos Internacionales") {
        habilitar_fondos();
        hablitar_viaticos();
    } else {
        habilitar_precompromiso();
    }

    $('#tabla_datos_precompromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs":
            [
                {
                    "targets": [ 0, 15, 16 ],
                    "visible": false,
                    "searchable": false
                }
            ]
    });

    $('#tabla_datos_viaticos_precompromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  [0, -1] ,
            "visible": false,
            "searchable": false
        }]
    });

    refrescarDetalle();

    refrescarDetalleViaticos();

});

function refrescarDetalle() {
    $.ajax({
        url: '/ciclo/tabla_detalle_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
            precompromiso: $("#ultimo_pre").val()
        },
        success: function(s){
            $('#tabla_datos_precompromiso').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][13]);
                $('#tabla_datos_precompromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    "$"+$.number( s[i][10], 2 ),
                    "$"+$.number( s[i][11], 2 ),
                    "$"+$.number( s[i][12], 2 ),
                    "$"+$.number( s[i][13], 2 ),
                    s[i][14],
                    s[i][15],
                    s[i][16]
                ]);
            } // End For

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#total_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

function refrescarDetalleViaticos() {
    $.ajax({
        url: '/ciclo/tabla_detalle_viaticos_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
            precompromiso: $("#ultimo_pre").val()
        },
        success: function(s){
            $('#tabla_datos_viaticos_precompromiso').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][6]);
                $('#tabla_datos_viaticos_precompromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    "$"+$.number( s[i][5], 2 ),
                    "$"+$.number( s[i][6], 2 ),
                    s[i][7]
                ]);
            } // End For

            $("#suma_total_viaticos").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#total_viaticos_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

function habilitar_precompromiso() {
    $( ".forma_precompromis_dato" ).show();
    $( ".forma_fondo" ).hide();
    $( ".forma_viaticos" ).hide();
}

function habilitar_fondos() {
    $( ".forma_precompromis_dato" ).hide();
    $( ".forma_fondo" ).show();
    $( ".forma_viaticos" ).hide();
}

function hablitar_viaticos() {
    $( ".forma_precompromis_dato" ).hide();
    $( ".forma_fondo" ).show();
    $( ".forma_viaticos" ).show();
}