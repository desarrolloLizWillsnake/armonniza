/**
 * Created by Daniel on 17-Dec-14.
 */
var tipo_precompromiso;
var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "ajax": "/ciclo/tabla_indice_AJAX",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });
    $('.datos_tabla_aprobado').dataTable({
        "ajax": "/ciclo/tabla_indice_Aprobado",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });
    $('.datos_tabla_pendiente').dataTable({
        "ajax": "/ciclo/tabla_indice_Pendiente",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });
    $('.datos_tabla_autorizar').dataTable({
        "ajax": "/ciclo/tabla_indice_Autorizar",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });
    $('.datos_tabla_cancelado').dataTable({
        "ajax": "/ciclo/tabla_indice_Cancelado",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });
    $('.datos_tabla_terminado').dataTable({
        "ajax": "/ciclo/tabla_indice_Terminado",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         'Buscar ',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
});

$('.datos_tabla_aprobado tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla_aprobado').DataTable().row( this ).data();
});

$('.datos_tabla_pendiente tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla_pendiente').DataTable().row( this ).data();
});

$('.datos_tabla_autorizar tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla_autorizar').DataTable().row( this ).data();
});

$('.datos_tabla_terminado tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla_terminado').DataTable().row( this ).data();
});

$('.datos_tabla_cancelado tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla_cancelado').DataTable().row( this ).data();
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#cancelar_precompromiso').val(datos_tabla[0]);
});

$('.modal_solicitar_terminar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#solicitar_terminar_hidden').val(datos_tabla[0]);
});

$('.modal_terminar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#terminar_hidden').val(datos_tabla[0]);
});

$("#elegir_cancelar_precompromiso").click(function() {
    var precompromiso = $("#cancelar_precompromiso").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/cancelar_precompro_caratula",
        data: {
            precompromiso: precompromiso
        }
    })
        .done(function( data ) {
            $("#resultado_borrar").html(data.mensaje);
            $('#modal_resultado_borrar').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#elegir_solicitar_terminar").click(function() {
    var precompromiso = $("#solicitar_terminar_hidden").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/terminar_precompromiso",
        data: {
            precompromiso: precompromiso
        }
    })
        .done(function( data ) {
            $('#mensaje_resultado').html(data.mensaje);
            $('.resultado').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#elegir_terminar").click(function() {
    var precompromiso = $("#terminar_hidden").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/terminar_precompromiso",
        data: {
            precompromiso: precompromiso
        }
    })
        .done(function( data ) {
            $('#mensaje_resultado').html(data.mensaje);
            $('.resultado').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

function BtnTodas() {
    document.getElementById('tab-todas').style.display='block';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-autorizar').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('tab-terminado').style.display='none';
    document.getElementById('btn-todas').className='active';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-autorizar').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
    document.getElementById('btn-terminado').className='noactive';
}
function BtnAprobado() {
    document.getElementById('tab-aprobado').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-autorizar').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('tab-terminado').style.display='none';
    document.getElementById('btn-aprobado').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-autorizar').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
    document.getElementById('btn-terminado').className='noactive';
}
function BtnPendiente() {
    document.getElementById('tab-pendiente').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-autorizar').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('tab-terminado').style.display='none';
    document.getElementById('btn-pendiente').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-autorizar').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
    document.getElementById('btn-terminado').className='noactive';
}
function BtnAutorizar() {
    document.getElementById('tab-autorizar').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('tab-terminado').style.display='none';
    document.getElementById('btn-autorizar').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
    document.getElementById('btn-terminado').className='noactive';
}
function BtnCancelado() {
    document.getElementById('tab-cancelado').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-autorizar').style.display='none';
    document.getElementById('tab-terminado').style.display='none';
    document.getElementById('btn-cancelado').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-autorizar').className='noactive';
    document.getElementById('btn-terminado').className='noactive';
}
function BtnTerminado() {
    document.getElementById('tab-terminado').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-autorizar').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('btn-terminado').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-autorizar').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
}
