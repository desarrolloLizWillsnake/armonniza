/**
 * Created by Lizbeth on 29/07/15.
 */
//Fecha
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$(document).ready(function(){

    $( "#fecha_inicial" ).val(curr_year + "-01-01" );
    $( "#fecha_final" ).val(curr_year + "-12-31");
    $('#tabla_cuenta_inicial').dataTable({
        "ajax": "contabilidad/tabla_cuentas_inicial",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

});

$('#tabla_cuenta_inicial tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta = $('#tabla_cuenta_inicial').DataTable().row( this ).data();
});

$('#modal_cuenta_inicial').on('hidden.bs.modal', function () {
    $("#cuenta_inicial").val(datos_tabla_cuenta[0]);
});

$("#consultar_reporte_auxiliar").on( "click", function() {

    $(".datos_auxiliar").validate({
        focusInvalid: true,
        rules: {
            fecha_inicial: {
                required: true
            },
            fecha_final: {
                required: true
            },
            cuenta_inicial: {
                required: true
            },
            nombre_cuenta: {
                required: true
            }
        },
        messages: {
            fecha_inicial: {
                required: "Formato Correcto AAAA-MM-DD"
            },
            fecha_final: {
                required: "* Este campo es obligatorio"
            },
            cuenta_inicial: {
                required: "* Este campo es obligatorio"
            },
            nombre_cuenta: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".datos_auxiliar").valid()){
        var map = {};
        $(":input").each(function() {
            map[$(this).attr("name")] = $(this).val();
        });

        $.ajax({
            url: '../contabilidad/imprimir_reporte_auxiliar',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function(s){
                    $("#fecha_inicial").val('');
                    $("#fecha_final").val('');
                    $("#cuenta_inicial").val('');
                    $("#nombre_cuenta").val('');
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }

});

