var nivel1;
var nivel2;
var nivel3;
var nivel4;
var nivel5;
var nivel6;

var nivel_buscar1;
var nivel_buscar2;
var nivel_buscar3;
var nivel_buscar4;
var nivel_buscar5;
var nivel_buscar6;
// Create a Bar Chart with Morris
var chart = Morris.Bar({
            element: 'grafica_partida',
            data: [ ],
            title: {
                text: ''
            },
            xkey: 'y',
            ykeys: ['a'],
            barColors: ['#B6CE33'],
            hideHover: 'auto',
            labels: ["Dinero"],
            hoverCallback: function (index, options, content, row) {
                return row.y + " $" + $.number( row.a, 2 );
            },
            resize: true
        });

$("#forma_nivel1").click(function(){
    $("#buscar_nivel3").prop('disabled', true);
    $( "#select_nivel3" ).html( '' );
    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel1 option:selected').each(function(){
        nivel1 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel2",
                type: "POST",
                data: { nivel1: nivel1 },
                success:function(result) {
                    $("#input_nivel1").val(nivel1);
                    $("#buscar_nivel2").prop('disabled', false);
                    $( "#select_nivel2" ).html( '' );
                    $( "#select_nivel2" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel1").keyup(function(){
    nivel_buscar1 = $('#buscar_nivel1').val();
    $.ajax( {
            url: "/egresos/buscar_nivel1",
            type: "POST",
            data: { nivel_buscar1: nivel_buscar1  },
            success:function(result) {
                $( "#select_nivel1" ).html( '' );
                $( "#select_nivel1" ).append( result );
            }
        }
    );
});

$("#forma_nivel2").click(function(){

    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel2 option:selected').each(function(){
        nivel2 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel3",
                type: "POST",
                data: { nivel1: nivel1, nivel2: nivel2 },
                success: function(result) {
                    $("#input_nivel2").val(nivel2);
                    $("#buscar_nivel3").prop('disabled', false);
                    $( "#select_nivel3" ).html( '' );
                    $( "#select_nivel3" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel2").keyup(function(){
    nivel_buscar2 = $('#buscar_nivel2').val();
    $.ajax( {
            url: "/egresos/buscar_nivel2",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel_buscar2
            },
            success:function(result) {
                $( "#select_nivel2" ).html( '' );
                $( "#select_nivel2" ).append( result );
            }
        }
    );
});

$("#forma_nivel3").click(function(){

    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );
    $("#buscar_nivel6").prop('disabled', true);
    $( "#select_nivel6" ).html( '' );

    $('#forma_nivel3 option:selected').each(function(){
        nivel3 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel4",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3
                },
                success: function(result) {
                    $("#input_nivel3").val(nivel3);
                    $("#buscar_nivel4").prop('disabled', false);
                    $( "#select_nivel4" ).html( '' );
                    $( "#select_nivel4" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel3").keyup(function(){
    nivel_buscar3 = $('#buscar_nivel3').val();
    $.ajax( {
            url: "/egresos/buscar_nivel3",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel_buscar3
            },
            success:function(result) {
                $( "#select_nivel3" ).html( '' );
                $( "#select_nivel3" ).append( result );
            }
        }
    );
});

$("#forma_nivel4").click(function(){
    $("#buscar__nivel6").prop('disabled', true);
    $( "#select__nivel6" ).html( '' );

    $('#forma_nivel4 option:selected').each(function(){
        nivel4 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel5",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4
                },
                success: function(result) {
                    $("#input_nivel4").val(nivel4);
                    $("#buscar_nivel5").prop('disabled', false);
                    $( "#select_nivel5" ).html( '' );
                    $( "#select_nivel5" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel4").keyup(function(){
    nivel_buscar4 = $('#buscar_nivel4').val();
    $.ajax( {
            url: "/egresos/buscar_nivel4",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel_buscar4
            },
            success:function(result) {
                $( "#select_nivel4" ).html( '' );
                $( "#select_nivel4" ).append( result );
            }
        }
    );
});

$("#forma_nivel5").click(function(){
    $('#forma_nivel5 option:selected').each(function(){
        nivel5 = $(this).val();
        $.ajax( {
                url: "/egresos/obtener_nivel6",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4,
                    nivel5: nivel5
                },
                success: function(result) {
                    $("#input_nivel5").val(nivel5);
                    $("#buscar_nivel6").prop('disabled', false);
                    $( "#select_nivel6" ).html( '' );
                    $( "#select_nivel6" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel5").keyup(function(){
    nivel_buscar5 = $('#buscar_nivel5').val();
    $.ajax( {
            url: "/egresos/buscar_nivel5",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel_buscar5
            },
            success:function(result) {
                $( "#select_nivel5" ).html( '' );
                $( "#select_nivel5" ).append( result );
            }
        }
    );
});

//Esta funcion es para obtener los datos del ultimo nivel
$("#forma_nivel6").click(function(){
    $('#forma_nivel6 option:selected').each(function(){
        nivel6 = $(this).val();
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1;
        var curr_year = d.getFullYear();
        var fecha = curr_year + "-" + curr_month + "-" + curr_date;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/egresos/obtener_datosUltimoNivel",
            data: {
                fecha: fecha,
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4,
                nivel5: nivel5,
                nivel6: nivel6
            }
        })
            .done(function( data ) {
                $("#input_nivel6").val(nivel6);
                $("#elegir_ultimoNivel").prop('disabled', false);
                // When the response to the AJAX request comes back render the chart with new data
                $("#mes_grafica").html("<p>"+data.mes+"</p>");
                chart.setData(
                    [
                        { y: "Autorizado Total", a: data.total_anual },
                        { y: "por Ejercer", a: data.saldo },
                        { y: "Precomprometido del Mes", a: data.mes_precompromiso },
                        { y: "Comprometido del Mes", a: data.mes_compromiso }
                    ]);
            })
            .fail(function(e) {
                // If there is no communication between the server, show an error
                alert( "error occured" );
            });
    });

});

$("#buscar_nivel6").keyup(function(){
    nivel_buscar6 = $('#buscar_nivel6').val();
    $.ajax( {
            url: "/egresos/buscar_nivel6",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel5,
                nivel_buscar6: nivel_buscar6
            },
            success:function(result) {
                $( "#select_nivel6" ).html( '' );
                $( "#select_nivel6" ).append( result );
            }
        }
    );
});

$("#egresos_elegir_agregar_nivel").on( "change", function(){
    var nivel = $("#egresos_elegir_agregar_nivel option:selected").val();

    switch (nivel) {
        case "1":
            agregar_nivel1();
            break;
        case "2":
            agregar_nivel2();
            break;
        case "3":
            agregar_nivel3();
            break;
        case "4":
            agregar_nivel4();
            break;
        case "5":
            agregar_nivel5();
            break;
        case "6":
            agregar_nivel6();
            break;
    }


});

function agregar_nivel1() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#fuente_financiamiento_agregar_select").hide();
    $("#programa_financiamiento_agregar_select").hide();
    $("#centro_de_costos_agregar_select").hide();
    $("#capitulo_agregar_select").hide();
    $("#concepto_agregar_select").hide();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
            '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Fuente de Financiamiento" required />' +
            '<br />' +
            '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Fuente de Financiamiento" required />' +
            '<hr />' +
            '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel2() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#fuente_financiamiento_agregar_select").show();
    $("#programa_financiamiento_agregar_select").hide();
    $("#centro_de_costos_agregar_select").hide();
    $("#capitulo_agregar_select").hide();
    $("#concepto_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
            '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Programa de Financiamiento" required />' +
            '<br />' +
            '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Programa de Financiamiento" required />' +
            '<hr />' +
            '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel3() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#fuente_financiamiento_agregar_select").show();
    $("#programa_financiamiento_agregar_select").show();
    $("#centro_de_costos_agregar_select").hide();
    $("#capitulo_agregar_select").hide();
    $("#concepto_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
            '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Centro de Costos" required />' +
            '<br />' +
            '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Centro de Costos" required />' +
            '<hr />' +
            '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel4() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#fuente_financiamiento_agregar_select").show();
    $("#programa_financiamiento_agregar_select").show();
    $("#centro_de_costos_agregar_select").show();
    $("#capitulo_agregar_select").hide();
    $("#concepto_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
            '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Cap&oacute;tulo" required />' +
            '<br />' +
            '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Cap&oacute;tulo" required />' +
            '<hr />' +
            '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel5() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#fuente_financiamiento_agregar_select").show();
    $("#programa_financiamiento_agregar_select").show();
    $("#centro_de_costos_agregar_select").show();
    $("#capitulo_agregar_select").show();
    $("#concepto_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
            '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Concepto" required />' +
            '<br />' +
            '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Concepto" required />' +
            '<hr />' +
            '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel6() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#fuente_financiamiento_agregar_select").show();
    $("#programa_financiamiento_agregar_select").show();
    $("#centro_de_costos_agregar_select").show();
    $("#capitulo_agregar_select").show();
    $("#concepto_agregar_select").show();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
            '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Partida" required />' +
            '<br />' +
            '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Partida" required />' +
            '<hr />' +
            '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function tomar_nivel_1() {
    $( "#fuente_financiamiento_agregar_select" ).html('');

    $.ajax( {
            url: "/egresos/obtener_nivel1",
            type: "POST",
            success:function(result) {
                $( "#fuente_financiamiento_agregar_select" ).append( '<option value="">Elegir Fuente de Financiamiento</option>' );
                $( "#fuente_financiamiento_agregar_select" ).append( result );
            }
        }
    );
}

$("#fuente_financiamiento_agregar_select").on("change", function(){
    nivel1 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel2",
            type: "POST",
            data: { nivel1: nivel1 },
            success:function(result) {
                $( "#programa_financiamiento_agregar_select" ).html( '' );
                $( "#programa_financiamiento_agregar_select" ).append( '<option value="">Elegir Programa de Financiamiento</option>' );
                $( "#programa_financiamiento_agregar_select" ).append( result );
            }
        }
    );
});

$("#programa_financiamiento_agregar_select").on("change", function(){
    nivel1 = $('#fuente_financiamiento_agregar_select option:selected').val();
    nivel2 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel3",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2
            },
            success:function(result) {
                $( "#centro_de_costos_agregar_select" ).html( '' );
                $( "#centro_de_costos_agregar_select" ).append( '<option value="">Elegir Centro de Costos</option>' );
                $( "#centro_de_costos_agregar_select" ).append( result );
            }
        }
    );
});

$("#centro_de_costos_agregar_select").on("change", function(){
    nivel1 = $('#fuente_financiamiento_agregar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_agregar_select option:selected').val();
    nivel3 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel4",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3
            },
            success:function(result) {
                $( "#capitulo_agregar_select" ).html( '' );
                $( "#capitulo_agregar_select" ).append( '<option value="">Elegir Cap�tulo</option>' );
                $( "#capitulo_agregar_select" ).append( result );
            }
        }
    );
});

$("#capitulo_agregar_select").on("change", function(){
    nivel1 = $('#fuente_financiamiento_agregar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_agregar_select option:selected').val();
    nivel3 = $('#centro_de_costos_agregar_select option:selected').val();
    nivel4 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel5",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4
            },
            success:function(result) {
                $( "#concepto_agregar_select" ).html( '' );
                $( "#concepto_agregar_select" ).append( '<option value="">Elegir Concepto</option>' );
                $( "#concepto_agregar_select" ).append( result );
            }
        }
    );
});

$("#form_agregar_partida").on("submit", function(e) {
    e.preventDefault();
    var map = {};
    var nivel = 1;

    $("#form_agregar_partida select").each(function () {
        var valor = "";
        if ($(this).val() == null) {
            valor = "";
        } else {
            valor = $(this).val();
        }
        map["nivel" + nivel] = valor;
        nivel += 1;
    });
    map["clave"] = $("#clave").val();
    map["descripcion"] = $("#descripcion").val();

    $.ajax( {
            url: "/egresos/agregar_nivel",
            type: "POST",
            dataType: 'json',
            data: map,
            success:function(result) {
                $("#egresos_modal_agregar_contenido").append(result.mensaje);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        }
    );

});

$("#egresos_elegir_editar_nivel").on( "change", function(){
    var nivel = $("#egresos_elegir_editar_nivel option:selected").val();

    switch (nivel) {
        case "1":
            editar_nivel1();
            break;
        case "2":
            editar_nivel2();
            break;
        case "3":
            editar_nivel3();
            break;
        case "4":
            editar_nivel4();
            break;
        case "5":
            editar_nivel5();
            break;
        case "6":
            editar_nivel6();
            break;
    }


});

function editar_nivel1() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").hide();
    $("#centro_de_costos_editar_select").hide();
    $("#capitulo_editar_select").hide();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Fuente de Financiamiento" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Fuente de Financiamiento" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel2() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").hide();
    $("#capitulo_editar_select").hide();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Programa de Financiamiento" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Programa de Financiamiento" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel3() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").hide();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Centro de Costos" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Centro de Costos" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel4() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").show();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Cap&oacute;tulo" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Cap&oacute;tulo" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel5() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").show();
    $("#concepto_editar_select").show();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Concepto" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Concepto" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel6() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").show();
    $("#concepto_editar_select").show();
    $("#partida_editar_select").show();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Partida" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Partida" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function tomar_nivel_editar_1() {
    $( "#fuente_financiamiento_editar_select" ).html('');

    $.ajax( {
            url: "/egresos/obtener_nivel1",
            type: "POST",
            success:function(result) {
                $( "#fuente_financiamiento_editar_select" ).append( '<option value="">Elegir Fuente de Financiamiento</option>' );
                $( "#fuente_financiamiento_editar_select" ).append( result );
            }
        }
    );
}

$("#fuente_financiamiento_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#fuente_financiamiento_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel2",
            type: "POST",
            data: { nivel1: nivel1 },
            success:function(result) {
                $( "#programa_financiamiento_editar_select" ).html( '' );
                $( "#programa_financiamiento_editar_select" ).append( '<option value="">Elegir Programa de Financiamiento</option>' );
                $( "#programa_financiamiento_editar_select" ).append( result );
            }
        }
    );
});

$("#programa_financiamiento_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#programa_financiamiento_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel3",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2
            },
            success:function(result) {
                $( "#centro_de_costos_editar_select" ).html( '' );
                $( "#centro_de_costos_editar_select" ).append( '<option value="">Elegir Centro de Costos</option>' );
                $( "#centro_de_costos_editar_select" ).append( result );
            }
        }
    );
});

$("#centro_de_costos_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#centro_de_costos_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_editar_select option:selected').val();
    nivel3 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel4",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3
            },
            success:function(result) {
                $( "#capitulo_editar_select" ).html( '' );
                $( "#capitulo_editar_select" ).append( '<option value="">Elegir Cap�tulo</option>' );
                $( "#capitulo_editar_select" ).append( result );
            }
        }
    );
});

$("#capitulo_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#capitulo_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_editar_select option:selected').val();
    nivel3 = $('#centro_de_costos_editar_select option:selected').val();
    nivel4 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel5",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4
            },
            success:function(result) {
                $( "#concepto_editar_select" ).html( '' );
                $( "#concepto_editar_select" ).append( '<option value="">Elegir Concepto</option>' );
                $( "#concepto_editar_select" ).append( result );
            }
        }
    );
});

$("#concepto_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#concepto_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_editar_select option:selected').val();
    nivel3 = $('#centro_de_costos_editar_select option:selected').val();
    nivel4 = $('#capitulo_editar_select option:selected').val();
    nivel5 = $(this).val();
    $.ajax( {
            url: "/egresos/obtener_nivel6",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4,
                nivel5: nivel5
            },
            success:function(result) {
                $( "#partida_editar_select" ).html( '' );
                $( "#partida_editar_select" ).append( '<option value="">Elegir Partida</option>' );
                $( "#partida_editar_select" ).append( result );
            }
        }
    );
});

$("#partida_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#partida_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);
});

$("#form_editar_partida").on("submit", function(e) {
    e.preventDefault();
    var map = {};
    var nivel = 1;

    $("#form_editar_partida select").each(function () {
        var valor = "";
        if ($(this).val() == null) {
            valor = "";
        } else {
            valor = $(this).val();
        }
        map["nivel" + nivel] = valor;
        nivel += 1;
    });
    map["clave"] = $("#clave").val();
    map["descripcion"] = $("#descripcion").val();
    map["clave_anterior"] = $("#clave_anterior").val();
    map["descripcion_anterior"] = $("#descripcion_anterior").val();

    $.ajax( {
            url: "/egresos/editar_nivel",
            type: "POST",
            dataType: 'json',
            data: map,
            success:function(result) {
                $("#egresos_modal_editar_contenido").append(result.mensaje);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        }
    );

});