//Fecha
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
var curr_year = d.getFullYear();

//Se crean las cajas para la selección de fecha
$( "#fecha_inicial" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#fecha_final" ).datepicker({ dateFormat: 'yy-mm-dd' });
//Se crean las cajas para la selección de fecha
$( "#fecha_inicial_exportar" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#fecha_final_exportar" ).datepicker({ dateFormat: 'yy-mm-dd' });

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$(document).ready(function(){

    $( "#fecha_inicial" ).val(curr_year + "-01-01" );
    $( "#fecha_final" ).val(curr_year + "-12-31");
    $( "#fecha_inicial_exportar" ).val(curr_year + "-01-01" );
    $( "#fecha_final_exportar" ).val(curr_year + "-12-31");
    $('#tabla_subsidio').dataTable({
        "ajax": "tabla_subsidio",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_cuenta_inicial').dataTable({
        "ajax": "tabla_cuentas_inicial",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_cuenta_final').dataTable({
        "ajax": "tabla_cuentas_final",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_centro_costos').dataTable({
        "ajax": "tabla_centro_costos",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
});

$('#tabla_subsidio tbody').on( 'click', 'tr', function () {
    datos_tabla_subsidio = $('#tabla_subsidio').DataTable().row( this ).data();
});

$('#modal_subsidio').on('hidden.bs.modal', function () {
    $("#subsidio").val(datos_tabla_subsidio[1]);
});

$('#tabla_cuenta_inicial tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta = $('#tabla_cuenta_inicial').DataTable().row( this ).data();
});

$('#modal_cuenta_inicial').on('hidden.bs.modal', function () {
    $("#cuenta_inicial").val(datos_tabla_cuenta[0]);
    $("#nombre_cuenta").val(datos_tabla_cuenta[1]);
    $("#cuenta_inicial_exportar").val(datos_tabla_cuenta[0]);
    $("#nombre_cuenta_exportar").val(datos_tabla_cuenta[1]);
});

$('#tabla_cuenta_final tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta = $('#tabla_cuenta_final').DataTable().row( this ).data();
});

$('#modal_cuenta_final').on('hidden.bs.modal', function () {
    $("#cuenta_final").val(datos_tabla_cuenta[0]);
    $("#nombre_cuenta_final").val(datos_tabla_cuenta[1]);
    $("#cuenta_final_exportar").val(datos_tabla_cuenta[0]);
    $("#nombre_cuenta_final_exportar").val(datos_tabla_cuenta[1]);
});

$('#tabla_centro_costos tbody').on( 'click', 'tr', function () {
    datos_tabla_centro_costo = $('#tabla_centro_costos').DataTable().row( this ).data();
});

$('#modal_centro_costos').on('hidden.bs.modal', function () {
    $("#centro_costos").val(datos_tabla_centro_costo[0]);
});