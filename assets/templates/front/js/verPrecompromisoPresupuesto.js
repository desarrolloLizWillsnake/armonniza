var datos_tabla_precompromiso;
var total = 0;

$(document).ready(function() {

    $("#forma_transferencia").bind("keypress", function(e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    $('body').on('copy paste', 'input', function (e) { e.preventDefault(); });

    var tipo = $("#tipo_precompromiso").text();
    if(tipo == "Fondo Revolvente" || tipo == "Gastos a Comprobar") {
        habilitar_fondos();
    } else if(tipo == "Viáticos Nacionales" || tipo == "Viáticos Internacionales") {
        habilitar_fondos();
        hablitar_viaticos();
    } else {
        habilitar_precompromiso();
    }

    $('#tabla_datos_precompromiso').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/egresos/tabla_detalle_precompromiso",
            "type": "POST",
            "data": function ( d ) {
                d.precompromiso = $("#ultimo_pre").val();
            }
        },
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs":
            [
                {
                    "targets": [ 0, 15, 16 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ -1 ],
                    "searchable": false
                },
                {
                    "targets": [ 10 , 11, 12, 13  ],
                    "render": function ( data, type, row ) {
                        return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
                    }
                }
            ]
    });

    $('#tabla_datos_viaticos_precompromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  [0, -1] ,
            "visible": false,
            "searchable": false
        }]
    });

    refrescarDetalle();

    refrescarDetalleViaticos();

});

function refrescarDetalle() {

    var precompromiso = $("#ultimo_pre").val();

    $.ajax({
        url: '/egresos/sumatorias_detalle',
        dataType: "json",
        method: 'POST',
        data: {
            precompromiso: precompromiso
        },
        success: function(s){

            var total = parseFloat(s.importe);

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");


        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

function refrescarDetalleViaticos() {
    $.ajax({
        url: '/ciclo/tabla_detalle_viaticos_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
            precompromiso: $("#ultimo_pre").val()
        },
        success: function(s){
            $('#tabla_datos_viaticos_precompromiso').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][6]);
                $('#tabla_datos_viaticos_precompromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    "$"+$.number( s[i][5], 2 ),
                    "$"+$.number( s[i][6], 2 ),
                    s[i][7]
                ]);
            } // End For

            $("#suma_total_viaticos").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#total_viaticos_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

$('#tabla_datos_precompromiso tbody').on( 'click', 'tr', function () {

    total = 0.0;

    datos_tabla_precompromiso = $('#tabla_datos_precompromiso').DataTable().row( this ).data();
    var meses = JSON.parse(datos_tabla_precompromiso[16]);

    if("enero" in meses ) {
        if(meses.enero == "" || meses.enero == null) {
            meses.enero = 0.0;
        }
        $("#enero_consulta").val("$"+$.number( meses.enero, 2 ));
        total += parseFloat(meses.enero);
    }
    if("febrero" in meses ) {
        if(meses.febrero == "" || meses.febrero == null) {
            meses.febrero = 0.0;
        }
        $("#febrero_consulta").val("$"+$.number( meses.febrero, 2 ));
        total += parseFloat(meses.febrero);
    }
    if("marzo" in meses ) {
        if(meses.marzo == "" || meses.marzo == null) {
            meses.marzo = 0.0;
        }
        $("#marzo_consulta").val("$"+$.number( meses.marzo, 2 ));
        total += parseFloat(meses.marzo);
    }
    if("abril" in meses ) {
        if(meses.abril == "" || meses.abril == null) {
            meses.abril = 0.0;
        }
        $("#abril_consulta").val("$"+$.number( meses.abril, 2 ));
        total += parseFloat(meses.abril);
    }
    if("mayo" in meses ) {
        if(meses.mayo == "" || meses.mayo == null) {
            meses.mayo = 0.0;
        }
        $("#mayo_consulta").val("$"+$.number( meses.mayo, 2 ));
        total += parseFloat(meses.mayo);
    }
    if("junio" in meses ) {
        if(meses.junio == "" || meses.junio == null) {
            meses.junio = 0.0;
        }
        $("#junio_consulta").val("$"+$.number( meses.junio, 2 ));
        total += parseFloat(meses.junio);
    }
    if("julio" in meses ) {
        if(meses.julio == "" || meses.julio == null) {
            meses.julio = 0.0;
        }
        $("#julio_consulta").val("$"+$.number( meses.julio, 2 ));
        total += parseFloat(meses.julio);
    }
    if("agosto" in meses ) {
        if(meses.agosto == "" || meses.agosto == null) {
            meses.agosto = 0.0;
        }
        $("#agosto_consulta").val("$"+$.number( meses.agosto, 2 ));
        total += parseFloat(meses.agosto);
    }
    if("septiembre" in meses ) {
        if(meses.septiembre == "" || meses.septiembre == null) {
            meses.septiembre = 0.0;
        }
        $("#septiembre_consulta").val("$"+$.number( meses.septiembre, 2 ));
        total += parseFloat(meses.septiembre);
    }
    if("octubre" in meses ) {
        if(meses.octubre == "" || meses.octubre == null) {
            meses.octubre = 0.0;
        }
        $("#octubre_consulta").val("$"+$.number( meses.octubre, 2 ));
        total += parseFloat(meses.octubre);
    }
    if("noviembre" in meses ) {
        if(meses.noviembre == "" || meses.noviembre == null) {
            meses.noviembre = 0.0;
        }
        $("#noviembre_consulta").val("$"+$.number( meses.noviembre, 2 ));
        total += parseFloat(meses.noviembre);
    }
    if("diciembre" in meses ) {
        if(meses.diciembre == "" || meses.diciembre == null) {
            meses.diciembre = 0.0;
        }
        $("#diciembre_consulta").val("$"+$.number( meses.diciembre, 2 ));
        total += parseFloat(meses.diciembre);
    }

    $('#presupuesto_total_modal').html($.number( total, 2 ));
    $('#total_precomprometido').html("$"+$.number( total, 2 ));
    $('#total_transferencia').html("$"+$.number( 0, 2 ));
    $('#total').val(total);
    console.log(total);
    
    $('#id_detalle').val(datos_tabla_precompromiso[0]);
    $('#nivel1').val(datos_tabla_precompromiso[1]);
    $('#nivel2').val(datos_tabla_precompromiso[2]);
    $('#nivel3').val(datos_tabla_precompromiso[3]);
    $('#nivel4').val(datos_tabla_precompromiso[4]);
    $('#nivel5').val(datos_tabla_precompromiso[5]);
    $('#nivel6').val(datos_tabla_precompromiso[6]);

});

$("#forma_meses").on("click", function(e) {
    
    e.preventDefault();

    var table = $('#tabla_datos_precompromiso').DataTable();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.meses_iniciales = datos_tabla_precompromiso[16];

    $.ajax({
        url: '/egresos/actualizar_presupuesto_tomado',
        dataType: 'json',
        method: 'POST',
        data: map
    })
    .done(function( data ) {
        $("#resultado_actualizar_presupuesto").html(data.mensaje);
        $("#adecuacion").val(data.adecuacion);
        table.ajax.reload();
    })
    .fail(function(e) {
        console.log(e.responseText);
    });

});

$('.modal_editar').on('hide.bs.modal', function (e) {
    $("#enero").val(0);
    $("#febrero").val(0);
    $("#marzo").val(0);
    $("#abril").val(0);
    $("#mayo").val(0);
    $("#junio").val(0);
    $("#julio").val(0);
    $("#agosto").val(0);
    $("#septiembre").val(0);
    $("#octubre").val(0);
    $("#noviembre").val(0);
    $("#diciembre").val(0);
    total = 0;

});

function habilitar_precompromiso() {
    $( ".forma_precompromis_dato" ).show();
    $( ".forma_fondo" ).hide();
    $( ".forma_viaticos" ).hide();
}

function habilitar_fondos() {
    $( ".forma_precompromis_dato" ).hide();
    $( ".forma_fondo" ).show();
    $( ".forma_viaticos" ).hide();
}

function hablitar_viaticos() {
    $( ".forma_precompromis_dato" ).hide();
    $( ".forma_fondo" ).show();
    $( ".forma_viaticos" ).show();
}

var app = angular.module('precompromisoApp', []);
app.controller('precompromisoControl', function($scope) {
    $scope.total_calculo = function() {
        var total_calculo = parseFloat($scope.enero || 0) + parseFloat($scope.febrero || 0) +
                            parseFloat($scope.marzo || 0) + parseFloat($scope.abril || 0) + 
                            parseFloat($scope.mayo || 0) + parseFloat($scope.junio || 0) +
                            parseFloat($scope.julio || 0) + parseFloat($scope.agosto || 0) +
                            parseFloat($scope.septiembre || 0) + parseFloat($scope.octubre || 0) +
                            parseFloat($scope.noviembre || 0) + parseFloat($scope.diciembre || 0);

        if(total_calculo.toFixed(2) !== total.toFixed(2)) {
            $( "#pie_modal" ).hide();
        } else {
            $( "#pie_modal" ).show();
        }

        return total_calculo || 0;
    };

    $('.modal_editar').on('hide.bs.modal', function (e) {
        $scope.enero = 0;
        $scope.febrero = 0;
        $scope.marzo = 0;
        $scope.abril = 0;
        $scope.mayo = 0;
        $scope.junio = 0;
        $scope.julio = 0;
        $scope.agosto = 0;
        $scope.septiembre = 0;
        $scope.octubre = 0;
        $scope.noviembre = 0;
        $scope.diciembre = 0;
        total_calculo = 0;
    });
});