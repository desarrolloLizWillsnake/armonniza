$(document).ready(function() {

    $('#tabla_datos_reduccion').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/egresos/tabla_detalle_ampliacion",
            "type": "POST",
            "data": function ( d ) {
                d.ampliacion = $("#ultimo").val();
            }
        },
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs":
            [
                {
                    "targets": [ 0, -1 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 7  ],
                    "render": function ( data, type, row ) {
                        return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
                    }
                },
                {
                    "targets": [ 8  ],
                    "render": function ( data, type, row ) {
                        return data.capitalize();
                    }
                }
            ]
    });

    refrescarDetalle();

});

function refrescarDetalle() {

    var table = $('#tabla_datos_reduccion').DataTable();

    $.ajax({
        url: '/egresos/sumatorias_detalle_adecuacion',
        dataType: 'json',
        method: 'POST',
        data: {
            adecuacion: $("#ultimo").val()
        },
        success: function(s){
            $("#total_hidden").val(s.total);
            table.ajax.reload();
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}