/**
 * Created by Lizbeth on 21/10/2015.
 */
var datos_tabla_detalle;

$(document).ready(function() {
    $('#tabla_detalle').dataTable({
        "ajax": "../tabla_detalle_nota_entrada",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 , -1 ],
            "visible": false,
            "searchable": false
        }]
    });

    refrescarDetalle();

});

function refrescarDetalle() {
    $.ajax({
        url: '/patrimonio/tabla_detalle_nota_entrada',
        dataType: 'json',
        method: 'POST',
        data: {
            nota: $('#ultima_nota').val()
        },
        success: function(s){
            $('#tabla_detalle').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][12]);
                $('#tabla_detalle').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    "$"+$.number( s[i][10], 2 ),
                    "$"+$.number( s[i][11], 2 ),
                    "$"+$.number( s[i][12], 2 ),
                    s[i][13],
                    s[i][14],
                    s[i][15]
                ]);
            } // End For

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");
            $('#importe_total').val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}