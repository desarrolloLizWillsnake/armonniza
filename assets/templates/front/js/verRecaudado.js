/**
 * Created by Lizbeth on 29/04/2015.
 */
$(document).ready(function() {
    $('#tabla_datos_recaudado').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs":
            [
                {
                    "targets": [ 0, -1 ],
                    "visible": false,
                    "searchable": false
                }
            ]
    });
    refrescarDetalle();
});

function refrescarDetalle() {
    $.ajax({
        url: '/recaudacion/tabla_detalle_recaudado',
        dataType: 'json',
        method: 'POST',
        data: {
            recaudado: $("#ultimo").val()
        },
        success: function(s){
            $('#tabla_datos_recaudado').dataTable().fnClearTable();
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                total += parseFloat(s[i][11]);
                $('#tabla_datos_recaudado').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][9], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][10], 2 )+"</div>",
                    "<div class='table-formant-sign'>$"+"</div><div class='table-formant-coin'>"+$.number( s[i][11], 2 )+"</div>",
                    s[i][12],
                ]);
            } // End For

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ "+$.number( total, 2 )+ "</span>");

            $("#total_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}
