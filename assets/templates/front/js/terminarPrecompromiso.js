$(document).ready(function() {

    $('#tabla_precompromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_compromiso').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    tabla_Precompromiso();

    tabla_Compromiso();

});

$("#elegir_terminar").click(function() {
    var precompromiso = $("#precompromiso").val();

    $("#resultado_terminar").html('<img src="../../img/ajax-loader.gif" style="width: 50px;" />');

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/elegir_terminar_precompromiso",
        data: {
            precompromiso: precompromiso
        }
    })
        .done(function( data ) {
            $("#resultado_terminar").html(data.mensaje);
            $("#terminar_precompromiso").prop('disabled', true);
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

function tabla_Precompromiso() {
    $.ajax({
        url: '/ciclo/tabla_terminar_precompromiso',
        dataType: 'json',
        method: 'POST',
        data: {
            precompromiso: $("#precompromiso").val()
        },
        success: function(s){
            $('#tabla_precompromiso').dataTable().fnClearTable();
            for(var i = 0; i < s.length; i++) {
                $('#tabla_precompromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    "$"+$.number( s[i][6], 2 )
                ]);
            } // End For
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

function tabla_Compromiso() {
    $.ajax({
        url: '/ciclo/tabla_terminar_compromiso',
        dataType: 'json',
        method: 'POST',
        data: {
            precompromiso: $("#precompromiso").val()
        },
        success: function(s){
            $('#tabla_compromiso').dataTable().fnClearTable();
            for(var i = 0; i < s.length; i++) {
                $('#tabla_compromiso').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    "$"+$.number( s[i][8], 2 )
                ]);
            } // End For
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}