var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "/prueba/tabla_proveedores_prueba",
        table: "#example",
        fields: [ {
            label: "Nombre:",
            name:  "nombre"
        }, {
            label: "Mypyme:",
            name:  "mypyme"
        }, {
            label: "Pago:",
            name:  "pago"
        }
        ]
    } );

    // Activate the bubble editor on click of a table cell
    $('#example').on( 'click', 'tbody td', function (e) {
        editor.bubble( this );
    } );

    $('#example').DataTable( {
        dom: "Tfrtip",
        ajax: "/prueba/tabla_proveedores_prueba",
        columns: [
            { data: "nombre" },
            { data: "pago" },
            {
                "class": "center",
                "data": "mypyme",
                "render": function (val, type, row) {
                    return val == 0 ? "No" : "Si";
                }
            }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        }
    } );
} );