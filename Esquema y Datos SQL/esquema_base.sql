-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema software
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `software` ;

-- -----------------------------------------------------
-- Schema software
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `software` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `software` ;

-- -----------------------------------------------------
-- Table `software`.`ci_sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`ci_sessions` ;

CREATE TABLE IF NOT EXISTS `software`.`ci_sessions` (
  `session_id` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT '0',
  `ip_address` VARCHAR(16) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT '0',
  `user_agent` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `last_activity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  PRIMARY KEY (`session_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `software`.`login_attempts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`login_attempts` ;

CREATE TABLE IF NOT EXISTS `software`.`login_attempts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ip_address` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `login` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `software`.`user_autologin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`user_autologin` ;

CREATE TABLE IF NOT EXISTS `software`.`user_autologin` (
  `key_id` CHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `user_id` INT(11) NOT NULL DEFAULT '0',
  `user_agent` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `last_ip` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `last_login` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`, `user_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `software`.`user_profiles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`user_profiles` ;

CREATE TABLE IF NOT EXISTS `software`.`user_profiles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `country` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `website` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `software`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`users` ;

CREATE TABLE IF NOT EXISTS `software`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `password` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `activated` TINYINT(1) NOT NULL DEFAULT '1',
  `banned` TINYINT(1) NOT NULL DEFAULT '0',
  `ban_reason` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `new_password_key` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `new_password_requested` DATETIME NULL DEFAULT NULL,
  `new_email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `new_email_key` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `last_ip` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `last_login` DATETIME NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `software`.`cat_clasificador_administrativo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_clasificador_administrativo` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_clasificador_administrativo` (
  `id_clasificador_administrativo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_clasificador_administrativo`),
  UNIQUE INDEX `id_clasificador_administrativo_UNIQUE` (`id_clasificador_administrativo` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_clasificador_capitulos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_clasificador_capitulos` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_clasificador_capitulos` (
  `id_clasificador_capitulos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_clasificador_capitulos`),
  UNIQUE INDEX `id_clasificador_administrativo_UNIQUE` (`id_clasificador_capitulos` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_clasificador_fuentes_financia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_clasificador_fuentes_financia` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_clasificador_fuentes_financia` (
  `id_clasificador_fuentes_financia` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_clasificador_fuentes_financia`),
  UNIQUE INDEX `id_clasificador_administrativo_UNIQUE` (`id_clasificador_fuentes_financia` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_clasificador_objetos_gasto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_clasificador_objetos_gasto` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_clasificador_objetos_gasto` (
  `id_clasificador_objetos_gasto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `capitulo` VARCHAR(10) NOT NULL,
  `concepto` VARCHAR(10) NOT NULL,
  `rubro` VARCHAR(10) NULL,
  `clave` VARCHAR(20) NULL,
  `descripcion` TEXT NULL,
  `cuenta` VARCHAR(20) NULL,
  `subcuenta` VARCHAR(15) NULL,
  `auxiliar` VARCHAR(15) NULL,
  `clasificacion` TEXT NULL,
  `aplicacion` VARCHAR(20) NULL,
  `restriccion` VARCHAR(30) NULL,
  `clavescian` VARCHAR(10) NULL,
  `amplia` TEXT NULL,
  PRIMARY KEY (`id_clasificador_objetos_gasto`),
  UNIQUE INDEX `id_clasificador_administrativo_UNIQUE` (`id_clasificador_objetos_gasto` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_clasificador_tipo_gasto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_clasificador_tipo_gasto` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_clasificador_tipo_gasto` (
  `id_clasificador_tipo_gasto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(45) NOT NULL,
  `titulo` VARCHAR(100) NOT NULL,
  `tipo` VARCHAR(100) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_clasificador_tipo_gasto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_plan_cuentas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_plan_cuentas` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_plan_cuentas` (
  `id_plan_cuentas` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(45) NOT NULL,
  `tipo` VARCHAR(1) NULL,
  `cargo` FLOAT NULL,
  `abono` FLOAT NULL,
  `saldo` FLOAT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_plan_cuentas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_unidad_medida`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_unidad_medida` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_unidad_medida` (
  `id_unidad_medida` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_unidad_medida`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_tipo_clase`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_tipo_clase` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_tipo_clase` (
  `id_tipo_clase` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(45) NOT NULL,
  `titulo` VARCHAR(100) NOT NULL,
  `tipo` VARCHAR(100) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_tipo_clase`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_egresos_elementos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_egresos_elementos` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_egresos_elementos` (
  `id_egresos_elementos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nivel` INT NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_egresos_elementos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_rubros_ingreso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_rubros_ingreso` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_rubros_ingreso` (
  `id_rubros_ingreso` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(45) NOT NULL,
  `titulo` VARCHAR(100) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_rubros_ingreso`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_ingresos_elementos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_ingresos_elementos` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_ingresos_elementos` (
  `id_ingresos_elementos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nivel` INT NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_ingresos_elementos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_maestro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_maestro` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_maestro` (
  `id_maestro` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(50) NOT NULL,
  `titulo` TEXT NOT NULL,
  `tipo` VARCHAR(100) NOT NULL,
  `corto1` TINYINT NULL,
  `corto2` TINYINT NULL,
  `corto3` TINYINT NULL,
  `corto4` TINYINT NULL,
  `corto5` TINYINT NULL,
  `importe1` DOUBLE(11,4) NULL,
  `importe2` DOUBLE(11,4) NULL,
  `importe3` DOUBLE(11,4) NULL,
  `importe4` DOUBLE(11,4) NULL,
  `importe5` DOUBLE(11,4) NULL,
  `campo1` VARCHAR(100) NULL,
  `campo2` VARCHAR(100) NULL,
  `campo3` VARCHAR(100) NULL,
  `campo4` VARCHAR(100) NULL,
  `campo5` VARCHAR(100) NULL,
  `descripcion` TEXT NULL,
  PRIMARY KEY (`id_maestro`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_programas_presupuesto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_programas_presupuesto` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_programas_presupuesto` (
  `id_programas_presupuesto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(45) NOT NULL,
  `titulo` VARCHAR(100) NOT NULL,
  `tipo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_programas_presupuesto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_tipo_mov_bancario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_tipo_mov_bancario` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_tipo_mov_bancario` (
  `id_tipo_mov_bancario` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(50) NOT NULL,
  `descripcion` TEXT NOT NULL,
  PRIMARY KEY (`id_tipo_mov_bancario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_niveles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_niveles` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_niveles` (
  `id_niveles` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nivel` BLOB NOT NULL,
  PRIMARY KEY (`id_niveles`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_precompromiso_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_precompromiso_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_precompromiso_caratula` (
  `id_precompromiso_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_pre` INT NULL,
  `tipo_requisicion` VARCHAR(50) NULL,
  `tipo_gasto` INT NULL DEFAULT 1,
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `firma1` TINYINT(1) NULL DEFAULT 0,
  `firma2` TINYINT(1) NULL DEFAULT 0,
  `firma3` TINYINT(1) NULL DEFAULT 0,
  `fecha_emision` DATE NULL,
  `fecha_autoriza` DATE NULL,
  `fecha_programada` DATE NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `total` DOUBLE(11,4) NULL DEFAULT 0,
  `total_compromiso` DOUBLE(11,4) NULL DEFAULT 0,
  `total_contrarecibo` DOUBLE(11,4) NULL DEFAULT 0,
  `total_pagado` DOUBLE(11,4) NULL DEFAULT 0,
  `estatus` VARCHAR(100) NULL DEFAULT 'espera',
  `usada` TINYINT(1) NULL DEFAULT 0,
  `cancelada` TINYINT(1) NULL DEFAULT 0,
  `fecha_cancelada` DATE NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  `descripcion` TEXT NULL,
  `ley_aplicable` VARCHAR(30) NULL,
  `articulo_procedencia` VARCHAR(30) NULL,
  `lugar_entrega` TEXT NULL,
  `plazo_numero` INT NULL,
  `plazo_tipo` VARCHAR(5) NULL,
  `plurianualidad` VARCHAR(3) NULL,
  `tipo_garantia` VARCHAR(100) NULL,
  `porciento_garantia` VARCHAR(15) NULL,
  `anexos` TINYINT(1) NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `proveedor` TEXT NULL,
  `importe_responsabilidad_civil` DOUBLE(11,4) NULL DEFAULT 0.0000,
  `lugar_adquisiciones` TEXT NULL,
  `existencia_almacen` TEXT NULL,
  `norma_inspeccion` TEXT NULL,
  `registros_sanitarios` TEXT NULL,
  `capacitacion` TEXT NULL,
  `npedido` VARCHAR(50) NULL,
  `hash` TEXT NULL,
  `id_persona` INT NULL,
  `nombre_completo` TEXT NULL,
  `puesto` TEXT NULL,
  `area` TEXT NULL,
  `no_empleado` VARCHAR(45) NULL,
  `clave` VARCHAR(45) NULL,
  `forma_pago` VARCHAR(45) NULL,
  `transporte` TEXT NULL,
  `grupo_jerarquico` TEXT NULL,
  `zona_marginada` TINYINT(1) NULL DEFAULT 0,
  `zona_mas_economica` TINYINT(1) NULL DEFAULT 0,
  `zona_menos_economica` TINYINT(1) NULL DEFAULT 0,
  `motivo_comision` TEXT NULL,
  PRIMARY KEY (`id_precompromiso_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_precompromiso_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_precompromiso_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_precompromiso_detalle` (
  `id_precompromiso_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_pre` INT UNSIGNED NOT NULL,
  `id_nivel` INT UNSIGNED NULL,
  `presupuesto_tomado` VARCHAR(10) NULL,
  `gasto` VARCHAR(50) NULL,
  `unidad_medida` VARCHAR(10) NULL,
  `cantidad` INT NULL,
  `p_unitario` DOUBLE(11,4) NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` VARCHAR(10) NULL,
  `importe` DOUBLE(11,4) NULL,
  `titulo` TEXT NULL,
  `year` VARCHAR(4) NULL,
  `especificaciones` TEXT NULL,
  `nivel` BLOB NULL,
  PRIMARY KEY (`id_precompromiso_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_conceptos_gasto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_conceptos_gasto` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_conceptos_gasto` (
  `id_conceptos_gasto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `articulo` VARCHAR(50) NULL,
  `codigo` VARCHAR(50) NULL,
  `clasificacion` VARCHAR(50) NULL,
  `patrimonio` VARCHAR(100) NULL,
  `descripcion` TEXT NULL,
  `localizacion` VARCHAR(50) NULL,
  `unidad` VARCHAR(8) NULL,
  `existencia` INT NULL,
  `promedio` DOUBLE(11,4) NULL,
  `acumulado` DOUBLE(11,4) NULL,
  `clave` VARCHAR(20) NULL,
  `estante` VARCHAR(20) NULL,
  `anaquel` VARCHAR(45) NULL,
  `afectacion` VARCHAR(100) NULL,
  `observaciones` TEXT NULL,
  PRIMARY KEY (`id_conceptos_gasto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_compromiso_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_compromiso_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_compromiso_caratula` (
  `id_compromiso_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_compromiso` INT NULL,
  `alterno` VARCHAR(100) NULL,
  `num_precompromiso` INT NULL DEFAULT NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `nombre_proveedor` TEXT NULL,
  `fecha_emision` DATE NULL,
  `fecha_autoriza` DATE NULL,
  `fecha_programada` DATE NULL,
  `no_solicitud` VARCHAR(15) NULL,
  `no_autorizacion` VARCHAR(15) NULL,
  `tipo_compra` INT NULL,
  `tipo_compromiso` VARCHAR(100) NULL,
  `licitacion` VARCHAR(20) NULL,
  `transporte` VARCHAR(10) NULL,
  `lugar_entrega` TEXT NULL,
  `condicion_entrega` TEXT NULL,
  `descripcion_compra` TEXT NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `total` DOUBLE(11,4) NULL,
  `estatus` VARCHAR(100) NULL DEFAULT 'espera',
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `firma1` TINYINT(1) NULL DEFAULT 0,
  `firma2` TINYINT(1) NULL DEFAULT 0,
  `firma3` TINYINT(1) NULL DEFAULT 0,
  `usado` TINYINT(1) NULL DEFAULT 0,
  `cancelada` TINYINT(1) NULL DEFAULT 0,
  `fecha_cancelada` DATE NULL DEFAULT NULL,
  `pagada` TINYINT(1) NULL DEFAULT 0,
  `tipo` VARCHAR(30) NULL,
  `numero` INT NULL,
  `tipo_gasto` INT NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  `descripcion_general` TEXT NULL,
  PRIMARY KEY (`id_compromiso_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_compromiso_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_compromiso_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_compromiso_detalle` (
  `id_compromiso_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_compromiso` INT NULL,
  `id_nivel` INT NULL,
  `presupuesto_tomado` VARCHAR(10) NULL,
  `gasto` VARCHAR(50) NULL,
  `unidad_medida` VARCHAR(10) NULL,
  `cantidad` INT NULL,
  `p_unitario` DOUBLE(11,4) NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `importe` DOUBLE(11,4) NULL,
  `titulo` TEXT NULL,
  `year` VARCHAR(4) NULL,
  `especificaciones` TEXT NULL,
  `nivel` BLOB NULL,
  PRIMARY KEY (`id_compromiso_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_proveedores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_proveedores` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_proveedores` (
  `id_proveedores` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `clave_prov` VARCHAR(45) NULL,
  `nombre` VARCHAR(150) NULL,
  `email` VARCHAR(100) NULL,
  `calle` VARCHAR(100) NULL,
  `telefono` VARCHAR(50) NULL,
  `no_exterior` VARCHAR(10) NULL,
  `no_interior` VARCHAR(10) NULL,
  `colonia` VARCHAR(150) NULL,
  `cp` VARCHAR(5) NULL,
  `ciudad` VARCHAR(100) NULL,
  `delegacion_municipio` VARCHAR(100) NULL,
  `pais` VARCHAR(25) NULL,
  `contacto` VARCHAR(60) NULL,
  `RFC` VARCHAR(15) NULL,
  `sucursal` VARCHAR(10) NULL,
  `cuenta` VARCHAR(20) NULL,
  `clabe` VARCHAR(30) NULL,
  `banco` VARCHAR(100) NULL,
  `pago` VARCHAR(10) NULL,
  `mypyme` TINYINT(1) NULL DEFAULT 0,
  `observaciones` TEXT NULL,
  `cat_proveedorescol` VARCHAR(45) NULL,
  `moneda` VARCHAR(10) NULL,
  PRIMARY KEY (`id_proveedores`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_contrarecibo_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_contrarecibo_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_contrarecibo_caratula` (
  `id_contrarecibo_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_compromiso` INT NULL,
  `alterno` VARCHAR(20) NULL,
  `fecha_emision` DATE NULL,
  `fecha_pago` DATE NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `proveedor` VARCHAR(255) NULL,
  `concepto` TEXT NULL,
  `importe` DOUBLE(11,4) NULL,
  `enfirme` TINYINT(1) NULL,
  `sifirme` TINYINT(1) NULL,
  `cancelada` TINYINT(1) NULL,
  `fecha_cancelada` DATE NULL,
  `pagada` TINYINT(1) NULL,
  `anticipado` DOUBLE(11,4) NULL,
  `tipo` VARCHAR(25) NULL,
  `numerot` INT NULL,
  `usado` TINYINT(1) NULL,
  `poliza` INT NULL DEFAULT 0,
  `estatus` VARCHAR(20) NULL,
  `destino` VARCHAR(45) NULL,
  `documento` TEXT NULL,
  `tipo_documento` VARCHAR(50) NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  `descripcion` TEXT NULL,
  PRIMARY KEY (`id_contrarecibo_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_contrarecibo_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_contrarecibo_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_contrarecibo_detalle` (
  `id_contrarecibo_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_contrarecibo` INT NULL,
  `numero_compromiso` INT NULL,
  `nombre` TEXT NULL,
  `tipo` TEXT NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `documento` INT NULL,
  `concepto` TEXT NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `total` DOUBLE(11,4) NULL,
  `descripcion` TEXT NULL,
  PRIMARY KEY (`id_contrarecibo_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_cuentas_bancarias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_cuentas_bancarias` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_cuentas_bancarias` (
  `id_cuentas_bancarias` INT NOT NULL AUTO_INCREMENT,
  `cuenta` VARCHAR(20) NULL,
  `banco` VARCHAR(100) NULL,
  `sucursal` VARCHAR(100) NULL,
  `num_sucursal` INT NULL,
  `fecha_apertura` DATE NULL,
  `minimo` DOUBLE(11,4) NULL,
  `saldo_inicial` DOUBLE(11,4) NULL,
  `saldo` DOUBLE(11,4) NULL,
  `cta_contable` VARCHAR(25) NULL,
  `nombre` TEXT NULL,
  PRIMARY KEY (`id_cuentas_bancarias`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_bancos_movimientos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_bancos_movimientos` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_bancos_movimientos` (
  `id_bancos_movimientos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cuenta` INT NULL,
  `cuenta` VARCHAR(20) NULL,
  `movimiento` INT NULL,
  `clave` VARCHAR(10) NULL,
  `concepto` TEXT NULL,
  `t_movimiento` VARCHAR(25) NULL,
  `fecha_emision` DATE NULL,
  `fecha_emision_real` DATE NOT NULL,
  `fecha_movimiento` DATE NULL,
  `fecha_movimiento_real` DATE NOT NULL,
  `hora_movimiento` TIME NOT NULL,
  `importe` DOUBLE(11,4) NULL,
  `neto` DOUBLE(11,4) NULL,
  `cargo` DOUBLE(11,4) NULL,
  `abono` DOUBLE(11,4) NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `proveedor` TEXT NULL,
  `descripcion` TEXT NULL,
  `saldo` DOUBLE(11,4) NULL,
  `creado_por` VARCHAR(100) NULL,
  `cancelada` TINYINT(1) NULL,
  `fecha_cancelada` DATE NULL,
  `fecha_conciliado` DATE NULL,
  `fecha_pago` DATE NULL,
  `enfirme` TINYINT(1) NULL,
  `sifirme` TINYINT(1) NULL,
  `tipo` VARCHAR(30) NULL,
  `numero` INT NULL,
  `aplicado` TINYINT(1) NULL,
  `pedido` INT NULL,
  `cuenta_cargo` VARCHAR(50) NULL,
  `descripcion_cuenta_cargo` VARCHAR(100) NULL,
  `cuenta_abono` VARCHAR(50) NULL,
  `descripcion_cuenta_abono` VARCHAR(100) NULL,
  `tipo_pedido` VARCHAR(30) NULL,
  `tipo_gasto` INT NULL,
  `subsidio` VARCHAR(50) NULL,
  `destino` VARCHAR(50) NULL,
  `id_usuario` INT NULL,
  PRIMARY KEY (`id_bancos_movimientos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_bancos_pagos_contrarecibos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_bancos_pagos_contrarecibos` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_bancos_pagos_contrarecibos` (
  `id_bancos_pagos_contrarecibos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cuenta` VARCHAR(20) NULL,
  `movimiento` INT NULL,
  `numero` INT NULL,
  `nombre` TEXT NULL,
  `importe` DOUBLE(11,4) NULL,
  PRIMARY KEY (`id_bancos_pagos_contrarecibos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_correlacion_partidas_contables`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_correlacion_partidas_contables` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_correlacion_partidas_contables` (
  `id_correlacion_partidas_contables` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_nivel` BLOB NULL,
  `clave` VARCHAR(20) NULL,
  `descripcion` TEXT NULL,
  `cuenta_cargo` VARCHAR(20) NULL,
  `nombre_cargo` TEXT NULL,
  `cuenta_abono` VARCHAR(20) NULL,
  `nombre_abono` TEXT NULL,
  `tipo_gasto` INT(3) NULL,
  `destino` VARCHAR(100) NULL,
  `caracteristicas` TEXT NULL,
  `medio` VARCHAR(50) NULL,
  PRIMARY KEY (`id_correlacion_partidas_contables`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`modulos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`modulos` ;

CREATE TABLE IF NOT EXISTS `software`.`modulos` (
  `id_modulos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_modulo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_modulos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`firmas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`firmas` ;

CREATE TABLE IF NOT EXISTS `software`.`firmas` (
  `id_firmas` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NULL,
  `firma` INT NULL,
  PRIMARY KEY (`id_firmas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`grupos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`grupos` ;

CREATE TABLE IF NOT EXISTS `software`.`grupos` (
  `id_grupos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_grupo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_grupos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`permisos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`permisos` ;

CREATE TABLE IF NOT EXISTS `software`.`permisos` (
  `id_permisos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `modulo` VARCHAR(100) NOT NULL,
  `activo` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id_permisos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`articulos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`articulos` ;

CREATE TABLE IF NOT EXISTS `software`.`articulos` (
  `id_articulos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ley` VARCHAR(45) NULL,
  `articulo` VARCHAR(45) NULL,
  PRIMARY KEY (`id_articulos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`datos_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`datos_usuario` ;

CREATE TABLE IF NOT EXISTS `software`.`datos_usuario` (
  `id_datos_usuario` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NULL,
  `nombre` VARCHAR(100) NULL,
  `apellido_paterno` VARCHAR(100) NULL,
  `apellido_materno` VARCHAR(100) NULL,
  `puesto` VARCHAR(100) NULL,
  PRIMARY KEY (`id_datos_usuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`lugar_de_entrega`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`lugar_de_entrega` ;

CREATE TABLE IF NOT EXISTS `software`.`lugar_de_entrega` (
  `id_lugar_de_entrega` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `lugar_de_entrega` TEXT NULL,
  PRIMARY KEY (`id_lugar_de_entrega`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_partidas_presupuestales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_partidas_presupuestales` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_partidas_presupuestales` (
  `id_partidas_presupuestales` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `capitulo` INT NULL,
  `concepto` VARCHAR(10) NULL,
  `rubro` VARCHAR(10) NULL,
  `clave` VARCHAR(25) NULL,
  `descripcion` TEXT NULL,
  `cuenta` VARCHAR(30) NULL,
  `sub_cuenta` VARCHAR(20) NULL,
  `auxiliar` VARCHAR(10) NULL,
  `clasificacion` VARCHAR(255) NULL,
  `aplicacion` VARCHAR(20) NULL,
  `restriccion` VARCHAR(50) NULL,
  `clave_scian` VARCHAR(10) NULL,
  `amplia` TEXT NULL,
  PRIMARY KEY (`id_partidas_presupuestales`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_cuentas_contables`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_cuentas_contables` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_cuentas_contables` (
  `id_cuentas_contables` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cuenta` VARCHAR(50) NULL,
  `nombre` TEXT NULL,
  `tipo` VARCHAR(1) NULL,
  `codigo_agrupador` VARCHAR(10) NULL,
  `grupo` VARCHAR(50) NULL,
  `cargo` DOUBLE(11,4) NULL,
  `abono` DOUBLE(11,4) NULL,
  `saldo` DOUBLE(11,4) NULL,
  `descripcion` TEXT NULL,
  `id_padre` INT NULL DEFAULT 0,
  PRIMARY KEY (`id_cuentas_contables`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_adecuaciones_egresos_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_adecuaciones_egresos_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_adecuaciones_egresos_detalle` (
  `id_adecuaciones_egresos_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_adecuacion` INT NULL,
  `tipo` VARCHAR(25) NULL,
  `control` TINYINT(1) NULL,
  `subsidio` VARCHAR(30) NULL,
  `year` YEAR NULL,
  `fecha_aplicacion` DATE NULL,
  `hora_aplicacion` VARCHAR(5) NULL,
  `fecha_sql` DATETIME NULL,
  `partida` VARCHAR(20) NULL,
  `id_nivel` INT NULL,
  `estructura` BLOB NULL,
  `capitulo` TEXT NULL,
  `mes_destino` TEXT NULL,
  `total` DOUBLE(11,4) NULL,
  `titulo` TEXT NULL,
  `texto` TEXT NULL,
  `id_enlace` INT NULL,
  PRIMARY KEY (`id_adecuaciones_egresos_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_adecuaciones_ingresos_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_adecuaciones_ingresos_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_adecuaciones_ingresos_detalle` (
  `id_adecuaciones_ingresos_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_adecuacion` INT NULL,
  `tipo` TEXT NULL,
  `control` TINYINT(1) NULL,
  `subsidio` TEXT NULL,
  `year` YEAR NULL,
  `fecha_aplicacion` DATE NULL,
  `hora_aplicacion` VARCHAR(5) NULL,
  `fecha_sql` DATETIME NULL,
  `partida` TEXT NULL,
  `id_nivel` INT NULL,
  `estructura` BLOB NULL,
  `capitulo` TEXT NULL,
  `mes_destino` TEXT NULL,
  `total` DOUBLE(11,4) NULL,
  `titulo` TEXT NULL,
  `texto` TEXT NULL,
  `id_enlace` INT NULL,
  PRIMARY KEY (`id_adecuaciones_ingresos_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_adecuaciones_egresos_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_adecuaciones_egresos_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_adecuaciones_egresos_caratula` (
  `id_adecuaciones_egresos_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(25) NULL,
  `numero` INT NULL,
  `control` TINYINT(1) NULL,
  `fecha_solicitud` DATE NULL,
  `fecha_aplicacion` DATE NULL,
  `fecha_sql` DATETIME NULL,
  `subsidio` TEXT NULL,
  `clasificacion` TEXT NULL,
  `no_solicitud` VARCHAR(100) NULL,
  `no_autorizacion` VARCHAR(100) NULL,
  `importe_total` DOUBLE(11,4) NULL,
  `firma1` TINYINT(1) NULL,
  `firma2` TINYINT(1) NULL,
  `firma3` TINYINT(1) NULL,
  `enfirme` TINYINT(1) NULL,
  `sifirme` TINYINT(1) NULL,
  `year` YEAR NULL,
  `cancelada` TINYINT(1) NULL,
  `fecha_cancelada` DATE NULL,
  `concepto` TEXT NULL,
  `aplicacion` TEXT NULL,
  `distribucion` TEXT NULL,
  `capitulo` TEXT NULL,
  `mov_bancario` INT NULL,
  `seleccion` TINYINT(1) NULL,
  `ejecucion` TINYINT(1) NULL,
  `confirmacion` TINYINT(1) NULL,
  `reclamo_tipo` TEXT NULL,
  `cve_progreso` TEXT NULL,
  `tipo_gasto` INT(1) NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  `destino` TEXT NULL,
  `descripcion` TEXT NULL,
  `estatus` VARCHAR(10) NULL DEFAULT 'espera',
  PRIMARY KEY (`id_adecuaciones_egresos_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_adecuaciones_ingresos_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_adecuaciones_ingresos_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_adecuaciones_ingresos_caratula` (
  `id_adecuaciones_ingresos_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(25) NULL,
  `numero` INT NULL,
  `control` TINYINT(1) NULL,
  `fecha_solicitud` DATE NULL,
  `fecha_aplicacion` DATE NULL,
  `fecha_sql` DATETIME NULL,
  `subsidio` TEXT NULL,
  `clasificacion` TEXT NULL,
  `no_solicitud` TEXT NULL,
  `no_autorizacion` TEXT NULL,
  `importe_total` DOUBLE(11,4) NULL,
  `firma1` TINYINT(1) NULL,
  `firma2` TINYINT(1) NULL,
  `firma3` TINYINT(1) NULL,
  `enfirme` TINYINT(1) NULL,
  `sifirme` TINYINT(1) NULL,
  `year` YEAR NULL,
  `cancelada` TINYINT(1) NULL,
  `fecha_cancelada` DATE NULL,
  `concepto` TEXT NULL,
  `aplicacion` TEXT NULL,
  `distribucion` VARCHAR(50) NULL,
  `capitulo` VARCHAR(10) NULL,
  `mov_bancario` INT NULL,
  `seleccion` TINYINT(1) NULL,
  `ejecucion` TINYINT(1) NULL,
  `confirmacion` TINYINT(1) NULL,
  `reclamo_tipo` TEXT NULL,
  `cve_progreso` TEXT NULL,
  `tipo_gasto` INT(1) NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  `destino` TEXT NULL,
  `descripcion` TEXT NULL,
  `estatus` VARCHAR(10) NULL DEFAULT 'espera',
  PRIMARY KEY (`id_adecuaciones_ingresos_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`grupos_usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`grupos_usuarios` ;

CREATE TABLE IF NOT EXISTS `software`.`grupos_usuarios` (
  `id_grupos_usuarios` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_grupo` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_grupos_usuarios`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`cat_bancos_conceptos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`cat_bancos_conceptos` ;

CREATE TABLE IF NOT EXISTS `software`.`cat_bancos_conceptos` (
  `id_cat_bancos_conceptos` INT NOT NULL AUTO_INCREMENT,
  `clave` VARCHAR(45) NOT NULL,
  `concepto` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_cat_bancos_conceptos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_polizas_cabecera`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_polizas_cabecera` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_polizas_cabecera` (
  `id_polizas_cabecera` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_poliza` INT NULL,
  `tipo_poliza` VARCHAR(30) NULL,
  `fecha` DATE NULL,
  `fecha_real` DATE NULL,
  `referencia` VARCHAR(255) NULL,
  `concepto` TEXT NULL,
  `auditada` TINYINT(1) NULL DEFAULT 0,
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `sifirme` TINYINT(1) NULL DEFAULT 0,
  `no_partidas` INT NULL,
  `nombre` VARCHAR(100) NULL,
  `importe` DOUBLE(11,4) NULL,
  `cancelada` TINYINT(1) NULL DEFAULT 0,
  `estatus` VARCHAR(15) NULL,
  `creado_por` VARCHAR(255) NULL,
  `autorizada` TINYINT(1) NULL DEFAULT 0,
  `revisado` TINYINT(1) NULL,
  `cargos` DOUBLE(11,4) NULL,
  `abonos` DOUBLE(11,4) NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `proveedor` TEXT NULL,
  PRIMARY KEY (`id_polizas_cabecera`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_polizas_periodos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_polizas_periodos` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_polizas_periodos` (
  `id_polizas_periodos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `year` DATE NULL,
  `fecha_inicia` DATE NULL,
  `fecha_termina` DATE NULL,
  `poliza_diario` INT NULL,
  `poliza_ingreso` INT NULL,
  `poliza_egreso` INT NULL,
  `cerrado` TINYINT(1) NULL,
  `salario_anterior` DOUBLE(11,4) NULL,
  `debe` DOUBLE(11,4) NULL,
  `haber` DOUBLE(11,4) NULL,
  `salario_actual` DOUBLE(11,4) NULL,
  PRIMARY KEY (`id_polizas_periodos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_poliza_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_poliza_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_poliza_detalle` (
  `id_poliza_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_poliza` INT NULL,
  `tipo_poliza` VARCHAR(30) NULL,
  `fecha` DATE NULL,
  `hora` TIME NULL,
  `fecha_real` DATE NULL,
  `id_cuenta` INT NULL,
  `cuenta` VARCHAR(100) NULL,
  `concepto` TEXT NULL,
  `aplica` TINYINT(1) NULL,
  `debe` DOUBLE(11,4) NULL,
  `haber` DOUBLE(11,4) NULL,
  `nueva` TINYINT(1) NULL DEFAULT 0,
  `depto` VARCHAR(30) NULL,
  `tipo_cambio` DOUBLE(11,4) NULL,
  `subsidio` TEXT NULL,
  `nombre` TEXT NULL,
  `destino` TEXT NULL,
  `centro_costo` VARCHAR(20) NULL,
  `partida` VARCHAR(20) NULL,
  PRIMARY KEY (`id_poliza_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_poliza_cabecera_modelo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_poliza_cabecera_modelo` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_poliza_cabecera_modelo` (
  `id_poliza_cabecera_modelo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo_poliza` VARCHAR(100) NULL,
  `fecha` DATE NULL,
  `fecha_real` DATE NULL,
  `numero_poliza` INT NULL,
  `referencia` TEXT NULL,
  `concepto` TEXT NULL,
  `auditada` TINYINT(1) NULL,
  `enfirme` TINYINT(1) NULL,
  `sifirme` TINYINT(1) NULL,
  `numero_partidas` INT NULL,
  `nombre` TEXT NULL,
  `importe` DOUBLE(11,4) NULL,
  `cancelada` TINYINT(1) NULL,
  `creado_por` VARCHAR(100) NULL,
  `autorizado` TINYINT(1) NULL,
  `revisado` VARCHAR(100) NULL,
  PRIMARY KEY (`id_poliza_cabecera_modelo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_poliza_detalle_modelo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_poliza_detalle_modelo` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_poliza_detalle_modelo` (
  `id_poliza_detalle_modelo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo_poliza` VARCHAR(100) NULL,
  `fecha` DATE NULL,
  `hora` TIME NULL,
  `fecha_real` DATE NULL,
  `numero_poliza` INT NULL,
  `id_cuenta` INT NULL,
  `cuenta` TEXT NULL,
  `concepto` TEXT NULL,
  `aplica` TINYINT(1) NULL,
  `debe` DOUBLE(11,4) NULL,
  `haber` DOUBLE(11,4) NULL,
  `nueva` TINYINT(1) NULL,
  `depto` TEXT NULL,
  `tipo_cambio` DOUBLE(11,4) NULL,
  `subsidio` TEXT NULL,
  `nombre` TEXT NULL,
  PRIMARY KEY (`id_poliza_detalle_modelo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_poliza_saldos_mes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_poliza_saldos_mes` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_poliza_saldos_mes` (
  `id_poliza_saldos_mes` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cuenta` INT NULL,
  `cuenta` VARCHAR(50) NULL,
  `year` VARCHAR(10) NULL,
  `mes` VARCHAR(2) NULL,
  `nombre` VARCHAR(255) NULL,
  `salario_anterior` DOUBLE(11,4) NULL,
  `debe` DOUBLE(11,4) NULL,
  `haber` DOUBLE(11,4) NULL,
  `salario_actual` DOUBLE(11,4) NULL,
  `acumulado_debe` DOUBLE(11,4) NULL,
  `acumulado_haber` DOUBLE(11,4) NULL,
  `subsidio` VARCHAR(50) NULL,
  `tipo` VARCHAR(1) NULL,
  PRIMARY KEY (`id_poliza_saldos_mes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_devengado_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_devengado_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_devengado_caratula` (
  `id_devengado` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  `numero` INT NULL,
  `no_solicitud` TEXT NULL,
  `no_autorizacion` TEXT NULL,
  `no_movimiento` TEXT NULL,
  `clave_libreria` VARCHAR(20) NULL,
  `fecha_solicitud` DATE NULL,
  `fecha_aplicacion` DATE NULL,
  `fecha_sql` DATETIME NULL,
  `year` YEAR NULL,
  `clasificacion` TEXT NULL,
  `importe_total` DOUBLE(11,4) NULL,
  `cancelada` TINYINT(1) NULL DEFAULT 0,
  `fecha_cancelada` DATE NULL,
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `sifirme` TINYINT(1) NULL DEFAULT 0,
  `estatus_recaudado` TINYINT(1) NULL DEFAULT 0,
  `estatus` VARCHAR(10) NULL,
  `descripcion` TEXT NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  PRIMARY KEY (`id_devengado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_recaudado_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_recaudado_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_recaudado_caratula` (
  `id_recaudado` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  `numero` INT NULL,
  `no_solicitud` TEXT NULL,
  `no_autorizacion` TEXT NULL,
  `no_movimiento` TEXT NULL,
  `clave_libreria` VARCHAR(20) NULL,
  `fecha_solicitud` DATE NULL,
  `fecha_aplicacion` DATE NULL,
  `fecha_recaudado` DATE NULL,
  `hora_recaudado` TIME NULL,
  `fecha_sql` DATETIME NULL,
  `year` YEAR NULL,
  `clasificacion` TEXT NULL,
  `importe_total` DOUBLE(11,4) NULL,
  `cancelada` TINYINT(1) NULL DEFAULT 0,
  `fecha_cancelada` DATE NULL,
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `sifirme` TINYINT(1) NULL DEFAULT 0,
  `estatus` VARCHAR(10) NULL,
  `descripcion` TEXT NULL,
  `mes_recaudado` VARCHAR(11) NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  PRIMARY KEY (`id_recaudado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_devengado_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_devengado_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_devengado_detalle` (
  `id_devengado_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_devengado` INT NULL,
  `tipo` VARCHAR(45) NULL,
  `subsidio` TEXT NULL,
  `fecha_aplicacion` DATE NULL,
  `hora_aplicacion` VARCHAR(5) NULL,
  `fecha_sql` DATETIME NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `total_importe` DOUBLE(11,4) NULL,
  `descripcion_detalle` TEXT NULL,
  `partida` TEXT NULL,
  `id_nivel` INT NULL,
  `nivel` BLOB NULL,
  `capitulo` TEXT NULL,
  `mes_destino` TEXT NULL,
  PRIMARY KEY (`id_devengado_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_recaudado_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_recaudado_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_recaudado_detalle` (
  `id_recaudado_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_recaudado` INT NULL,
  `tipo` VARCHAR(45) NULL,
  `subsidio` TEXT NULL,
  `fecha_aplicacion` DATE NULL,
  `hora_aplicacion` VARCHAR(5) NULL,
  `fecha_sql` DATETIME NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `total_importe` DOUBLE(11,4) NULL,
  `descripcion_detalle` TEXT NULL,
  `partida` TEXT NULL,
  `id_nivel` INT NULL,
  `nivel` BLOB NULL,
  `capitulo` TEXT NULL,
  `mes_destino` TEXT NULL,
  PRIMARY KEY (`id_recaudado_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_nota_entrada_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_nota_entrada_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_nota_entrada_caratula` (
  `id_nota_entrada_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_movimiento` INT NULL,
  `fecha_emision` DATE NULL,
  `fecha_emision_real` DATE NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `proveedor` TEXT NULL,
  `documento` TEXT NULL,
  `no_documento` TEXT NULL,
  `precompromiso` INT NULL,
  `pedido` INT NULL,
  `subotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `importe` DOUBLE(11,4) NULL,
  `total_compromiso` DOUBLE(11,4) NULL,
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `sifirme` TINYINT(1) NULL DEFAULT 0,
  `pago` TEXT NULL,
  `no_pago` TEXT NULL,
  `cheque` TINYINT(1) NULL DEFAULT 0,
  `cuenta` VARCHAR(45) NULL,
  `banco` VARCHAR(100) NULL,
  `cancelada` TINYINT(1) NULL,
  `fecha_cancelada` DATE NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(255) NULL,
  `tipo_poliza` VARCHAR(45) NULL,
  `cuenta_debe` VARCHAR(45) NULL,
  `descripcion_debe` TEXT NULL,
  `cuenta_haber` VARCHAR(45) NULL,
  `descripcion_haber` TEXT NULL,
  `condiciones` TEXT NULL,
  `observaciones` VARCHAR(45) NULL,
  PRIMARY KEY (`id_nota_entrada_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_nota_entrada_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_nota_entrada_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_nota_entrada_detalle` (
  `id_nota_entrada_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_movimiento` INT NULL,
  `id_nivel` INT NULL,
  `estructura` BLOB NULL,
  `articulo` VARCHAR(45) NULL,
  `unidad_medida` VARCHAR(45) NULL,
  `cantidad` INT NULL,
  `p_unitario` DOUBLE(11,4) NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `importe` DOUBLE(11,4) NULL,
  `titulo` TEXT NULL,
  `clave` INT NULL,
  `descripcion` TEXT NULL,
  `entregada` TINYINT(1) NULL DEFAULT 0,
  `tipo_poliza` VARCHAR(45) NULL,
  `cuenta_debe` VARCHAR(45) NULL,
  `descripcion_debe` TEXT NULL,
  `cuenta_haber` VARCHAR(45) NULL,
  `descripcion_haber` TEXT NULL,
  `observaciones` VARCHAR(45) NULL,
  PRIMARY KEY (`id_nota_entrada_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_nota_salida_caratula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_nota_salida_caratula` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_nota_salida_caratula` (
  `id_nota_salida_caratula` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_movimiento` INT NULL,
  `fecha_emision` DATE NULL,
  `fecha_emision_real` DATE NULL,
  `capitulo` VARCHAR(45) NULL,
  `concepto` TEXT NULL,
  `otro` TEXT NULL,
  `numero_acta` VARCHAR(45) NULL,
  `firma1` TINYINT(1) NULL DEFAULT 0,
  `firma2` TINYINT(1) NULL DEFAULT 0,
  `firma3` TINYINT(1) NULL DEFAULT 0,
  `id_nivel` INT NULL,
  `estructura` BLOB NULL,
  `descripcion` TEXT NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `importe` DOUBLE(11,4) NULL,
  `enfirme` TINYINT(1) NULL DEFAULT 0,
  `sifirme` TINYINT(1) NULL DEFAULT 0,
  `cancelada` TINYINT(1) NULL DEFAULT 0,
  `fecha_cancelada` DATE NULL,
  `pedido` INT NULL,
  `area_asignado` VARCHAR(100) NULL,
  `id_usuario` INT NULL,
  `creado_por` VARCHAR(100) NULL,
  `tipo_poliza` VARCHAR(45) NULL,
  `cuenta_debe` VARCHAR(45) NULL,
  `descripcion_debe` TEXT NULL,
  `cuenta_haber` VARCHAR(45) NULL,
  `decripcion_haber` TEXT NULL,
  `observaciones` VARCHAR(45) NULL,
  PRIMARY KEY (`id_nota_salida_caratula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_nota_salida_detalle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_nota_salida_detalle` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_nota_salida_detalle` (
  `id_nota_salida_detalle` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_movimiento` INT NULL,
  `id_nivel` INT NULL,
  `estructura` BLOB NULL,
  `articulo` VARCHAR(45) NULL,
  `unidad` VARCHAR(45) NULL,
  `cantidad` INT NULL,
  `p_unitario` DOUBLE(11,4) NULL,
  `subtotal` DOUBLE(11,4) NULL,
  `iva` DOUBLE(11,4) NULL,
  `total` DOUBLE(11,4) NULL,
  `recibida` DOUBLE(11,4) NULL,
  `titulo` TEXT NULL,
  `descripcion` TEXT NULL,
  `numero_entrada` INT NULL,
  `tipo_poliza` VARCHAR(45) NULL,
  `cuenta_debe` VARCHAR(45) NULL,
  `descripcion_debe` TEXT NULL,
  `cuenta_haber` VARCHAR(45) NULL,
  `descripcion_haber` TEXT NULL,
  `observaciones` VARCHAR(45) NULL,
  PRIMARY KEY (`id_nota_salida_detalle`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_kardex_almacen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_kardex_almacen` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_kardex_almacen` (
  `id_kardex_almacen` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_nivel` INT NULL,
  `estructura` BLOB NULL,
  `articulo` VARCHAR(45) NULL,
  `fecha` DATE NULL,
  `fecha_sql` DATE NULL,
  `hora` TIME NULL,
  `operacion` VARCHAR(20) NULL,
  `movimiento` TEXT NULL,
  `entra` INT NULL,
  `sale` INT NULL,
  `existe` INT NULL,
  `id_proveedor` VARCHAR(45) NULL,
  `proveedor` TEXT NULL,
  `precio_proveedor` DECIMAL(11,4) NULL,
  `precio_promedio` DOUBLE(11,4) NULL,
  `debe` DOUBLE(11,4) NULL,
  `haber` DOUBLE(11,4) NULL,
  `saldo` DOUBLE(11,4) NULL,
  `afectacion` VARCHAR(45) NULL,
  `descripcion` TEXT NULL,
  `observaciones` VARCHAR(45) NULL,
  PRIMARY KEY (`id_kardex_almacen`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `software`.`mov_tabla_lugares_periodos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `software`.`mov_tabla_lugares_periodos` ;

CREATE TABLE IF NOT EXISTS `software`.`mov_tabla_lugares_periodos` (
  `id_tabla_lugares_periodos` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numero_precompromiso` INT NULL,
  `numero_compromiso` INT NULL,
  `lugar` TEXT NULL,
  `periodo_inicio` DATE NULL,
  `periodo_fin` DATE NULL,
  `cuota_diaria` DOUBLE(11,4) NULL,
  `dias` INT NULL,
  `importe` DOUBLE(11,4) NULL,
  PRIMARY KEY (`id_tabla_lugares_periodos`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
