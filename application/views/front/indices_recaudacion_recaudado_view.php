<h3 class="page-header title center"><i class="fa fa-shopping-cart fa-fw"></i> Recaudado </h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_recaudado") || $this->utilerias->get_grupo() == 1) { ?>
                <a href="<?= base_url("recaudacion/agregar_recaudado")?>" class="btn btn-default"><i class="fa fa-plus-circle circle" style="color: #B6CE33;" ></i> Generar Recaudado</a>
            <?php } ?>
            <a href="<?= base_url("recaudacion/imprimir_recaudados") ?>" class="btn btn-default"><i class="fa fa-print" style="color: #B6CE33;"></i> Imprimir Movimientos</a>
            <a href="<?= base_url("recaudacion/exportar_recaudados") ?>" class="btn btn-default"><i class="fa fa-download" style="color: #B6CE33;"></i> Exportar Movimientos</a>
            <a href="<?= base_url("recaudacion/exportar_devengado_info_recaudado") ?>" class="btn btn-default"><i class="fa fa-download" style="color: #B6CE33;"></i> Exportar Devengado vs. Recaudado</a>
            <?php if($this->utilerias->get_permisos("subir_recaudado") || $this->utilerias->get_grupo() == 1) { ?>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload" style="color: #B6CE33;"></i> Subir Archivo por Recaudar</button>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="6%">No.</th>
                                <th width="10%">Devengado</th>
                                <th width="9%">Tipo</th>
                                <th width="9%">Factura</th>
                                <th width="8%">C.R.</th>
                                <th width="15%">Clasificación</th>
                                <th width="11%">F. Recaudado</th>
                                <th width="11%">Importe</th>
                                <th width="10%">Creado por</th>
                                <th width="8%">Estatus</th>
                                <th width="9%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo por Recaudar</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('recaudacion/subir_recaudado_devengado', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-default',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Recaudado</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el recaudado seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_recaudado" id="cancelar_recaudado" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_recaudado">Aceptar</button>
            </div>
        </div>
    </div>
</div>