<h3 class="page-header title center"><i class="fa fa-files-o"></i> Poner P&oacute;lizas En Firme</h3>
<div id="page-wrapper">
    <form class="" name="form" method="POST" id="datos_polizas" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">

                        <!--Tipo de Consulta-->
                        <label style="margin-top: 1%;">Seleccione las fechas para poner las p&oacute;lizas en firme</label>
                        <!--Rango Fechas-->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" />
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" />
                            </div>
                        </div>

                        <select class="form-control" id="tipo_poliza" name="tipo_poliza">
                            <option value="">Tipo de P&oacute;liza</option>
                            <option value="Diario">Diario</option>
                            <option value="Egresos">Egresos</option>
                            <option value="Ingresos">Ingresos</option>
                            <option value="Todas">Todas</option>
                        </select>

                        <!--Tipo de Consulta-->
                        <label style="margin-top: 1%;">Rango de n&uacute;meros</label>
                        <!--Rango Fechas-->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="number" class="form-control" name="numero_inicial" id="numero_inicial" placeholder="N&uacute;mero Inicial" />
                            </div>
                            <div class="col-lg-6">
                                <input type="number" class="form-control" name="numero_final" id="numero_final" placeholder="N&uacute;mero Final" />
                            </div>
                        </div>

                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("/contabilidad/polizas") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" id="boton_ejecutar" value="Continuar"/>
                        </div>

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-12" id="resultado">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>