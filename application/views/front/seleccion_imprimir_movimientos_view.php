<h3 class="page-header title center"><i class="fa fa-file-excel-o"></i>Imprimir Movimientos Bancarios</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("ciclo/imprimir_movimiento_formato/".$id_cuenta) ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <div class="list-group error-completar">
                            <?php if(isset($mensaje)) { ?>
                                <div class="alert alert-danger">
                                    <?= $mensaje ?>
                                </div>
                                <div class="text-center">
                                    <div class="btns-finales">
                                        <a class="btn btn-default" href="<?= base_url("ciclo/cuenta_bancaria/".$id_cuenta) ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <!-- Rango de Fechas-->
                                <div class="row" style="margin-top: 1%;">
                                    <input type="hidden" name="id_cuenta" id="id_cuenta" value="<?= $id_cuenta ?>">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                                    </div>
                                </div>
                                <!-- No. Movimiento Bancario-->
                                <input type="text" class="form-control" name="movimiento" id="movimiento" placeholder="No. Movimiento Bancario">
                                <!-- Proveedor-->
                                <input type="hidden" name="id_proveedor" id="id_proveedor" />
                                <div class="form-group input-group">
                                    <input type="text"  class="form-control ic-buscar-input" name="proveedor" id="proveedor" placeholder="Proveedor"/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- Tipo -->
                                <select class="form-control" id="tipo" name="tipo">
                                    <option value="">Tipo</option>
                                    <option value="presupuesto">Presupuestario</option>
                                    <option value="extra">Extra Presupuestario</option>
                                </select>
                                <!-- Tipo Movimiento-->
                                <select class="form-control" id="tipo_movimiento" name="tipo_movimiento">
                                    <option value="">Tipo Movimiento</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="En línea">En línea</option>
                                    <option value="SPEI">SPEI</option>
                                    <option value="Nómina">Nómina</option>
                                    <option value="">Todos</option>
                                </select>
                                <div class="btns-finales text-center">
                                    <a class="btn btn-default" href="<?= base_url("ciclo/cuenta_bancaria/".$id_cuenta) ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                                    <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar"/>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
                </div>
                <div class="modal-body table-gral modal-action modal-3">
                    <div class="table-responsive">
                        <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor"/>
                        <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Clave</th>
                                <th>Nombre Comercial</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

