<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Consulta Presupuestaria</h3>
<div id="page-wrapper">
    <div class="row add-pre error-detalle">
        <div class="col-lg-12">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <!--Primera Parte Partida Inicio-->
                    <div class="col-lg-2 niveles-pc" style="margin-top: 1%;">
                        <?= $nivel_superior ?>
                        <a href="#modal_estructura_superior" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura_superior">¿No conoces la
                            <br/>estructura?</a>
                    </div>

                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Enero</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="enero_superior_inicial" id="enero_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="enero_superior" id="enero_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="enero_superior_devengado" id="enero_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="enero_superior_recaudado" id="enero_superior_recaudado" disabled/> </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Febrero</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="febrero_superior_inicial" id="febrero_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="febrero_superior" id="febrero_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="febrero_superior_devengado" id="febrero_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="febrero_superior_recaudado" id="febrero_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Marzo</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="marzo_superior_inicial" id="marzo_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="marzo_superior" id="marzo_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="marzo_superior_devengado" id="marzo_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="marzo_superior_recaudado" id="marzo_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Abril</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="abril_superior_inicial" id="abril_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="abril_superior" id="abril_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="abril_superior_devengado" id="abril_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="abril_superior_recaudado" id="abril_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Mayo</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="mayo_superior_inicial" id="mayo_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="mayo_superior" id="mayo_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="mayo_superior_devengado" id="mayo_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="mayo_superior_recaudado" id="mayo_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Junio</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="junio_superior_inicial" id="junio_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="junio_superior" id="junio_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes"> Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="junio_superior_devengado" id="junio_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes"> Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="junio_superior_recaudado" id="junio_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Julio</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="julio_superior_inicial" id="julio_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="julio_superior" id="julio_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="julio_superior_devengado" id="julio_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="julio_superior_recaudado" id="julio_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Agosto</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="agosto_superior_inicial" id="agosto_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="agosto_superior" id="agosto_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="agosto_superior_devengado" id="agosto_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="agosto_superior_recaudado" id="agosto_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Septiembre</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="septiembre_superior_inicial" id="septiembre_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="septiembre_superior" id="septiembre_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="septiembre_superior_devengado" id="septiembre_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="septiembre_superior_recaudado" id="septiembre_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Octubre</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="octubre_superior_inicial" id="octubre_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="octubre_superior" id="octubre_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8">  <input type="text" class="form-control disabled-color" name="octubre_superior_devengado" id="octubre_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="octubre_superior_recaudado" id="octubre_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Noviembre</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="noviembre_superior_inicial" id="noviembre_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="noviembre_superior" id="noviembre_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="noviembre_superior_devengado" id="noviembre_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label></div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="noviembre_superior_recaudado" id="noviembre_superior_recaudado" disabled/> </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="center"><i class="fa fa-calendar-o" style="color:#B6CE33; margin-right: 2%;"></i>  Diciembre</h4>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Inicial</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="diciembre_superior_inicial" id="diciembre_superior_inicial" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Modificado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="diciembre_superior" id="diciembre_superior" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Devengado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="diciembre_superior_devengado" id="diciembre_superior_devengado" disabled/> </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label class="cont-mes">Recaudado</label> </div>
                                    <div class="col-lg-8"> <input type="text" class="form-control disabled-color" name="diciembre_superior_recaudado" id="diciembre_superior_recaudado" disabled/> </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-10 text-center totales">
                            <h4 id="total_superior"></h4>
                        </div>
                    </div>
                </div>
                <!--Primera Parte Partida Inicio-->

            </div>
        </div>
    </div>
</div>


<!-- Modal estructura superior-->
<div class="modal fade" id="modal_estructura_superior" tabindex="-1" role="dialog" aria-labelledby="modal_estructura_superior" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Estructura de Egresos</h4>
            </div>
            <div class="modal-body text-center">
                <?= $niveles_modal_superior ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel_superior">Elegir</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->