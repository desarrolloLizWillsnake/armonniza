<h3 class="page-header title center"><i class="fa fa-list-alt fa-fw"></i> Almacenes </h3>
<div id="page-wrapper">
    <div class="row cont_canales">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h3><i class="fa fa-building-o"></i> Librerías</h3>
            <div class="cont_detalles" style="margin-top: 8%;"><h4>Corporativo</h4></div>
            <div class="cont_detalles"><h4>Zona Sur</h4></div>
            <div class="cont_detalles"><h4>Zona Centro</h4></div>
            <div class="cont_detalles"><h4>Zona Norte</h4></div>
            <div class="cont_detalles"><h4>Librobus</h4></div>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>
<!--
<div id="page-wrapper">
        <ul class="media-list">
            <li class="media cont_almacenes">
                <h2>Librerías</h2>
                <div class="media-left">
                    <i class="fa fa-building-o"></i>
                </div>
                <div class="media-body">
                    <h3 class="media-heading" style="padding: 5% 0%;">Corporativo</h3>

                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-building"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading" style="padding: 6% 0%;">Zona Sur</h3>

                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-building-o"></i>
                                </div>
                                <div class="media-body" >
                                    <h3 class="media-heading" style="padding: 8% 0%;">Zona Centro</h3>

                                    <div class="media">
                                        <div class="media-left">
                                            <i class="fa fa-building"></i>
                                        </div>
                                        <div class="media-body">
                                            <h3 class="media-heading" style="padding: 11% 0%;">Zona Norte</h3>

                                            <div class="media">
                                                <div class="media-left">
                                                    <i class="fa fa-bus"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h3 class="media-heading" style="padding: 20% 0%;">Librobus</h3>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </li>
        </ul>

</div>-->