<h3 class="page-header title center"><i class="fa fa-eye ic-color"></i> Ver Nota de Salida</h3>
<div id="page-wrapper">

    <form class="forma_poliza_general" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">General</div>
                    <div class="panel-body">
                        <input type="hidden" name="ultima_nota" id="ultima_nota" value="<?= $numero_movimiento ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!---No. Nota Entrada-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Nota Salida</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_view"><?= $numero_movimiento ?></p></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Concepto de Salida</label>
                                    <p class="form-control-static input_view"><?= $concepto_salida ?></p>
                                </div>
                                <!-- Otro Tipo Concepto -->
                                <?php if($concepto_salida == 'Otro') { ?>
                                    <label>Descripci&oacute;n Otro</label>
                                    <p class="form-control-static input_view"><?= $concepto_salida_otro ?></p>
                                <?php } else{?>
                                    <p class="form-control-static input_view" style="display:none;"></p>
                                <?php } ?>
                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Centro de Costos</label>
                                    <p class="form-control-static input_view"><?= $centro_costos ?></p>
                                </div>

                                <div class="form-group">
                                    <label>Descripci&oacute;n Centro de Costos</label>
                                    <p class="form-control-static input_view"><?= $descripcion_centro_costos ?></p>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tecer Columna-->
                            <div class="col-lg-4">
                                <!-- Fecha -->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"> <label>Fecha Emisi&oacute;n</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_emision)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_emision ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- P�liza en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>&iquest;Nota Salida en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Tercer Columna-->

                        </div>

                        <div class="row">
                            <div class="col-lg-8">
                                <label>Observaciones</label>
                                <p class="form-control-static input_view"><?= $observaciones ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">

                            <h4 id="suma_total" class="text-center"></h4>
                            <input type="hidden" value="" name="importe_total" id="importe_total" />

                            <table class="table table-striped table-bordered table-hover" id="tabla_detalle">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>F.F.</th>
                                    <th>P.F.</th>
                                    <th>C.C.</th>
                                    <th>Cap&iacute;tulo</th>
                                    <th>Concepto</th>
                                    <th>Partida</th>
                                    <th>Art&iacute;culo / CUCOP</th>
                                    <th>U/M</th>
                                    <th>Cantidad</th>
                                    <th>Precio U.</th>
                                    <th>Subtotal</th>
                                    <th>Importe</th>
                                    <th>T&iacute;tulo</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("patrimonio/nota_salida") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
        </div>
    </form>

</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->