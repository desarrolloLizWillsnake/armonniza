<h3 class="page-header center"><i class="fa fa-random"></i> Conciliación Bancaria</h3>
<input type="hidden" name="cuenta" id="cuenta" value="<?= $id_cuenta ?>" />
<div id="page-wrapper">
    <div class="row cont-btns-c center"  style="margin-top: 1%;">
        <div class="col-lg-12">
            <h3 class="text-center"><?= $banco ?></h3>
            <h4 class="text-center" style="margin-bottom: 3%; color: #848484;"><i class="fa fa-credit-card" style="font-size: 16px;"></i> <?= $cuenta ?></h4>
<!--            <a href="--><?//= base_url("ciclo/agregar_conciliacion/".$id_cuenta) ?><!--" class="btn btn-default"><i class="fa fa-plus circle" style="color: #B6CE33;"></i> Agregar Movimiento</a>-->
            <a href="<?= base_url("ciclo/imprimir_conciliacion/".$id_cuenta) ?>" class="btn btn-default"><i class="fa fa-print circle" style="color: #B6CE33;"></i> Imprimir Conciliación</a>
            <a href="<?= base_url("ciclo/exportar_conciliacion/".$id_cuenta) ?>" class="btn btn-default"><i class="fa fa-download" style="color: #B6CE33;"></i> Exportar Conciliación</a>
            <a class="btn btn-default" onclick="window.history.back();"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Sifirme</th>
                                <th width="6%">No.</th>
                                <th width="8%">Tipo</th>
                                <th width="16%">Concepto</th>
                                <th width="13%">Aplicado</th>
                                <th  width="13%">Neto</th>
                                <th width="10%">F. Emisión</th>
                                <th width="11.5%">F. Conciliación</th>
                                <th>No. Cuenta</th>
                                <th width="10%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar movimiento</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el movimiento seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_movimiento" id="cancelar_movimiento" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_movimiento">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_conciliar" tabindex="-1" role="dialog" aria-labelledby="modal_conciliar" aria-hidden="true" style="z-index: 1; padding-top: 5%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-money ic-modal"></i> Conciliar Movimiento Bancario</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 center" style="margin-bottom: 1%;">
                                <label for="message-text" class="control-label">Por favor, ingrese la fecha en que el movimiento fue conciliado.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_conciliar" id="fecha_conciliar" placeholder="aaaa-mm-dd" required="required">
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                        <input type="hidden" value="" name="conciliar_movimiento" id="conciliar_movimiento" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_conciliar_movimiento">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_quitar_conciliar" tabindex="-1" role="dialog" aria-labelledby="modal_quitar_conciliar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-money ic-modal"></i> Quitar Conciliación Movimiento Bancario</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se va a quitar la conciliacipon del movimiento seleccionado</label>
                        <input type="hidden" value="" name="quitar_conciliar_movimiento" id="quitar_conciliar_movimiento" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_quitar_conciliar_movimiento">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->