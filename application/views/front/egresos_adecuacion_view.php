<h3 class="page-header title center"><i class="fa fa-exchange"></i> Adecuaciones Presupuestarias Egresos</h3>
<div id="page-wrapper">

    <div class="row cont-btns center" style="margin-top: 6%;">
    <div class="col-lg-12">
        <button type="submit" id="ampliacion" class="btn btn-default"><i class="fa fa-arrow-up" style="color: #B6CE33;"></i> Ampliación</button>
        <button type="submit" id="reduccion" class="btn btn-default"><i class="fa fa-arrow-down" style="color: #B6CE33;"></i> Reducción</button>
        <button type="submit" id="transferencia" class="btn btn-default"><i class="fa fa-mail-forward" style="color: #B6CE33;"></i> Transferencia</button>
        <a href="<?= base_url("plantillas/plantilla_adecuacion.xls") ?>" class="btn btn-default"><i class="fa fa-download" style="color: #B6CE33;"></i> Descargar Plantilla</a>
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload" style="color: #B6CE33;"></i> Subir Archivo</button>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li id= "btn-todas" class="active"><a onclick="BtnTodas()">Todos</a></li>
                <li id= "btn-ampliaciones" role="presentation"><a onclick="BtnAmpliaciones()">Ampliaciones</a></li>
                <li id= "btn-reducciones" role="presentation"><a onclick="BtnReducciones()">Reducciones</a></li>
                <li id= "btn-transferencias" role="presentation"><a onclick="BtnTransferencias()">Transferencias</a></li>
            </ul>
            <div class="panel panel-default" id="tab-todas">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla table-cpago ajuste">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="6%">No.</th>
                                <th width="7.5%">Tipo</th>
                                <th width="10%">F. Solicitud</th>
                                <th width="11%">F. Aplicación</th>
                                <th >Clasificación</th>
                                <th>Importe</th>
                                <th>Creado por</th>
                                <th width="6.5%">Firme</th>
                                <th>Estatus</th>
                                <th width="10%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-ampliaciones" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_ampliaciones table-cpago ajuste" >
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>No.</th>
                                <th>Tipo</th>
                                <th>F. Solicitud</th>
                                <th>F. Aplicación</th>
                                <th>Clasificación</th>
                                <th>Importe</th>
                                <th>Creado por</th>
                                <th>Firme</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-reducciones" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_reducciones table-cpago ajuste">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>No.</th>
                                <th>Tipo</th>
                                <th>F. Solicitud</th>
                                <th>F. Aplicación</th>
                                <th>Clasificación</th>
                                <th>Importe</th>
                                <th>Creado por</th>
                                <th>Firme</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default" id="tab-transferencias" style="display: none;">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla_transferencias table-cpago ajuste">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>No.</th>
                                <th>Tipo</th>
                                <th>F. Solicitud</th>
                                <th>F. Aplicación</th>
                                <th>Clasificación</th>
                                <th>Importe</th>
                                <th>Creado por</th>
                                <th>Firme</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Adecuación</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="message-text" class="control-label">¿Realmente desea cancelar la adecucación seleccionada?</label>
                            <input type="hidden" value="" name="cancelar_adecuacion" id="cancelar_adecuacion" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_adecuacion">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo de Adecuación</h4>
                </div>
                <div class="modal-body">
                    <?php
                    $attributes = array(
                        'role' => 'form',
                    );

                    echo(form_open_multipart('egresos/subir_adecuacion', $attributes));
                    ?>
                    <div class="form-group center">
                        <div class="fileUpload btn btn-default btn-size">
                            <span>Selecciona Archivo</span>
                            <?php
                            $data = array(
                                'name'        => 'archivoSubir',
                                'id'          => 'archivoSubir',
                                'class'       => 'upload',
                                'accept' => 'application/vnd.ms-excel',
                                'required' => 'required',
                            );

                            echo(form_upload($data));
                            ?>
                        </div>
                        <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <?php
                    $atributos_submit = array(
                        'class' => 'btn btn-default',
                        'required' => 'required',
                    );

                    echo(form_submit($atributos_submit, 'Subir Archivo'));
                    ?>
                    <?php echo(form_close()); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade resultado" tabindex="-1" role="dialog" aria-labelledby="resultado" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-archive ic-modal"></i> Cancelar Adecuación</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="message-text" id="mensaje_resultado" class="control-label"></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- /#page-wrapper -->