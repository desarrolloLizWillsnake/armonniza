<h3 class="page-header title center"><i class="fa fa-pie-chart"></i> Consulta Balanza</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <a href="#" class="btn btn-default"><i class="fa fa-download circle" style="color: #B6CE33;" ></i> Exportar Excel</a>
            <a href="<?= base_url("contabilidad/imprimir_reporte_consulta_balanza")?>"  class="btn btn-default"><i class="fa fa-print circle" style="color: #B6CE33;" ></i> Imprimir</a>
            <a href="<?= base_url("contabilidad/balanza")?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-left circle" style="color: #B6CE33;" ></i> Regresar</a>
        </div>
    </div>

    <input type="hidden" name="subsidio" id="subsidio" value="<?= $subsidio ?>">
    <input type="hidden" name="centro_costos" id="centro_costos" value="<?= $centro_costos ?>">
    <input type="hidden" name="fecha_inicial" id="fecha_inicial" value="<?= $fecha_inicial ?>">
    <input type="hidden" name="fecha_final" id="fecha_final" value="<?= $fecha_final ?>">
    <input type="hidden" name="cuenta_inicial" id="cuenta_inicial" value="<?= $cuenta_inicial ?>">
    <input type="hidden" name="cuenta_final" id="cuenta_final" value="<?= $cuenta_final ?>">
    <input type="hidden" name="nivel_cuenta" id="nivel_cuenta" value="<?= $nivel_cuenta ?>">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="12%">No. Cuenta</th>
                                <th width="18%">Nombre Cuenta</th>
                                <th width="15%">Saldo Inicial</th>
                                <th width="15%">Debe</th>
                                <th width="15%">Haber</th>
                                <th width="15%">Saldo Final</th>
                                <!--<th width="15%">Subsidio</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

       <div class="row">
           <div class="col-lg-4 text-center">
               <h4>Debe</h4>
               <div style="border: 1px solid #ccc; border-radius: 4px 25px 4px 25px; width:80%; margin: 0 auto;">
                   <h4>$0.00</h4>
               </div>
           </div>
           <div class="col-lg-4 text-center"">
               <h4>Haber</h4>
               <div style="border: 1px solid #ccc; border-radius: 4px 25px 4px 25px; width:80%; margin: 0 auto;">
                   <h4>$0.00</h4>
               </div>
           </div>
           <div class="col-lg-4 text-center"00;">
               <h4>Diferencia</h4>
               <div style="border: 1px solid #B6CE33; border-radius: 4px 25px 4px 25px; width:80%; margin: 0 auto;">
                   <h4>$0.00</h4>
               </div>
           </div>

       </div>
   </div>

</div>

</div>