<h3 class="page-header title center"><i class="fa fa-eye"></i> Ver Recaudado</h3>
<div id="page-wrapper">
    <form class="" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo" id="ultimo" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!---No. Consecutivo-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Recaudado</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                    </div>
                                </div>
                                <!---No. Devengado-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Devengado</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_ver"><?= $numero_devengado ?></p></div>
                                    </div>
                                </div>
                                <!---Clasificación-->
                                <div class="form-group">
                                    <label>Clasificación</label>
                                    <?php if(isset($clasificacion)) { ?>
                                        <p class="form-control-static input_view"><?= $clasificacion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--No. Factura-->
                                <div class="form-group">
                                    <label>No. Factura</label>
                                    <?php if(isset($no_movimiento)) { ?>
                                        <p class="form-control-static input_view"><?= $no_movimiento ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <!---Clave Cliente-->
                                <div class="form-group">
                                    <label>Clave Cliente</label>
                                    <?php if(isset($clave_cliente)) { ?>
                                        <p class="form-control-static input_view"><?= $clave_cliente ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!---Cliente-->
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <?php if(isset($cliente)) { ?>
                                        <p class="form-control-static input_view"><?= $cliente ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!---Fecha de Solicitud-->
<!--                                <div class="form-group">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-lg-6"><label class="label-f">Fecha Solicitud</label></div>-->
<!--                                        <div class="col-lg-6">-->
<!--                                            --><?php //if(isset($fecha_solicitud)) { ?>
<!--                                                <p class="form-control-static input_view">--><?//= $fecha_solicitud ?><!--</p>-->
<!--                                            --><?php //} else { ?>
<!--                                                <p class="form-control-static input_view"></p>-->
<!--                                            --><?php //}  ?>
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <!--Fecha Aplicación-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Recaudado</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_recaudado)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_recaudado ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- Descripción General-->
                                <div class="form-group">
                                    <label>Descripción General</label>
                                    <?php if(isset($descripcion)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div
                                <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Tabla Detalle -->
        <!--
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <h4 id="suma_total" class="text-center"></h4>
                            <input type="hidden" value="" name="subtotal_hidden" id="subtotal_hidden" />
                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_recaudado">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th width="8%">Gerencia</th>
                                    <th width="5%">C.C.</th>
                                    <th width="6%">Rubro</th>
                                    <th width="5%">Tipo</th>
                                    <th width="7%">Clase</th>
                                    <th width="9%">F. Aplicada</th>
                                    <th width="10%">Forma Pago</th>
                                    <th width="11%">Subtotal No Gravado</th>
                                    <th width="11%">Descuento No Gravado</th>
                                    <th width="11%">Subtotal Gravado</th>
                                    <th width="11%">Descuento Gravado</th>
                                    <th width="11%">IVA</th>
                                    <th width="12%">Importe</th>
                                    <th width="9%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        -->
        <!-- Tabla Detalle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <h4 id="suma_total" class="text-center"></h4>
                            <input type="hidden" value="" name="importe_total" id="importe_total" />
                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_recaudado">
                                <thead>
                                <th>ID</th>
                                <th width="8%">Gerencia</th>
                                <th width="5%">C.R.</th>
                                <th width="6%">Rubro</th>
                                <th width="5%">Tipo</th>
                                <th width="6%">Clase</th>
                                <th width="9%">F. Aplicada</th>
                                <th width="11%">Forma Pago</th>
                                <th width="5.5%">Cant.</th>
                                <th width="11%">Importe</th>
                                <th width="11%">IVA</th>
                                <th width="11%">Total</th>
                                <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="btns-finales text-center">
            <a href="<?= base_url("recaudacion/recaudado") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
        </div>

    </form>
</div>
