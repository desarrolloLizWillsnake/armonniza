<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Agregar / Editar Póliza</h3>
<div id="page-wrapper">
    <form class="forma_movimiento" role="form" action="<?= base_url("ciclo/actualizar_poliza") ?>" method="POST">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Polizas
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="movimiento" id="movimiento" value="<?= $movimiento ?>">
                        <input type="hidden" name="id_cuenta" id="id_cuenta" value="<?= $id_cuenta ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-5">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control ic-buscar-input" name="cuenta_cargo" id="cuenta_cargo" placeholder="Cuenta Cargo" <?= $cuenta_cargo != NULL ? ' value="'.$cuenta_cargo.'"' : '';?> required="required"/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_cargo">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <input type="text" class="form-control" name="descripcion_cuenta_cargo" id="descripcion_cuenta_cargo" placeholder="Descripción Cuenta Cargo" <?= $descripcion_cuenta_cargo != NULL ? ' value="'.$descripcion_cuenta_cargo.'"' : '';?> required="required"/>
                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-2"></div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-5">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control ic-buscar-input" name="cuenta_abono" id="cuenta_abono" placeholder="Cuenta Abono" <?= $cuenta_abono != NULL ? ' value="'.$cuenta_abono.'"' : '';?> required="required"/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta_abono">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <input type="text" class="form-control" name="descripcion_cuenta_abono" id="descripcion_cuenta_abono" placeholder="Descripción Cuenta Abono" <?= $descripcion_cuenta_abono != NULL ? ' value="'.$descripcion_cuenta_abono.'"' : '';?> required="required"/>
                            </div>
                            <!--Fin Tercer Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="form-group c-firme">
                            <h3>¿Movimiento en Firme?</h3>
                            <label class="checkbox-inline">
                                <input type="hidden" id="firme" name="firme" <?= $enfirme == 1 ? ' value="1"' : 'value="0"';?> />
                                <input type="checkbox" id="check_firme" name="check_firme" <?= $enfirme == 1 ? ' checked="checked" disabled' : '';?> />Sí
                            </label>
                        </div>
                    </div>
                </div>

                <div class="btns-finales text-center">
                    <div id="resultado_insertar_caratula"></div>
                    <a class="btn btn-default" onclick="window.history.back();">Cancelar</a>
                    <input type="submit" class="btn btn-green" value="Guardar Póliza" id="guardar_movimiento" name="guardar_movimiento" />
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal Cuenta Cargo -->
<div class="modal fade" id="modal_cuenta_cargo" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_cargo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-arrow-circle-down ic-modal"></i> Cuenta Cargo</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_cargo">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Abono -->
<div class="modal fade" id="modal_cuenta_abono" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_abono" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-arrow-circle-up ic-modal"></i> Cuenta Abono</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-4">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_abono">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->