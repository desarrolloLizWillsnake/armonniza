<h3 class="page-header title center"><i class="fa fa-area-chart"></i> Editar Matriz</h3>
<div id="page-wrapper">
    <form class="forma_compromiso" role="form">
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" value="<?= $id_correlacion_partidas_contables ?>" name="matriz" id="matriz" />
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-3 text-right">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cog"><i class="fa fa-search"></i> COG</button>
                            </div>
                            <div class="col-lg-3 text-left">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cri"><i class="fa fa-search"></i> CRI</button>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>

                        <br />

                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="clave" id="clave" placeholder="Clave" <?= $clave != NULL ? ' value="'.$clave.'"' : '';?> />
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-2"></div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="titulo_clave" id="titulo_clave" placeholder="Descripción de clave" <?= $descripcion != NULL ? ' value="'.$descripcion.'"' : '';?> />
                            </div>
                            <!--Fin Tercera Columna-->

                        </div>

                        <br />

                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="cuenta_cargo" id="cuenta_cargo" placeholder="Cuenta Cargo" <?= $cuenta_cargo != NULL ? ' value="'.$cuenta_cargo.'"' : '';?> />
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-2 text-center">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cuenta_cargo"><i class="fa fa-search"></i></button>
                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="descripcion_cuenta_cargo" id="descripcion_cuenta_cargo" placeholder="Descripción de Cuenta Cargo" <?= $nombre_cargo != NULL ? ' value="'.$nombre_cargo.'"' : '';?> />
                            </div>
                            <!--Fin Tercera Columna-->
                        </div>

                        <br />

                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="cuenta_abono" id="cuenta_abono" placeholder="Cuenta Abono" <?= $cuenta_abono != NULL ? ' value="'.$cuenta_abono.'"' : '';?> />
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-2 text-center">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_cuenta_abono"><i class="fa fa-search"></i></button>
                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="descripcion_cuenta_abono" id="descripcion_cuenta_abono" placeholder="Descripción de Cuenta Abono" <?= $nombre_abono != NULL ? ' value="'.$nombre_abono.'"' : '';?> />
                            </div>
                            <!--Fin Tercera Columna-->
                        </div>

                        <br />

                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-12">
                                <select class="form-control" id="destino" name="destino">
                                    <option value="">Destino</option>
                                    <option value="Devengado Gasto"<?= $destino == 'Devengado Gasto' ? ' selected="selected"' : '';?>>Devengado Gasto</option>
                                    <option value="Pagado Gasto"<?= $destino == 'Pagado Gasto' ? ' selected="selected"' : '';?>>Pagado Gasto</option>
                                    <option value="Ingreso Devengado"<?= $destino == 'Ingreso Devengado' ? ' selected="selected"' : '';?>>Ingreso Devengado</option>
                                    <option value="Ingreso Recaudado"<?= $destino == 'Ingreso Recaudado' ? ' selected="selected"' : '';?>>Ingreso Recaudado</option>
                                </select>
                            </div>
                            <!--Fin Primera Columna-->

                        </div>

                        <br />

                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-12">
                                <textarea style="height: 9em;" class="form-control" id="caracteristicas" name="caracteristicas" placeholder="Caracteristicas" <?= $caracteristicas != NULL ? '> '.$caracteristicas.'</textarea>' : '> </textarea>' ;?>
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-12">
                                <textarea style="height: 9em;" class="form-control" id="medio" name="medio" placeholder="Medio" <?= $medio != NULL ? '> '.$medio.'</textarea>' : '> </textarea>' ;?>
                            </div>
                            <!--Fin Segunda Columna-->

                        </div>

                        <br />

                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-12">
                                <!-- Tipo de Gasto-->
                                <label>Tipo de Gastos</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios1" value="1" <?= $tipo_gasto == '1' ? ' checked="checked"' : '';?>>Gasto Corriente
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios2" value="2" <?= $tipo_gasto == '2' ? ' checked="checked"' : '';?>>Gasto de Capital
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_gasto" id="optionsRadios3" value="3" <?= $tipo_gasto == '3' ? ' checked="checked"' : '';?>>Amortización de la cuenta y disminución de pasivos
                                    </label>
                                </div>
                            </div>
                            <!--Fin Primera Columna-->

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("contabilidad/matriz") ?>" >Cancelar</a>
            <a id="actualizar_matriz" class="btn btn-green">Actualizar Matriz</a>
        </div>
    </form>
</div>

<!-- Modal COG -->
<div class="modal fade" id="modal_cog" tabindex="-1" role="dialog" aria-labelledby="modal_cog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> COG</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="no_cog" id="no_cog" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cog">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Capítulo</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal CRI-->
<div class="modal fade" id="modal_cri" tabindex="-1" role="dialog" aria-labelledby="modal_cri" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> CRI</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="no_cog" id="no_cog" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cri">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Titulo</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Cargo-->
<div class="modal fade" id="modal_cuenta_cargo" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_cargo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> Catálogo de Cuentas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="hidden_cuenta_cargo" id="hidden_cuenta_cargo" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_cargo">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>No. Cuenta</th>
                            <th>Descripción</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta Abono-->
<div class="modal fade" id="modal_cuenta_abono" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta_abono" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file ic-modal"></i> Catálogo de Cuentas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <input type="hidden" name="hidden_cuenta_abono" id="hidden_cuenta_abono" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuenta_abono">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>No. Cuenta</th>
                            <th>Descripción</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>