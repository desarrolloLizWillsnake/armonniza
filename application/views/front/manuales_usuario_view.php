<h3 class="page-header title center">  <img src="<?= base_url('img/logo_small.png') ?>" class="logo-ar-titulo"/> Manuales de usuario Armonniza</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group" style="margin: 3%;">
                <!--Manual Usuario 1-->
                <a href="<?= base_url("documentos/Guia_General_Programa_Armonniza.pdf") ?>" class="list-group-item">
                    <div class="row">
                        <div class="col-lg-2"><i class="fa fa-leanpub" style="color: #61AB59;"></i></div>
                    <div class="col-lg-10 text-left">Guía General Programa Armonniza</div>
                    </div>
                </a>
                <!--Manual Usuario 2-->
                <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_Estructura_Administrativa_Egresos.pdf") ?>" class="list-group-item">
                    <div class="row">
                        <div class="col-lg-2"><i class="fa fa-leanpub" style="color: #002E6E;"></i></div>
                        <div class="col-lg-10 text-left">Guía de Registro y Captura Estructura Administrativa Egresos</div>
                    </div>
                </a>
                <!--Manual Usuario 3-->
                <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_de_PreCompromisos.pdf") ?>" class="list-group-item">
                    <div class="row">
                        <div class="col-lg-2"><i class="fa fa-leanpub" style="color: #61AB59;"></i></div>
                        <div class="col-lg-10 text-left">Guía de Registro y Captura de PreCompromisos</div>
                    </div>
                </a>
                <!--Manual Usuario 4-->
                <a href="<?= base_url("documentos/Guia_de_Registro_y_Captura_de_Compromisos.pdf") ?>" class="list-group-item">
                    <div class="row">
                        <div class="col-lg-2"><i class="fa fa-leanpub" style="color: #002E6E;"></i></div>
                        <div class="col-lg-10 text-left">Guía de Registro y Captura de Compromisos</div>
                    </div>
                </a>
                <!--Manual Usuario 5-->
                <a href="" class="list-group-item">
                    <div class="row">
                        <div class="col-lg-2"><i class="fa fa-leanpub" style="color: #61AB59;"></i></div>
                        <div class="col-lg-10 text-left">Guía de Registro y Captura de Contra Recibos de Pago</div>
                    </div>
                </a>
                <!--Manual Usuario 6-->
                <a href="" class="list-group-item">
                    <div class="row">
                        <div class="col-lg-2"><i class="fa fa-leanpub" style="color: #002E6E;"></i></div>
                        <div class="col-lg-10 text-left">Guía de Registro y Captura de Movimientos Bancarios</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

</div>