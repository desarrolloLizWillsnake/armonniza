<h3 class="page-header title center"><i class="fa fa-book"></i> Ver Póliza</h3>
<div id="page-wrapper">

    <form class="forma_poliza_general" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">General</div>
                    <div class="panel-body">
                        <input type="hidden" name="ultima_poliza" id="ultima_poliza" value="<?= $numero_poliza ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-3">
                                <!---Tipo Poliza-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>Tipo Póliza</label></div>
                                        <div class="col-lg-6"> <p class="form-control-static input_view"><?= $tipo_poliza ?></p></div>
                                    </div>
                                </div>
                                <!---No. Poliza-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Póliza</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_view"><?= $numero_poliza ?></p></div>
                                    </div>
                                </div>
                                <?php if($poliza_contrarecibo != 0) { ?>
                                    <!---No. Póliza Diario-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Póliza de Diario</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $poliza_contrarecibo ?></p></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if($poliza_devengado != 0) { ?>
                                    <!---No. Póliza Diario-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Póliza de Diario</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $poliza_devengado ?></p></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!---No. Contra Recibo de Pago-->
                                <?php if($contrarecibo != 0) { ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Contra Recibo de Pago</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_view"><?= $contrarecibo ?></p></div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if($movimiento != 0) { ?>
                                <!---No. Movimiento Bancario-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Movimiento Bancario</label></div>
                                        <div class="col-lg-6"><p class="form-control-static input_view"><?= $movimiento ?></p></div>
                                    </div>
                                </div>
                                <?php } ?>
                                <!---No. Devengado-->
                                <?php if($no_devengado != 0) { ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Devengado</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $no_devengado ?></p></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!---No. Recaudado-->
                                <?php if($no_recaudado != 0) { ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Recaudado</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $no_recaudado ?></p></div>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-6">
                                <!-- Clave Proveedor -->
                                <?php if($id_proveedor != NULL) { ?>
                                    <div class="form-group">
                                        <label>Clave Proveedor / Cliente</label>
                                        <?php if(isset($id_proveedor)) { ?>
                                            <p class="form-control-static input_view"><?= $id_proveedor ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                    </div>
                                <?php } ?>
                                <!-- Proveedor -->
                                <?php if($proveedor != NULL) { ?>
                                <div class="form-group">
                                    <label>Proveedor / Cliente</label>
                                    <?php if(isset($proveedor)) { ?>
                                        <p class="form-control-static input_view"><?= $proveedor ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <?php } ?>
                                <!---Clave Cliente-->
                                <?php if($clave_cliente != NULL) { ?>
                                    <div class="form-group">
                                        <label>Clave Proveedor / Cliente</label>
                                        <p class="form-control-static input_view"><?= $clave_cliente ?></p>
                                    </div>
                                <?php } ?>
                                <!---Clave Cliente-->
                                <?php if($cliente != NULL) { ?>
                                    <div class="form-group">
                                        <label>Proveedor / Cliente</label>
                                        <p class="form-control-static input_view"><?= $cliente ?></p>
                                    </div>
                                <?php } ?>
                                <!---Campo Vacío-->
                                <?php if($id_proveedor == NULL && $proveedor == NULL && $clave_cliente == NULL && $cliente == NULL) { ?>
                                    <div class="form-group">
                                        <label>Clave Proveedor / Cliente</label>
                                        <p class="form-control-static input_view"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Proveedor / Cliente</label>
                                        <p class="form-control-static input_view"></p>
                                    </div>
                                <?php } ?>
                                <!-- Importe -->
                                <div class="form-group">
                                    <label>Importe</label>
                                    <?php if(isset($importe)) { ?>
                                        <p class="form-control-static input_view">$ <?= $this->cart->format_number(round($importe, 2)) ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                             </div>
                            <!--Fin Segunda Columna-->
                            <!--Tecer Columna-->
                            <div class="col-lg-3">
                                <!-- Fecha -->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-3"> <label>Fecha</label></div>
                                        <div class="col-lg-9">
                                            <?php if(isset($fecha)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!---No. Factura-->
                                <?php if($no_movimiento != NULL) { ?>
                                    <div class="form-group">
                                        <label>No. Factura</label>
                                        <p class="form-control-static input_view"><?= $no_movimiento ?></p>
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group">
                                        <label>No. Factura</label>
                                        <p class="form-control-static input_view"></p>
                                    </div>
                                <?php } ?>

                                <!-- Póliza en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>¿Póliza en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <!--Fin Tercer Columna-->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Concepto -->
                                    <div class="form-group" style="margin: 0% 1.5%;">
                                        <label>Concepto Específico</label>
                                        <?php if(isset($concepto_especifico)) { ?>
                                            <p style="padding: 1%;"class="form-control-static input_view"><?= $concepto_especifico ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                    </div>
                                    <!-- Concepto -->
                                    <div class="form-group" style="margin: 0% 1.5%;">
                                        <label>Concepto</label>
                                        <?php if(isset($concepto)) { ?>
                                            <p style="padding: 1%;"class="form-control-static input_view"><?= $concepto ?></p>
                                        <?php } else { ?>
                                            <p class="form-control-static input_view"></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover datos_tabla">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Cuenta</th>
                                    <th width="6%">C.C.</th>
                                    <th>Partida</th>
                                    <th>Subsidio</th>
                                    <th>Descripción Cuenta</th>
                                    <th width="14%">Debe</th>
                                    <th width="14%">Haber</th>
                                    <th width="7%">No. Póliza</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <h5 id="sumas_totales" class="text-center"></h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("contabilidad/polizas") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
        </div>
    </form>

</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->