<h3 class="page-header title center"><i class="fa fa-tasks"></i> Kardex de Existencias</h3>
<div id="page-wrapper">
    <div class="row" style="margin-top: 10%;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th width="9%">No. Entrada</th>
                                <th width="8%">No. Salida</th>
                                <th width="7.5%">Operación</th>
                                <th width="10%">Clave</th>
                                <th>Descripción</th>
                                <th width="8%">Fecha</th>
                                <th width="5%">U/M</th>
                                <th width="6.5%">Entrada</th>
                                <th width="6%">Salida</th>
                                <th width="8%">Existencia</th>
                                <th>Precio Unitario</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--    <div class="row">-->
<!--        <div class="col-lg-12">-->
<!--            <div class="panel panel-default">-->
<!--                <div class="panel-body table-gral">-->
<!--                    <div class="table-responsive">-->
<!--                        <table class="table table-striped table-bordered table-hover datos_tabla">-->
<!--                            <thead>-->
<!--                            <tr>-->
<!--                                <th>F.F.</th>-->
<!--                                <th>Clave</th>-->
<!--                                <th>Tipo/Clase</th>-->
<!--                                <th>Operación</th>-->
<!--                                <th>Movimiento</th>-->
<!--                                <th>Fecha</th>-->
<!--                                <th>Hora</th>-->
<!--                                <th>Entra</th>-->
<!--                                <th>Salida</th>-->
<!--                                <th>Existencia</th>-->
<!--                                <th>C. Unitario</th>-->
<!--                                <th>C. Promedio</th>-->
<!--                            </tr>-->
<!--                            </thead>-->
<!--                            <tbody>-->
<!--                            </tbody>-->
<!--                        </table>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>