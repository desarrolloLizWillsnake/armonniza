    <h3 class="page-header title center"><i class="fa fa-book"></i> Catálogo Proveedores</h3>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <table id="tabla_proveedores" class="table table-striped table-bordered table-hover datos_tabla">
                                <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Nombre</th>
                                    <th>Calle</th>
                                    <th>No. Exterior</th>
                                    <th>No. Interior</th>
                                    <th>Colonia</th>
                                    <th>Teléfono</th>
                                    <th>RFC</th>
                                    <th>No. Cuenta</th>
                                    <th>CLABE</th>
                                    <th>Pyme</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->