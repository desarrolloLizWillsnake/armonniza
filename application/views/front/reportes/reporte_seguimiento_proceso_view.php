<h3 class="page-header title center"><i class="fa fa-files-o"></i> Consulta Seguimiento de Proceso</h3>
<div id="page-wrapper">
    <form action="<?= base_url("reportes/exportar_reporte_seguimientofinal") ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Tipo de Consulta-->
                        <label style="margin-top: 1%;">Seleccione el tipo de consulta</label>
                        <div class="row">
                            <div class="col-lg-6" >
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="opciones" id="general" value="gral" checked>General
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="opciones" id="especifica" value="espec">Específica
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>


                        <div class="row niveles-pc" id="consulta_especifica" name="consulta_especifica" style="margin-top: 3%; display: none;">
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="hidden" name="id_precompromiso" id="id_precompromiso" value="">
                                    <input type="text" class="form-control marg_sup" name="precompromiso" id="precompromiso" placeholder="No. Precompromiso">
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default b-espec" type="button" data-toggle="modal" data-target="#modal_precompromisos"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="hidden" name="id_compromiso" id="id_compromiso" value="">
                                    <input type="text" class="form-control marg_sup" name="compromiso" id="compromiso" placeholder="No. Compromiso">
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default b-espec" type="button" data-toggle="modal" data-target="#modal_compromisos"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesAdministracion") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal Precompromisos -->
<div class="modal fade" id="modal_precompromisos" tabindex="-1" role="dialog" aria-labelledby="modal_precompromisos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o ic-modal"></i> Precompromisos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-9">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_precompromiso" id="hidden_precompromiso" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_precompromisos">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th width="5%">No.</th>
                            <th width="10%">Tipo</th>
                            <th width="10%">F. Emisión</th>
                            <th width="10%">F. Entrega</th>
                            <th width="10%">Importe</th>
                            <th width="10%">Creado Por</th>
                            <th width="10%">Firme</th>
                            <th width="10%">Estatus</th>
                            <th width="9%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Compromisos -->
<div class="modal fade" id="modal_compromisos" tabindex="-1" role="dialog" aria-labelledby="modal_compromisos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-text-o ic-modal"></i> Compromisos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_compromisos" id="hidden_compromisos" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_compromisos">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th width="5%">No.</th>
                            <th width="10%">Tipo</th>
                            <th width="10%">F. Emisión</th>
                            <th width="10%">F. Entrega</th>
                            <th width="10%">Importe</th>
                            <th width="10%">Creado Por</th>
                            <th width="10%">Firme</th>
                            <th width="10%">Estatus</th>
                            <th width="9%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>