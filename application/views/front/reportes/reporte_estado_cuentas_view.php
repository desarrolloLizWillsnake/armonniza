<h3 class="page-header title center"><i class="fa fa-files-o"></i> Estado de Cuentas</h3>
<div id="page-wrapper">
    <form class="" action="" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Tipo Adeacuación-->
                        <select class="form-control" id="tipo_categoria" name="tipo_adecuacion">
                            <option value="">Nivel de Cuenta Emisión</option>
                            <option value="Genero">Género</option>
                            <option value="Grupo">Grupo</option>
                            <option value="Rubro">Rubro</option>
                            <option value="Cuenta">Cuenta</option>
                            <option value="Subcuenta">Sub-Cuenta</option>
                            <option value="Todos">Todos</option>
                        </select>
                        <!--Persona que elaboró-->
                        <div class="form-group">
                            <select class="form-control" name="persona" id="persona">
                                <option value="">Persona que elaboró</option>
                                <option value="Angelica Caballero">Angelica Caballero</option>
                                <option value="Lizbeth Luna">Lizbeth Luna</option>
                                <option value="Raúl Sánchez">Raúl Sánchez</option>
                            </select>
                        </div>
                        <!--Rango Cuentas -->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="hidden" name="" id="" value=""/>
                                    <input type="text" class="form-control" name="" id="" style="margin-top: -1%;"  placeholder="No. Cuenta Inicial" disabled />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="hidden" name="" id="" value=""/>
                                    <input type="text" class="form-control" name="" id="" style="margin-top: -1%;" placeholder="No. Cuenta Final" disabled />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <!--Rango Fechas-->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>