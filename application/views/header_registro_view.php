<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Registrar Usuario</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url("assets/templates/front/css/bootstrap.css") ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/metisMenu/metisMenu.min.css") ?>" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/timeline.css") ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url("assets/templates/front/css/sb-admin-2.css") ?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/morris.css") ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url("assets/templates/front/font-awesome-4.1.0/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url('img/logo.png') ?>"></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

        </ul>
        <!-- /.navbar-top-links -->
        <!-- Inicio Menú Lateral   -->
        <div class="navbar-default sidebar nav-side" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>


    <!-- Fin de la sección del menú   -->