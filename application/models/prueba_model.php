<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prueba_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function insertar_imagen($datos_imagen) {
		// $this->debugeo->imprimir_pre($datos_imagen);

		$this->db->trans_begin();

		$nombre_archivo = $datos_imagen["raw_name"]."_thumb".$datos_imagen["file_ext"];
		$ruta = "uploads/".$nombre_archivo;

		$datos_insertar = array(
			'ruta_imagen' => $ruta,
			);

		$this->db->insert('prueba_imagen', $datos_insertar);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		}
		else {
			$this->db->trans_commit();
			return TRUE;
		}

	}
	

}

/* End of file prueba_model.php */
/* Location: ./application/models/prueba_model.php */