<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patrimonio_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /*Nota de Entrada*/

    function ultima_nota_entrada() {
        $sql = "SELECT numero_movimiento as ultimo FROM mov_nota_entrada_caratula  ORDER BY numero_movimiento DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartar_nota_entrada($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_movimiento' => $ultimo,
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_nota_entrada_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function tabla_nota_entrada_principal() {
        $this->db->select('*')->from('mov_nota_entrada_caratula')->order_by("numero_movimiento", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function copiarDatosContrarecibo($datos) {

        $this->db->delete('mov_nota_entrada_detalle', array('numero_movimiento' => $datos['ultima_nota']));

        $sql_detalle = "SELECT * FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_detalle = $this->db->query($sql_detalle, array($datos['compromiso']));
        $result_detalle = $query_detalle->result_array();

        foreach($result_detalle as $key => $value) {

//            Se el resultado que sea, se inserta el detalle la table del detalle del compromiso
            $sql_insertar_detalle = "INSERT INTO mov_nota_entrada_detalle (numero_movimiento, id_nivel, estructura, articulo, unidad_medida,
                                                            cantidad, p_unitario, subtotal, iva, importe,
                                                            titulo, clave, descripcion, entregada, observaciones) VALUES (
                                                            ?, ?, ?, ?, ?,
                                                            ?, ?, ?, ?, ?,
                                                            ?, ?, ?, ?, ?);";
            $this->db->query($sql_insertar_detalle, array($datos['ultima_nota'], $value["id_nivel"], $value["nivel"], $value["gasto"], $value["unidad_medida"],
                                                        $value["cantidad"], $value["p_unitario"], $value["subtotal"], $value["iva"], $value["importe"],
                                                        $value["titulo"], $value["gasto"], $value["especificaciones"], 0, $value["especificaciones"]));

        }

        $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($datos['contrarecibo']));
        $result = $query->row();

        $datos_actualizar = array(
            'contrarecibo' => $result->id_contrarecibo_caratula,
            'compromiso' => $result->numero_compromiso,
            'id_proveedor' => $result->id_proveedor,
            'proveedor' => $result->proveedor,
            'importe' => $result->importe,
            'documento' => $result->tipo_documento,
            'no_documento' => $result->documento,
            'observaciones' => $result->descripcion,
        );

        $this->db->where('numero_movimiento', $datos['ultima_nota']);
        $resultado = $this->db->update('mov_nota_entrada_caratula', $datos_actualizar);

//        $this->debugeo->imprimir_pre($result);

        if($resultado) {
            return $datos_actualizar;
        }
        else {
            return FALSE;
        }
    }

    function insertar_nota_entrada_detalle($datos, $query) {

        $this->db->trans_begin();

        $this->db->query($query, array(
            $datos["ultima_nota"],
            $datos["id_nivel"],
            $datos["gasto"],
            $datos["u_medida"],
            $datos["cantidad"],
            $datos["precio"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            $datos["descripcion_detalle"],
            $datos["tipo_poliza"],
            $datos["descripcion_detalle"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"],
            $datos["titulo_gasto"]));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertar_nota_entrada_detalle_archivo($datos, $query) {

        $this->db->trans_begin();

        $this->db->query($query, array(
            $datos["ultima_nota"],
            $datos["id_nivel"],
            $datos["G"],
            $datos["u_medida"],
            $datos["I"],
            $datos["J"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            $datos["L"],
            $datos["L"],
            $datos["A"],
            $datos["B"],
            $datos["C"],
            $datos["D"],
            $datos["E"],
            $datos["F"],
            $datos["titulo_gasto"]));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function tomar_unidad_medida($u_medida) {
        $this->db->select('clave')->from('cat_unidad_medida')->where('descripcion', $u_medida);
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado["clave"];
    }

    function datosNotaEntradaDetalle($nota, $query) {
        $query = $this->db->query($query, array($nota));
        $result = $query->result();
        return $result;
    }

    function borrarDetalleNotaEntrada($nota) {
        $this->db->where('id_nota_entrada_detalle', $nota);
        $query = $this->db->delete('mov_nota_entrada_detalle');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_nota_entrada($datos) {

//        Se inicia una transacción en la base de datos
        $this->db->trans_begin();

//        En caso de que el importe sea igual a 0, se inicia en 0
        if(!$datos["importe_total"]) {
            $datos["importe_total"] = 0;
        }

        $total_contrarecibo = 0;

//        Si la nota de entrada viene de un contrarecibo de pago, se toma el total del importe y se guarda en una variable
        if(isset($datos["no_contrarecibo"]) && $datos["no_contrarecibo"] != "" && $datos["no_contrarecibo"] != NULL && $datos["no_contrarecibo"] != 0) {
            $this->db->select('importe')->from('mov_contrarecibo_caratula')->where('id_contrarecibo_caratula', $datos["no_contrarecibo"]);
            $query = $this->db->get();
            $total = $query->row();
            $total_contrarecibo = $total->importe;

//            Se revisa que la nota de entrada esté en firme, en caso de que lo sea, se marca el contrarecibo como usado para no poder usarlo dos veces
            if($datos["check_firme"] == 1) {

                $datos_contra = array(
                    "usado_patrimonio" => 1,
                );

                $this->db->where('id_contrarecibo_caratula', $datos["no_contrarecibo"]);
                $this->db->update('mov_contrarecibo_caratula', $datos_contra);
            }

        }

//        Este arreglo son los datos que van a ser insertados en la caratula de la nota de entrada
        $data = array(
            "fecha_emision" => $datos["fecha"],
            "fecha_emision_real" => "NOW()",
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "documento" => $datos["tipo_documento"],
            "no_documento" => $datos["documento"],
            "compromiso" => $datos["compromiso"],
            "pedido" => $datos["no_contrarecibo"],
            "importe" => $datos["importe_total"],
            "enfirme" => $datos["check_firme"],
            "total_contrarecibo" => $total_contrarecibo,
            "cancelada" => 0,
            "tipo_poliza" => $datos["tipo_poliza"],
            "observaciones" => $datos["observaciones"],
            "estatus" => $datos["estatus"],
        );

//        Esta función es la que se encarga de actualizar la nota de entrada
        $this->db->where('numero_movimiento', $datos["ultima_nota"]);
        $this->db->update('mov_nota_entrada_caratula', $data);

//        De lo contrario devuelve una variable booleana falsa
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertar_caratula_nota_entrada_archivo($datos) {

//        Se inicia una transacción en la base de datos
        $this->db->trans_begin();

//        Este arreglo son los datos que van a ser insertados en la caratula de la nota de entrada
        $data = array(
            "fecha_emision" => $datos["B"],
            "fecha_emision_real" => "NOW()",
            "id_proveedor" => "",
            "proveedor" => "",
            "documento" => $datos["C"],
            "no_documento" => $datos["D"],
            "compromiso" => 0,
            "pedido" => 0,
            "importe" => $datos["importe_total"],
            "enfirme" => 0,
            "total_contrarecibo" => 0,
            "cancelada" => 0,
            "tipo_poliza" => $datos["A"],
            "observaciones" => $datos["E"],
            "estatus" => "espera",
        );

//        Esta función es la que se encarga de actualizar la nota de entrada
        $this->db->where('numero_movimiento', $datos["ultima_nota"]);
        $this->db->update('mov_nota_entrada_caratula', $data);

//        De lo contrario devuelve una variable booleana falsa
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function get_datos_nota_entrada_caratula($nota_etrada, $query) {
        $query = $this->db->query($query, array($nota_etrada));
        $result = $query->row();
        return $result;
    }

    function datos_nota_entrada($nota_etrada, $query){
        $query = $this->db->query($query, array($nota_etrada));
        $result = $query->result();
        return $result;
    }

    function cancelarNotaEntradaCaratula($nota_entrada, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $nota_entrada,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_nota_entrada_kardex($datos){

        $this->db->trans_begin();

        try {

            $this->db->select('*')->from('mov_nota_entrada_detalle')->where('numero_movimiento', $datos["ultima_nota"]);
            $query = $this->db->get();
            $resultado_detalle = $query->result_array();

            foreach($resultado_detalle as $key => $value) {
                // Se toma la existencia y la cantidad promedio del articulo actual
                $this->db->select('id_conceptos_gasto AS ID, existencia, promedio')
                            ->from('cat_conceptos_gasto')
                            ->where('articulo', $value["articulo"]);
                $query = $this->db->get();
                $existencia_detalle = $query->row_array();

                // Se suma a la cantidad existente, la cantidad de cada detalle de la nota
                $existencia_detalle["existencia"] += $value["cantidad"];

                // Se actualiza la existencia del articulo en el catalogo de productos
                $datos_actualizar = array(
                   'existencia' => $existencia_detalle["existencia"],
                   'promedio' => $existencia_detalle["promedio"],
                );

                $this->db->where('articulo', $value["articulo"]);
                $this->db->update('cat_conceptos_gasto', $datos_actualizar); 

                // Se insertan los datos de la nota en le kardex del inventario
                $datos_insertar = array(
                    'id_nivel' => $value["id_nivel"],
                    'estructura' => $value["estructura"],
                    'id_articulo' => $existencia_detalle["ID"],
                    'articulo' => $value["articulo"],
                    'fecha' => $datos["fecha"],
                    'fecha_sql' => "NOW()",
                    'hora' => "NOW()",
                    'operacion' => "Entrada",
                    'movimiento' => $datos["ultima_nota"],
                    'entra' => $value["cantidad"],
                    'sale' => 0,
                    'existe' => $existencia_detalle["existencia"],
                    'unidad_medida' => $value["unidad_medida"],
                    'id_proveedor' => $datos["id_proveedor"],
                    'proveedor' => $datos["proveedor"],
                    'precio_promedio' => $value["importe"],
                    'precio_unitario' => $value["p_unitario"],
                    'descripcion' => $value["titulo"],
                    'observaciones' => $value["observaciones"],
                );

                $this->db->insert('mov_kardex_almacen', $datos_insertar);

            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            }
            else {
                $this->db->trans_commit();
                return TRUE;
            }

        } catch (Exception $e) {
            return FALSE;
        }

    }

    function datos_de_partida($datos, $nombre) {
        $sql = "SELECT id_niveles AS ID, COLUMN_JSON(nivel) as estructura
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[5]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1"], $datos["nivel2"], $datos["nivel3"], $datos["nivel4"], $datos["nivel5"], $datos["nivel6"]));
        $resultado = $query->row_array();
        return $resultado;
    }

    /**
     * Esta función sirve para poder verificar que el centro de costos que ingresa el usuario, exista en la base de datos
     * @param  string $centro_costos Este es el centro de costos que ingresa el usuario para verificar
     * @return array                Devuelve el valor encontrado
     */
    function verificar_centro_costos($centro_costos) {
        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_costos' as char) AS centro_costos
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'centro_de_costos' as char) = ?;";
        $query = $this->db->query($sql, array($centro_costos));
        return $query->row_array();
    }

    /**
     * Esta función sirve para poder verificar que el articulo que ingresa el usuario, exista en la base de datos
     * @param  string $articulo Este es el articulo que ingresa el usuario para verificar
     * @return array                Devuelve el valor encontrado
     */
    function verificar_articulo($articulo) {
        $sql = "SELECT articulo, descripcion
                FROM cat_conceptos_gasto
                WHERE articulo = ?;";
        $query = $this->db->query($sql, array($articulo));
        return $query->row_array();
    }

    /**
     * Esta función sirve para poder actualizar los datos de la línea del detalle de las notas de entrada
     * @param  stdObject $datos Es un arreglo tipo std que contiene los datos de la línea que se va a actualizar
     * @return boolean        Devuelve un valor lógico si es que se pudo realizar la operación o no
     */
    function editarDetalleNotaEntrada($datos) {

        $this->db->trans_begin();

        $sql = "UPDATE mov_nota_entrada_detalle SET estructura = COLUMN_ADD(estructura, 'centro_de_costos', '".$datos["centro_costos"]."')
                    WHERE id_nota_entrada_detalle = ?;";
        $this->db->query($sql, array(
            $datos["borrar_nota_hidden"],
        ));

        $sql2 = "UPDATE mov_nota_entrada_detalle SET titulo = ?,
                    articulo = ?,
                    clave = ?,
                    titulo = ?
                    WHERE id_nota_entrada_detalle = ?;";
        $this->db->query($sql2, array(
            $datos["titulo"],
            $datos["articulo"],
            $datos["articulo"],
            $datos["titulo"],
            $datos["borrar_nota_hidden"],
        ));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }
    }


    /*Nota de Salida*/
    function ultima_nota_salida() {
        $sql = "SELECT numero_movimiento as ultimo FROM mov_nota_salida_caratula  ORDER BY numero_movimiento DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartar_nota_salida($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_movimiento' => $ultimo,
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_nota_salida_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function tabla_nota_salida_principal() {        
        $this->db->select('*')->from('mov_nota_salida_caratula')->order_by("numero_movimiento", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function insertar_caratula_nota_salida($datos) {

//        Se inicia una transacción en la base de datos
        $this->db->trans_begin();

//        En caso de que el importe sea igual a 0, se inicia en 0
        if(!$datos["importe_total"]) {
            $datos["importe_total"] = 0;
        }

//        Este arreglo son los datos que van a ser insertados en la caratula de la nota de salida
        $data = array(
            "fecha_emision" => $datos["fecha"],
            "fecha_emision_real" => "NOW()",
            "importe" => $datos["importe_total"],
            "concepto_salida" => $datos["tipo_concepto"],
            "concepto_salida_otro" => $datos["tipo_concepto_otro"],
            "enfirme" => $datos["check_firme"],
            "cancelada" => 0,
            "observaciones" => $datos["observaciones"],
            "estatus" => $datos["estatus"],
            "centro_costos" => $datos["centro_costos"],
            "descripcion_centro_costos" => $datos["descripcion_centro_costos"],
        );

//        Esta función es la que se encarga de actualizar la nota de entrada
        $this->db->where('numero_movimiento', $datos["ultima_nota"]);
        $this->db->update('mov_nota_salida_caratula', $data);

//        De lo contrario devuelve una variable booleana falsa
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertar_caratula_nota_salida_archivo($datos) {

//        Se inicia una transacción en la base de datos
        $this->db->trans_begin();

//        Este arreglo son los datos que van a ser insertados en la caratula de la nota de entrada
        $data = array(
            "fecha_emision" => $datos["B"],
            "fecha_emision_real" => "NOW()",
            "importe" => $datos["importe_total"],
            "concepto_salida" => $datos["A"],
            "concepto_salida_otro" => "",
            "enfirme" => 0,
            "cancelada" => 0,
            "observaciones" => $datos["C"],
            "estatus" => "espera",
        );

//        Esta función es la que se encarga de actualizar la nota de entrada
        $this->db->where('numero_movimiento', $datos["ultima_nota"]);
        $this->db->update('mov_nota_salida_caratula', $data);

//        De lo contrario devuelve una variable booleana falsa
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertar_nota_salida_detalle($datos, $query) {

        if($datos["producto_nota_entrada"] == NULL || $datos["producto_nota_entrada"] == "" ) {
            $datos["producto_nota_entrada"] = 0;
        }
        

        $this->db->trans_begin();

        $this->db->query($query, array(
            $datos["ultima_nota"],
            $datos["id_nivel"],
            $datos["gasto"],
            $datos["u_medida"],
            $datos["cantidad"],
            $datos["precio"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            $datos["descripcion_detalle"],
            $datos["producto_nota_entrada"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"],
            $datos["titulo_gasto"]));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertar_nota_salida_detalle_archivo($datos, $query) {

        $this->db->trans_begin();

        $this->db->query($query, array(
            $datos["ultima_nota"],
            0,
            $datos["id_nivel"],
            $datos["G"],
            $datos["u_medida"],
            $datos["I"],
            $datos["J"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            $datos["L"],
            $datos["L"],
            $datos["A"],
            $datos["B"],
            $datos["C"],
            $datos["D"],
            $datos["E"],
            $datos["F"],
            $datos["titulo_gasto"]));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function borrarDetalleNotaSalida($nota) {
        $this->db->where('id_nota_salida_detalle', $nota);
        $query = $this->db->delete('mov_nota_salida_detalle');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_productos_nota_entrada() {
        $sql = "SELECT mned.*, COLUMN_JSON(estructura) AS estructura FROM mov_nota_entrada_detalle mned
                JOIN mov_nota_entrada_caratula mnec ON mnec.numero_movimiento = mned.numero_movimiento
                WHERE mnec.enfirme = 1;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_nota_salida($nota_salida, $query){
        $query = $this->db->query($query, array($nota_salida));
        $result = $query->result();
        return $result;
    }

    function get_datos_nota_salida_caratula($nota_salida, $query) {
        $query = $this->db->query($query, array($nota_salida));
        $result = $query->row();
        return $result;
    }

    function cancelarNotaSalidaCaratula($nota_salida, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $nota_salida,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_nota_salida_kardex($datos){

        $this->db->trans_begin();

        try {

            $this->db->select('*')->from('mov_nota_salida_detalle')->where('numero_movimiento', $datos["ultima_nota"]);
            $query = $this->db->get();
            $resultado_detalle = $query->result_array();

//            TODO: En esta parte se tienen que revisar la existencia de este producto en el catalogo de productos, para poder sacar el promedio del costo,
//            existencias, etc.
//            Por el momento solo se jalan los datos del detalle

            foreach($resultado_detalle as $key => $value) {
                // Se toma la existencia y la cantidad promedio del articulo actual
                $this->db->select('id_conceptos_gasto AS ID, existencia, promedio')
                            ->from('cat_conceptos_gasto')
                            ->where('articulo', $value["articulo"]);
                $query = $this->db->get();
                $existencia_detalle = $query->row_array();

                // Se suma a la cantidad existente, la cantidad de cada detalle de la nota
                $existencia_detalle["existencia"] -= $value["cantidad"];

                if($existencia_detalle["existencia"] < 0) {
                    throw new Exception('La cantidad de art&iacute;culos que ingres&oacute;, es mayor a la cantidad existente dentro del cat&aacute;logo de productos.');
                }

                // Se actualiza la existencia del articulo en el catalogo de productos
                $datos_actualizar = array(
                   'existencia' => $existencia_detalle["existencia"],
                   'promedio' => $existencia_detalle["promedio"],
                );

                $this->db->where('articulo', $value["articulo"]);
                $this->db->update('cat_conceptos_gasto', $datos_actualizar); 

                $datos_insertar = array(
                    'id_nivel' => $value["id_nivel"],
                    'estructura' => $value["estructura"],
                    'id_articulo' => $existencia_detalle["ID"],
                    'articulo' => $value["articulo"],
                    'fecha' => $datos["fecha"],
                    'fecha_sql' => "NOW()",
                    'hora' => "NOW()",
                    'operacion' => "Salida",
                    'movimiento' => $value["numero_movimiento_entrada"],
                    'movimiento_salida' => $value["numero_movimiento"],
                    'entra' => 0,
                    'sale' => $value["cantidad"],
                    'existe' => $existencia_detalle["existencia"],
                    'unidad_medida' => $value["unidad_medida"],
                    'precio_promedio' => $value["importe"],
                    'precio_unitario' => $value["p_unitario"],
                    'descripcion' => $value["titulo"],
                    'observaciones' => $value["observaciones"],
                );

                $this->db->insert('mov_kardex_almacen', $datos_insertar);

            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            }
            else {
                $this->db->trans_commit();
                return TRUE;
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    /*Inventarios*/
    function get_datos_indice_inventario() {
        $sql = "SELECT * FROM cat_conceptos_gasto WHERE unidad NOT LIKE '%SRV%' AND unidad NOT LIKE '%SOP%';";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*Seccion de Kardex*/
    function get_datos_indice_kardex() {
        $sql = "SELECT articulo,
                        descripcion,
                        fecha,
                        operacion,
                        movimiento,
                        movimiento_salida,
                        hora,
                        entra,
                        sale,
                        existe,
                        unidad_medida,
                        precio_proveedor,
                        precio_promedio
                        precio_unitario
                        FROM mov_kardex_almacen;";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    // ------------------------------------------------------------------------ //
    /**
     * Aqui comienza la sección del catalogo de productos
     */
    
    /**
     * Esta función toma todos los datos de la tabla de "cat_niveles"
     * para desplegarlos en forma de tabla
     * @return array Este es un conjunto de todos los elementos de la tabla de "cat_niveles" en formato JSON, la única modificación, es que tienen
     * que devolverse en este formato para poder manipular la información
     */
    function contenido_tabla_partidas() {
        $sql = "SELECT COLUMN_JSON(nivel) AS estructura
                FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                    AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                    AND COLUMN_GET(nivel, 'concepto' as char) != ''
                    AND COLUMN_GET(nivel, 'partida' as char) != ''
                    AND COLUMN_GET(nivel, 'partida' as char) NOT LIKE '1%'
                    AND COLUMN_GET(nivel, 'partida' as char) NOT LIKE '3%'
                GROUP BY COLUMN_GET(nivel, 'partida' as char);";
        $query = $this->db->query($sql);
        return $query->result_array(); 
    }

    /**
     * Esta función toma todos los datos de la tabla de "cat_unidad_medida"
     * para desplegarlos en forma de tabla
     * @return array Este es un conjunto de todos los elementos de la tabla de "cat_unidad_medida"
     */
    function contenido_tabla_unidad_medida() {
        $sql = "SELECT clave, descripcion
                FROM cat_unidad_medida;";
        $query = $this->db->query($sql);
        return $query->result_array(); 
    }

    /**
     * Esta función se encarga de insertar un nuevo producto a la tabla "cat_conceptos_gasto"
     * @param  array $datos Es un conjunto de variables que contienen la información necesaria para poder ingresar un nuevo producto
     * @return boolean Devuelve un verdadero o falso, dependiendo del estatus de la transacción final
     */
    function insertar_catalogo_nuevo_producto($datos) {
        
        $this->db->trans_begin();

        $datos_insertar = array(
           'articulo' => $datos["clave"],
           'descripcion' => $datos["descripcion"],
           'unidad' => $datos["unidad_medida"],
           'tipo_clase' => $datos["tipo_clase"],
           'codigo' => $datos["codigo"],
           'ruta_imagen' => $datos["ruta_imagen"],
           'ultimo_costo' => $datos["ultimo_costo"],
           'fecha_compra' => $datos["fecha_compra"],
           'partida' => $datos["partida"],
           'clave_activo' => $datos["clave_activo"],
           'clasificacion_patrimonial' => $datos["clasificacion_patriomnial"],
           'marca' => $datos["marca"],
           'tipo' => $datos["tipo"],
           'modelo' => $datos["modelo"],
           'serie' => $datos["serie"],
           'tipo_unidad' => $datos["tipo_unidad"],
           'existencia' => $datos["existencia"],
           'maximo_stock' => $datos["maximo_stock"],
           'punto_reorden' => $datos["punto_reorden"],
           'minimo_stock' => $datos["minimo_stock"],
           'estante' => $datos["estante"],
           'anaquel' => $datos["anaquel"],
           'localizacion' => $datos["localizacion"],
           'area_asignado' => $datos["area_asignado"],
           'nombre_resguardante' => $datos["nombre_resguardante"],
        );

        $this->db->insert('cat_conceptos_gasto', $datos_insertar); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    /**
     * Esta función se encarga de actualizar un producto existente dentro de la tabla "cat_conceptos_gasto"
     * @param  array $datos Es un conjunto de variables que contienen la información necesaria para poder actualizar el producto
     * @return boolean Devuelve un verdadero o falso, dependiendo del estatus de la transacción final
     */
    function actualizar_catalogo_producto($datos) {
        
        $this->db->trans_begin();

        $datos_actualizar = array(
           'articulo' => $datos["clave"],
           'descripcion' => $datos["descripcion"],
           'unidad' => $datos["unidad_medida"],
           'tipo_clase' => $datos["tipo_clase"],
           'codigo' => $datos["codigo"],
           'ruta_imagen' => $datos["ruta_imagen"],
           'ultimo_costo' => $datos["ultimo_costo"],
           'fecha_compra' => $datos["fecha_compra"],
           'partida' => $datos["partida"],
           'clave_activo' => $datos["clave_activo"],
           'clasificacion_patrimonial' => $datos["clasificacion_patriomnial"],
           'marca' => $datos["marca"],
           'tipo' => $datos["tipo"],
           'modelo' => $datos["modelo"],
           'serie' => $datos["serie"],
           'tipo_unidad' => $datos["tipo_unidad"],
           'existencia' => $datos["existencia"],
           'maximo_stock' => $datos["maximo_stock"],
           'punto_reorden' => $datos["punto_reorden"],
           'minimo_stock' => $datos["minimo_stock"],
           'estante' => $datos["estante"],
           'anaquel' => $datos["anaquel"],
           'localizacion' => $datos["localizacion"],
           'area_asignado' => $datos["area_asignado"],
           'nombre_resguardante' => $datos["nombre_resguardante"],
        );

        $this->db->where('id_conceptos_gasto', $datos["id_conceptos_gasto"]);
        $this->db->update('cat_conceptos_gasto', $datos_actualizar);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    /**
     * Esta función devuelve todos los datos del producto, para poder desplegarlos al usuario,
     * y que este último, sea capaz de editarlos
     * @param  string $producto Esta es la clave del producto que va a ser editado
     * @return array Devuelve un arreglo con todos los datos del producto solicitado
     */
    function get_datos_producto($producto) {
        $this->db->select('*')
                    ->from('cat_conceptos_gasto')
                    ->where('id_conceptos_gasto', $producto);
        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Esta función sirve para poder eliminar un producto del catálogo de productos
     * @param  int $producto Esta variable contiene el ID del producto que va a ser eliminado
     * @return boolean           Devuelve una variable verdadera o falsa dependiendo del resultado de la operación
     */
    function borrar_producto($producto) {
        // Se activa la transacción
        $this->db->trans_begin();

        // Se realiza el query en la base de datos
        $this->db->where('id_conceptos_gasto', $producto);
        $this->db->delete('cat_conceptos_gasto'); 

        if ($this->db->trans_status() === FALSE) {
            // Si ocurre un error en la transacción, se devuelve una variable como falsa
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            // Si la transacción es un éxito, se devuelve una variable como verdadera
            $this->db->trans_commit();
            return TRUE;
        }

    }

    // ------------------------------------------------------------------------ //
    /** Aqui comienza la sección para la consulta del inventario **/

    function get_datos_inventario_impresion($datos) {
        $sql = "SELECT mka.operacion, mka.articulo, mka.descripcion, mka.unidad_medida, ccg.existencia, mka.precio_promedio
                    FROM cat_conceptos_gasto ccg
                    INNER JOIN mov_kardex_almacen mka
                    ON mka.articulo = ccg.articulo
                    WHERE mka.fecha >= ?
                    AND mka.fecha <= ?
                    AND mka.id_articulo BETWEEN ? AND ?
                    GROUP BY mka.articulo;";
        $query = $this->db->query($sql, array(
            $datos["fecha_inicial"],
            $datos["fecha_final"],
            $datos["gasto_inicial"],
            $datos["gasto_final"],
            ));
        return $query->result_array();
    }

    function get_ID_gastos($gasto_inicial, $gasto_final) {
        $ID_gastos = array(
            'ID_inicial' => "",
            'ID_final' => "",
        );

        $this->db->select('id_conceptos_gasto')
                   ->from('cat_conceptos_gasto')
                   ->where('articulo', $gasto_inicial);
        $query1 = $this->db->get();

        $resultado_inicial = $query1->row_array();

        if(!isset($resultado_inicial["id_conceptos_gasto"]) || $resultado_inicial["id_conceptos_gasto"] == "" || $resultado_inicial["id_conceptos_gasto"] == NULL) {
            $ID_gastos["ID_inicial"] = 1;
        } else {
            $ID_gastos["ID_inicial"] = $resultado_inicial["id_conceptos_gasto"] ;
        }

        $this->db->select('id_conceptos_gasto')
                   ->from('cat_conceptos_gasto')
                   ->where('articulo', $gasto_final);
        $query2 = $this->db->get();

        $resultado_final = $query2->row_array();

        if(!isset($resultado_final["id_conceptos_gasto"]) || $resultado_final["id_conceptos_gasto"] == "" || $resultado_final["id_conceptos_gasto"] == NULL) {
            $ID_gastos["ID_final"] = 9999999;
        } else {
            $ID_gastos["ID_final"] = $resultado_final["id_conceptos_gasto"] ;
        }

        return $ID_gastos;

    }

}