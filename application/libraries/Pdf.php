<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF {
    function __construct() {
        parent::__construct();
    }

    //Page header
    public function Header() {
        $html = '<table cellspacing="3">
                    <tr>
                        <th align="left"><img src="'.base_url("img/logo.png").'" style="width: 150px;" /> </th>
                        <th align="right">Gerencia Tecnico Administrativa<br />Jefatura de Recursos Materiales<br />Coordinación de Adquisiciones</th>
                    </tr>
                 </table>';
        $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}