<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Prueba extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('prueba_model');
    }

    function index() {
        $this->load->view('prueba_view', array('error' => ' ' ));
        $this->load->library('image_lib');
    }

    function acomodar_precompromisos_terminados() {
        $sql = "SELECT numero_pre FROM mov_precompromiso_caratula WHERE estatus = 'Terminado';";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach ($result as $key => $value) {
            $datos_precompromiso = $this->ciclo_model->get_datos_precompromiso_detalle($value["numero_pre"]);

            $sql_caratula = "SELECT fecha_emision FROM mov_precompromiso_caratula WHERE numero_pre = ?";

            $datos_caratula_precompromiso = $this->ciclo_model->get_datos_precompromiso_caratula($value["numero_pre"], $sql_caratula);

            $mes_devolver = $this->utilerias->convertirFechaAMes($datos_caratula_precompromiso->fecha_emision);

            $query_compromisos_detalle = "SELECT importe, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle mcd
                                            JOIN mov_compromiso_caratula mcc
                                            ON mcd.numero_compromiso = mcc.numero_compromiso
                                            WHERE mcc.num_precompromiso = ".$value["numero_pre"]."
                                            AND mcc.enfirme = 1
                                            AND mcc.firma1 = 1
                                            AND mcc.firma2 = 1
                                            AND mcc.firma3 = 1
                                            AND mcc.cancelada = 0;";
            $datos_compromiso = $this->ciclo_model->get_arreglo_datos($query_compromisos_detalle);

//            $this->debugeo->imprimir_pre($datos_compromiso);

            $arreglo_precompromisos = array();
            $arreglo_compromisos = array();

            foreach($datos_precompromiso as $key_precompromiso => $value_precompromiso) {
                $estructura = json_decode($value_precompromiso->estructura, TRUE);
                $indice = $estructura["fuente_de_financiamiento"].".".$estructura["programa_de_financiamiento"].".".$estructura["centro_de_costos"].".".$estructura["capitulo"].".".$estructura["concepto"].".".$estructura["partida"];
                if (array_key_exists($indice, $arreglo_precompromisos)) {
                    $arreglo_precompromisos[$indice] += $value_precompromiso->importe;
                } else {
                    $arreglo_precompromisos[$indice] = $value_precompromiso->importe;
                }

            }

            unset($key_precompromiso);
            unset($value_precompromiso);

            foreach($datos_compromiso as $key_compromiso => $value_compromiso) {

                $estructura = json_decode($value_compromiso["estructura"], TRUE);
                $indice = $estructura["fuente_de_financiamiento"].".".$estructura["programa_de_financiamiento"].".".$estructura["centro_de_costos"].".".$estructura["capitulo"].".".$estructura["concepto"].".".$estructura["partida"];
                if (array_key_exists($indice, $arreglo_compromisos)) {
                    $arreglo_compromisos[$indice] += $value_compromiso["importe"];
                } else {
                    $arreglo_compromisos[$indice] = $value_compromiso["importe"];
                }

            }

            unset($key_compromiso);
            unset($value_compromiso);

            $total_precompromiso = 0;
            $importe_devolver = 0;
            $importe_devolver_total = 0;

            foreach($arreglo_precompromisos as $key_precompromiso => $value_precompromiso) {

                if (array_key_exists($key_precompromiso, $arreglo_compromisos)) {

                    $importe_devolver = abs($value_precompromiso - $arreglo_compromisos[$key_precompromiso]);

                    if($importe_devolver > 0) {
                        //        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
                        $total_egresos = $this->egresos_model->contar_egresos_elementos();
                //        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
                        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

                //        En este arreglo se van a guardar los nombres de los niveles
                        $nombre = array();

                //        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
                        foreach($nombres_egresos as $fila) {
                            array_push($nombre, $fila->descripcion);
                        }

                        $query_insertar = "INSERT INTO mov_precompromiso_detalle (numero_pre, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, mov_precompromiso_detalle.year, especificaciones, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";

                        for($i = 0; $i < $total_egresos->conteo; $i++){
                            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                        }

                        $partida = explode(".",$key_precompromiso);

                        $query_insertar .= "'gasto', ?));";

                        $datos = array(
                            "ultimo_pre" => $value["numero_pre"],
                            "id_nivel" => 0,
                            "gasto" => "N/A",
                            "u_medida" => "N/A",
                            "cantidad" => 1,
                            "precio" => -1 * $importe_devolver,
                            "subtotal" => -1 * $importe_devolver,
                            "iva" => 0,
                            "importe" => -1 * $importe_devolver,
                            "titulo_gasto" => "Presupuesto Devuelto",
                            "descripcion_detalle" => "Presupuesto Devuelto",
                            "nivel1" => $partida[0],
                            "nivel2" => $partida[1],
                            "nivel3" => $partida[2],
                            "nivel4" => $partida[3],
                            "nivel5" => $partida[4],
                            "nivel6" => $partida[5],
                            "titulo_gasto" => "Presupuesto Devuelto",
                        );

                        $resultado = $this->ciclo_model->insertar_detalle_precompromiso($datos, $query_insertar);

                        $this->debugeo->imprimir_pre($resultado);
                        $this->debugeo->imprimir_pre("Estructura:".$key_precompromiso);
                        $this->debugeo->imprimir_pre("Precompromiso:".$value["numero_pre"]);
                        $this->debugeo->imprimir_pre($importe_devolver);

                    }

                } else {
//                    $this->debugeo->imprimir_pre("Entro");
                    $partida = explode(".",$key_precompromiso);
                    $this->debugeo->imprimir_pre('La partida '.$partida[0].' '.$partida[1].' '.$partida[2].' '.$partida[3].' '.$partida[4].' '.$partida[5].', no se encuentra del precompromiso.' );
                }
            }

        }
    }

    function acomodar_cuentas() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $sql_cuentas = "SELECT * FROM cat_cuentas_contables;";
        $query_cuentas = $this->db->query($sql_cuentas);
        $cuentas_contables = $query_cuentas->result_array();

        try {

            foreach ($cuentas_contables as $key => $value) {
                
                $padre = $this->BuscarPadre($value["cuenta"]);
                $nivel = $this->SetNivel($value["cuenta"]);

                $this->db->select('cuenta');
                $query = $this->db->get_where('cat_cuentas_contables', array('cuenta' => $padre));

                $resultado_padre = $query->row_array();

                if (!isset($resultado_padre["cuenta"]) && $padre != 0) {
                    throw new Exception("La cuenta: ".$padre.", no existe");
                }

                $datos_actualizar = array(
                    'cuenta_padre' => $padre,
                    'nivel' => $nivel,
                );

                $this->db->where('cuenta', $value["cuenta"]);
                $this->db->update('cat_cuentas_contables', $datos_actualizar);
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

        } catch (Exception $e) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre($e->getMessage());
        }

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');

    }

    function acomodar_saldos_iniciales() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $sql_cuentas = "SELECT * FROM cat_cuentas_contables WHERE tipo = 'D';";
        $query_cuentas = $this->db->query($sql_cuentas);
        $cuentas_contables = $query_cuentas->result_array();

        foreach ($cuentas_contables as $key => $value) {

            $padre = $value["cuenta_padre"];

            do {

                $this->db->select('saldo_inicial');
                $query = $this->db->get_where('cat_cuentas_contables', array('cuenta' => $padre));

                $resultado_padre = $query->row_array();

                if (!isset($resultado_padre["saldo_inicial"]) || !isset($value["saldo_inicial"])) {
                    $this->debugeo->imprimir_pre($padre);
                    $this->debugeo->imprimir_pre($value);
                }
                

                $datos_actualizar_padre = array(
                    'saldo_inicial' => $resultado_padre["saldo_inicial"] + $value["saldo_inicial"],
                );

                $this->db->where('cuenta', $padre);
                $this->db->update('cat_cuentas_contables', $datos_actualizar_padre);

                $this->db->select('*');
                $query = $this->db->get_where('cat_cuentas_contables', array('cuenta' => $padre));
                $resultado_padre_nuevo = $query->row_array();

                $padre = $resultado_padre_nuevo["cuenta_padre"];

            } while ($padre != 0);

        }

        // foreach ($cuentas_contables as $key => $value) {

        //     if ($this->db->conn_id->ping() === FALSE) {
        //         sleep(1);
        //         $this->db->reconnect();
        //     }
            
        //     $datos_actualizar = array(
        //         'saldo_inicial' => $value["saldo_inicial"],
        //     );

        //     $this->db->where('id_cuentas_contables', $value["id_cuentas_contables"]);
        //     $this->db->update('cat_cuentas_contables', $datos_actualizar);

        // }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        
        // echo("Cuentas contables finales");
        // $this->debugeo->imprimir_pre($cuentas_contables);

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    private function BuscarPadre($ctaSeek = NULL) {
        $posicion_punto = strrpos($ctaSeek, '.', -1);
        $cuenta_cortada = substr($ctaSeek, 0, $posicion_punto);
        if (!$cuenta_cortada) {
            $cuenta_cortada = 0;
        }
        return $cuenta_cortada;
    }

    private function SetNivel($cuenta = NULL) {
        $arreglo_cuenta = explode(".", $cuenta);
        $nivel = count($arreglo_cuenta);
        return $nivel;
    }

    function revisar_presupuesto_tomado($precompromiso) {

        $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado,
                            COLUMN_JSON(nivel) AS estructura,
                            importe')
                    ->from('mov_precompromiso_detalle')
                    ->where('numero_pre', $precompromiso);
        $query = $this->db->get();
        $resultado = $query->result_array();
        $linea = 1;

        foreach ($resultado as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);
            $presupuesto_tomado = json_decode($value["presupuesto_tomado"], TRUE);
            echo "Línea ".$linea."<br />";
            echo "Estructura [ ".$estructura["fuente_de_financiamiento"]." ".$estructura["programa_de_financiamiento"]." ".$estructura["centro_de_costos"]." ".$estructura["partida"]." ] <br />";
            echo "Presupuesto Tomado [ Enero: $".number_format($presupuesto_tomado["enero"], 2, '.', ',')
            .", Febrero: $".number_format($presupuesto_tomado["febrero"], 2, '.', ',')
            .", Marzo: $".number_format($presupuesto_tomado["marzo"], 2, '.', ',')
            .", Abril: $".number_format($presupuesto_tomado["abril"], 2, '.', ',')
            .", Mayo: $".number_format($presupuesto_tomado["mayo"], 2, '.', ',')
            .", Junio: $".number_format($presupuesto_tomado["junio"], 2, '.', ',')
            .", Julio: $".number_format($presupuesto_tomado["julio"], 2, '.', ',')
            .", Agosto: $".number_format($presupuesto_tomado["agosto"], 2, '.', ',')
            .", Septiembre: $".number_format($presupuesto_tomado["septiembre"], 2, '.', ',')
            .", Octubre: $".number_format($presupuesto_tomado["octubre"], 2, '.', ',')
            .", Noviembre: $".number_format($presupuesto_tomado["noviembre"], 2, '.', ',')
            .", Diciembre: $".number_format($presupuesto_tomado["diciembre"], 2, '.', ',')." ] <br />";
            $linea += 1;
        }

        $this->db->cache_delete('prueba', 'revisar_presupuesto_tomado');
    }

    function revisar_presupuesto_tomado_compromiso($compromiso) {
        
        $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado,
                            COLUMN_JSON(nivel) AS estructura,
                            importe')
                    ->from('mov_compromiso_detalle')
                    ->where('numero_compromiso', $compromiso);
        $query = $this->db->get();
        $resultado = $query->result_array();

        $linea = 1;

        foreach ($resultado as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);
            $presupuesto_tomado = json_decode($value["presupuesto_tomado"], TRUE);
            echo "Línea ".$linea."<br />";
            echo "Estructura [ ".$estructura["fuente_de_financiamiento"]." ".$estructura["programa_de_financiamiento"]." ".$estructura["centro_de_costos"]." ".$estructura["partida"]." ] <br />";
            echo "Presupuesto Tomado [ Enero: $".number_format($presupuesto_tomado["enero"], 2, '.', ',')
            .", Febrero: $".number_format($presupuesto_tomado["febrero"], 2, '.', ',')
            .", Marzo: $".number_format($presupuesto_tomado["marzo"], 2, '.', ',')
            .", Abril: $".number_format($presupuesto_tomado["abril"], 2, '.', ',')
            .", Mayo: $".number_format($presupuesto_tomado["mayo"], 2, '.', ',')
            .", Junio: $".number_format($presupuesto_tomado["junio"], 2, '.', ',')
            .", Julio: $".number_format($presupuesto_tomado["julio"], 2, '.', ',')
            .", Agosto: $".number_format($presupuesto_tomado["agosto"], 2, '.', ',')
            .", Septiembre: $".number_format($presupuesto_tomado["septiembre"], 2, '.', ',')
            .", Octubre: $".number_format($presupuesto_tomado["octubre"], 2, '.', ',')
            .", Noviembre: $".number_format($presupuesto_tomado["noviembre"], 2, '.', ',')
            .", Diciembre: $".number_format($presupuesto_tomado["diciembre"], 2, '.', ',')." ] <br />";
            $linea += 1;
        }
    }

    function acomodar_presupuesto_tomado() {
        
        $precompromisos = array(
            0 => 48,
            1 => 49,
            2 => 50,
            3 => 55,
            4 => 56,
            5 => 57,
            6 => 58,
            7 => 59,
            8 => 60,
            9 => 62,
            10 => 63,
            11 => 64,
            12 => 65,            
        );

        foreach ($precompromisos as $key => $value) {
            $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado,
                                id_precompromiso_detalle AS id_detalle')
                    ->from('mov_precompromiso_detalle')
                    ->where('numero_pre', $value);
            $query = $this->db->get();
            $resultado = $query->result_array();

            foreach ($resultado as $key_interno => $value_interno) {
                // $this->debugeo->imprimir_pre($value_interno);
                $presupuesto_tomado = json_decode($value_interno["presupuesto_tomado"], TRUE);
                $operacion = round($presupuesto_tomado["enero"] - abs($presupuesto_tomado["febrero"]), 2);
                $this->debugeo->imprimir_pre($operacion);
                $sql = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_ADD(presupuesto_tomado, 'febrero', 0.0),
                        presupuesto_tomado = COLUMN_ADD(presupuesto_tomado, 'enero', ?)
                        WHERE id_precompromiso_detalle = ?;";
                $query_interno = $this->db->query($sql, array( $operacion, $value_interno["id_detalle"] ));
            }
        }

        
    }

    function generar_polizas_diario_egresos() {
        $this->db->select('id_contrarecibo_caratula AS numero')
                  ->from('mov_contrarecibo_caratula')
                  ->where('enfirme', 1)
                  ->where('cancelada !=', 1)
                  ->where('estatus', "activo");
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);
            $poliza = $this->utilerias->generar_poliza_diario_egresos($value["numero"]);
            $this->debugeo->imprimir_pre($poliza);
        }
        
    }

    function generar_polizas_egresos_directa() {
        $sql = "SELECT movimiento AS numero
                        FROM (mov_bancos_movimientos)
                        WHERE contrarecibo IS NULL
                        AND enfirme = 1
                        AND cuenta_cargo IS NOT NULL
                        AND cuenta_abono IS NOT NULL
                        AND cancelada != 1;";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();        

        foreach ($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);
            $poliza = $this->utilerias->generar_poliza_movimiento_extrapresupuesto($value["numero"]);
            $this->debugeo->imprimir_pre($poliza);
        }
        
    }

    function generar_polizas_egresos() {
        $this->db->select('movimiento AS numero')
                  ->from('mov_bancos_movimientos')
                  ->where('contrarecibo !=', '')
                  ->where('enfirme', 1)
                  ->where('cancelada !=', 1);
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);
            $poliza = $this->utilerias->generar_poliza_egresos($value["numero"]);
            $this->debugeo->imprimir_pre($poliza);
        }
        
    }

    function armar_estructura() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->benchmark->mark('code_start');

        $this->db->trans_start();

        $this->db->select('clave, descripcion')
                    ->from('tabla_nivel1');
        $query1 = $this->db->get();
        $resultado1 = $query1->result_array();
        // Nivel 1
        foreach ($resultado1 as $key1 => $value1) {
            $datos_insertar_nivel1 = array(
                'nivel1' => $value1["clave"],
                'descripcion' => $value1["descripcion"],
            );
            $this->db->insert('tabla_estructura_armada', $datos_insertar_nivel1);

            $this->db->select('clave, descripcion')
                        ->from('tabla_nivel2');
            $query2 = $this->db->get();
            $resultado2 = $query2->result_array();
            // Nivel 2
            foreach ($resultado2 as $key2 => $value2) {
                
                $datos_insertar_nivel2 = array(
                    'nivel1' => $value1["clave"],
                    'nivel2' => $value2["clave"],
                    'descripcion' => $value2["descripcion"],
                );
                $this->db->insert('tabla_estructura_armada', $datos_insertar_nivel2);

                $this->db->select('clave, descripcion')
                            ->from('tabla_nivel3');
                $query3 = $this->db->get();
                $resultado3 = $query3->result_array();
                // Nivel 3
                foreach ($resultado3 as $key3 => $value3) {
                    $datos_insertar_nivel3 = array(
                        'nivel1' => $value1["clave"],
                        'nivel2' => $value2["clave"],
                        'nivel3' => $value3["clave"],
                        'descripcion' => $value3["descripcion"],
                    );
                    $this->db->insert('tabla_estructura_armada', $datos_insertar_nivel3);

                    $this->db->select('clave, descripcion')
                                ->from('tabla_nivel4');
                    $query4 = $this->db->get();
                    $resultado4 = $query4->result_array();
                    // Nivel 4
                    foreach ($resultado4 as $key4 => $value4) {
                        $datos_insertar_nivel4 = array(
                            'nivel1' => $value1["clave"],
                            'nivel2' => $value2["clave"],
                            'nivel3' => $value3["clave"],
                            'nivel4' => $value4["clave"],
                            'descripcion' => $value4["descripcion"],
                        );
                        $this->db->insert('tabla_estructura_armada', $datos_insertar_nivel4);

                        $id_nivel4 = substr($value4["clave"], 0, 0);

                        $this->db->select('clave, descripcion')
                                    ->from('tabla_nivel5')
                                    ->like('clave', $id_nivel4, 'after');

                        $query5 = $this->db->get();
                        $resultado5 = $query5->result_array();
                        // Nivel 5
                        foreach ($resultado5 as $key5 => $value5) {
                            $datos_insertar_nivel5 = array(
                                'nivel1' => $value1["clave"],
                                'nivel2' => $value2["clave"],
                                'nivel3' => $value3["clave"],
                                'nivel4' => $value4["clave"],
                                'nivel5' => $value5["clave"],
                                'descripcion' => $value5["descripcion"],
                            );
                            $this->db->insert('tabla_estructura_armada', $datos_insertar_nivel5);

                            $this->db->select('clave, descripcion')
                                        ->from('tabla_nivel6')
                                        ->like('clave', $value5["clave"], 'both');

                            $query6 = $this->db->get();
                            $resultado6 = $query6->result_array();
                            // Nivel 6
                            foreach ($resultado6 as $key6 => $value6) {
                                $datos_insertar_nivel6 = array(
                                    'nivel1' => $value1["clave"],
                                    'nivel2' => $value2["clave"],
                                    'nivel3' => $value3["clave"],
                                    'nivel4' => $value4["clave"],
                                    'nivel5' => $value5["clave"],
                                    'nivel6' => $value6["clave"],
                                    'descripcion' => $value6["descripcion"],
                                );
                                $this->db->insert('tabla_estructura_armada', $datos_insertar_nivel6);
                            }

                        }
                    }
                }
            }
        }

        $this->db->trans_complete();

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function agregar_matriz_ingresos() {

        $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $this->db->select('*')
                    ->from('tabla_armado_ingresos_matriz');
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {

            if($value["tipo_momento"] == "DEVOLUCION") {
                $value["centro_recaudacion"] .= "D";
            } elseif ($value["tipo_momento"] == "CANCELACION") {
                $value["centro_recaudacion"] .= "CR";
            }

            $data_insertar = array(
               'clave' => $value["centro_recaudacion"],
               'descripcion' => $value["descripcion"],
               'cuenta_cargo' => $value["cuenta_cargo"],
               'nombre_cargo' => $value["descripcion_cargo"],
               'cuenta_abono' => $value["cuenta_abono"],
               'nombre_abono' => $value["descripcion_abono"],
               'tipo_gasto' => 1,
               'destino' => $value["destino"],
            );

            $this->db->insert('cat_correlacion_partidas_contables', $data_insertar); 
            // $this->debugeo->imprimir_pre($value);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');

    }

    function crear_columnas_compromiso() {
        $sql = "SELECT mcd.id_compromiso_detalle AS id_compro, COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado
                FROM mov_compromiso_detalle mcd
                JOIN mov_compromiso_caratula mcc
                ON mcd.numero_compromiso = mcc.numero_compromiso
                WHERE mcc.enfirme = 1
                AND mcc.firma1 = 1
                AND mcc.firma2 = 1
                AND mcc.firma3 = 1
                AND mcc.cancelada != 1;";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            $columnas_presupuesto = json_decode($value["presupuesto_tomado"], TRUE);
            
            $presupuesto_tomado = "UPDATE mov_compromiso_detalle SET presupuesto_tomado = COLUMN_CREATE( ";
            
            for ($i = 1; $i <= 12; $i++) {

                // Se toma el año en curso
                $year = date("Y");

                // Se transforma el numero del mes a texto
                $fecha_interna = $this->utilerias->convertirFechaAMes($year."-".$i."-01");

                if(!isset($columnas_presupuesto)) {
                    $presupuesto_tomado .= " '".$fecha_interna."', 0.0,";
                } else {
                    $presupuesto_tomado .= " '".$fecha_interna."', ".$columnas_presupuesto[$fecha_interna].",";
                }

            }
            $presupuesto_tomado = substr($presupuesto_tomado, 0, -1);

            $presupuesto_tomado .= " ) WHERE id_compromiso_detalle = ".$value["id_compro"].";";

            $this->debugeo->imprimir_pre($presupuesto_tomado);
            
            $this->db->query($presupuesto_tomado);

        }

        
    }

    function actualizar_presupuesto_tomado_compromisos() {
        $sql = "SELECT mcd.id_compromiso_detalle AS id_compro, COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado, mcd.importe, mcc.fecha_emision AS fecha
                FROM mov_compromiso_detalle mcd
                JOIN mov_compromiso_caratula mcc
                ON mcd.numero_compromiso = mcc.numero_compromiso
                WHERE mcc.enfirme = 1
                AND mcc.firma1 = 1
                AND mcc.firma2 = 1
                AND mcc.firma3 = 1
                AND mcc.cancelada != 1;";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);
            $columnas_presupuesto = json_decode($value["presupuesto_tomado"], TRUE);
            
            $presupuesto_tomado = "UPDATE mov_compromiso_detalle SET presupuesto_tomado = COLUMN_ADD( presupuesto_tomado, ";

            $fecha_solicitado = $this->utilerias->convertirFechaAMes($value["fecha"]);
            
            for ($i = 1; $i <= 12; $i++) {

                // Se toma el año en curso
                $year = date("Y");

                // Se transforma el numero del mes a texto
                $fecha_interna = $this->utilerias->convertirFechaAMes($year."-".$i."-01");

                if($fecha_solicitado == $fecha_interna) {
                    $presupuesto_tomado .= " '".$fecha_interna."', ".$value["importe"].",";
                } else {
                    $presupuesto_tomado .= " '".$fecha_interna."', ".$columnas_presupuesto[$fecha_interna].",";
                }

            }
            $presupuesto_tomado = substr($presupuesto_tomado, 0, -1);

            $presupuesto_tomado .= " ) WHERE id_compromiso_detalle = ".$value["id_compro"].";";

            // $this->debugeo->imprimir_pre($presupuesto_tomado);
            
            $this->db->query($presupuesto_tomado);

        }
    }

    function revisar_partidas_duplicadas() {
        $sql = "SELECT COLUMN_JSON(nivel) AS estructura
                    FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                    AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                    AND COLUMN_GET(nivel, 'concepto' as char) != ''
                    AND COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            
            $estructura = json_decode($value["estructura"], TRUE);
            
            $sql2 = "SELECT COLUMN_JSON(nivel) AS estructura
                        FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            $query2 = $this->db->query($sql2, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
                ));
            
            if($query2->num_rows() != 1) {
                $this->debugeo->imprimir_pre($estructura["fuente_de_financiamiento"]. " ".
                $estructura["programa_de_financiamiento"]. " ".
                $estructura["centro_de_costos"]. " ".
                $estructura["capitulo"]. " ".
                $estructura["concepto"]. " ".
                $estructura["partida"]);
            }

        }
    }

    function revisar_partidas_faltantes_matriz() {
        $sql = "SELECT COLUMN_GET(nivel, 'partida' as char) AS partida
                    FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                    AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                    AND COLUMN_GET(nivel, 'concepto' as char) != ''
                    AND COLUMN_GET(nivel, 'partida' as char) != ''
                    GROUP BY COLUMN_GET(nivel, 'partida' as char);";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('clave')->from('cat_correlacion_partidas_contables')->where('clave', $value["partida"]);
            $query = $this->db->get();

            if ($query->num_rows() == 0) {
                $this->debugeo->imprimir_pre($value["partida"]);
            }
            
        }
    }

    function revisar_cuentas_balanza() {
        $arreglo = array(
            "1.1.1.2",
            "1.1.2.9",
            "1.1.1.3",
            "1.1.3.9",
            "1.1.2.2",
            "1.1.2.3",
            "1.1.2.7",
            "1.1.2.8",
            "1.1.4.1",
            "1.1.5.1",
            "1.1.6.1",
            "1.1.6.2",
            "1.2.3.1",
            "1.2.3.3",
            "1.2.3.6",
            "1.2.4.1",
            "1.2.4.4",
            "1.2.4.6",
            "1.2.6.1",
            "1.2.6.3",
            "1.2.6.7",
            "1.2.7.3",
            "1.2.7.9",
            "1.1.1.1.1",
            "1.1.1.1.2",
            "1.1.1.1.3",
            "1.1.1.1.4",
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_balanza_pasivo() {
        $arreglo = array(
            '2.1.1.2',
            '2.1.1.1',
            '2.1.2.1',
            '2.1.7.9',
            '2.1.5.1',
            '2.1.2.2',
            '2.1.1.7',
            '2.1.7.1',
            '2.1.7.2',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_balanza_detalle() {
        $arreglo = array(
            '4.1.7.4',
            '4.3.9.2',
            '4.3.9.3',
            '5.5.9.3',
            '4.1.7.4.4',
            '5.6.2.1',
            '4.2.2',
            '4.3.1.1',
            '4.3.1.1.1',
            '4.3.1.1.1.2',
            '5.5.9.4',
            '4.3.9.9',
            '5.5.9.9.9',
            '5.5.1.3',
            '5.5.1.5',
            '5.5.1.8',
            '5.5.9.9.4',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_balanza_activo_s() {
        $arreglo = array(
            '1.1.1.1.1',
            '1.1.1.1.2',
            '1.1.1.1.3',
            '1.1.1.1.4',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_balanza_activo_pS() {
        $arreglo = array(
            '1.1.1.1',
            '1.1.1.2',
            '1.1.1.3',
            '1.1.6.1',

            '1.1.2.2',
            '1.1.2.3',
            '1.1.2.9',

            '1.1.4.1',
            '1.1.6.2',
            '1.1.5.1',

            '1.1.3.9',

            '1.2.3.1',
            '1.2.3.3',
            '1.2.3.6',

            '1.2.4.1',
            '1.2.4.4',
            '1.2.4.6',
            '1.2.6.1',
            '1.2.6.3',

            '1.1.2.7',
            '1.1.2.8',
            '1.2.7.3',
            '1.2.6.7',
            '1.2.7.9',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_balanza_resultados() {
        $arreglo = array(
            '4.1.7.4',
            '4.3.9.2',
            '4.3.9.3',
            '5.5.9.3',
            '4.1.7.4.4',
            '5.6.2.1',
            '4.2.2',
            '4.3.1.1',
            '4.3.1.1.1',
            '4.3.1.1.1.1.0002',
            '5.5.9.4',
            '4.3.9.9',
            '5.5.9.9.9',
            '5.5.1.3',
            '5.5.1.5',
            '5.5.1.8',
            '5.5.9.9.4',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_pasivo_p() {
        $arreglo = array(
            '2.1.1.2',
            '2.1.1.7',
            '2.1.1.1',
            '2.1.2.1',
            '2.1.2.2',
            '2.1.5.1',
            '2.1.7.1',
            '2.1.7.2',
            '2.1.7.9',
            '2.1.7.2.1.1',
            '2.1.7.2.1.3',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function revisar_cuentas_balanza_hacienda_p() {
        $arreglo = array(
            '3.1.1.2',
            '3.1.1.3',
            '3.1.1.4',
            '3.1.2',
            '3.1.3',
            '3.2.3.1',
            '3.2.3.2',
            '3.2.3.9',
            '3.2.4',
            '3.3.3.1',
            '3.1.1',
            '3.1.2',
            '3.1.3',
            '3.2.1',
            '3.2.2',
            '3.2.3',
            '3.2.4',
            '3.3.3',
        );

        foreach ($arreglo as $key => $value) {
            // $this->debugeo->imprimir_pre($value);

            $this->db->select('cuenta')
                      ->from('cat_cuentas_contables')
                      ->where('cuenta', $value);
            $query = $this->db->get();

            if($query->num_rows() <= 0) {
                $this->debugeo->imprimir_pre($value);
            }
        }
    }

    function redondear_partidas_egresos() {
        $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $this->db->select('id_niveles, COLUMN_JSON(nivel) AS estructura')
                    ->from('cat_niveles')
                    ->where("COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ", '')
                    ->where("COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ", '')
                    ->where("COLUMN_GET(nivel, 'centro_de_costos' as char) != ", '')
                    ->where("COLUMN_GET(nivel, 'capitulo' as char) != ", '')
                    ->where("COLUMN_GET(nivel, 'concepto' as char) != ", '')
                    ->where("COLUMN_GET(nivel, 'partida' as char) != ", '');
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            
            $estructura = json_decode($value["estructura"], TRUE);
            
            foreach ($estructura as $key_interno => $value_interno) {
                if(is_numeric ($value_interno)
                    && $key_interno != "fuente_de_financiamiento"
                    && $key_interno != "programa_de_financiamiento"
                    && $key_interno != "centro_de_costos"
                    && $key_interno != "capitulo"
                    && $key_interno != "concepto"
                    && $key_interno != "partida") {
                    $estructura[$key_interno] = number_format($value_interno, 2, '.', '');
                    // $this->debugeo->imprimir_pre("Modificado [".$estructura[$key_interno]."]");
                }
            }
            $this->debugeo->imprimir_pre($estructura);
            $estructura["modificado_anual"] = $estructura["enero_modificado"] + 
                                                    $estructura["febrero_modificado"] + 
                                                    $estructura["marzo_modificado"] + 
                                                    $estructura["abril_modificado"] + 
                                                    $estructura["mayo_modificado"] + 
                                                    $estructura["junio_modificado"] + 
                                                    $estructura["julio_modificado"] + 
                                                    $estructura["agosto_modificado"] + 
                                                    $estructura["septiembre_modificado"] + 
                                                    $estructura["octubre_modificado"] + 
                                                    $estructura["noviembre_modificado"] + 
                                                    $estructura["diciembre_modificado"];

            $estructura["precomprometido_anual"] = $estructura["enero_precompromiso"] + 
                                                    $estructura["febrero_precompromiso"] + 
                                                    $estructura["marzo_precompromiso"] + 
                                                    $estructura["abril_precompromiso"] + 
                                                    $estructura["mayo_precompromiso"] + 
                                                    $estructura["junio_precompromiso"] + 
                                                    $estructura["julio_precompromiso"] + 
                                                    $estructura["agosto_precompromiso"] + 
                                                    $estructura["septiembre_precompromiso"] + 
                                                    $estructura["octubre_precompromiso"] + 
                                                    $estructura["noviembre_precompromiso"] + 
                                                    $estructura["diciembre_precompromiso"];

            $estructura["comprometido_anual"] = $estructura["enero_compromiso"] + 
                                                    $estructura["febrero_compromiso"] + 
                                                    $estructura["marzo_compromiso"] + 
                                                    $estructura["abril_compromiso"] + 
                                                    $estructura["mayo_compromiso"] + 
                                                    $estructura["junio_compromiso"] + 
                                                    $estructura["julio_compromiso"] + 
                                                    $estructura["agosto_compromiso"] + 
                                                    $estructura["septiembre_compromiso"] + 
                                                    $estructura["octubre_compromiso"] + 
                                                    $estructura["noviembre_compromiso"] + 
                                                    $estructura["diciembre_compromiso"];

            $estructura["devengado_anual"] = $estructura["enero_devengado"] + 
                                                    $estructura["febrero_devengado"] + 
                                                    $estructura["marzo_devengado"] + 
                                                    $estructura["abril_devengado"] + 
                                                    $estructura["mayo_devengado"] + 
                                                    $estructura["junio_devengado"] + 
                                                    $estructura["julio_devengado"] + 
                                                    $estructura["agosto_devengado"] + 
                                                    $estructura["septiembre_devengado"] + 
                                                    $estructura["octubre_devengado"] + 
                                                    $estructura["noviembre_devengado"] + 
                                                    $estructura["diciembre_devengado"];

            $estructura["ejercido_anual"] = $estructura["enero_ejercido"] + 
                                                    $estructura["febrero_ejercido"] + 
                                                    $estructura["marzo_ejercido"] + 
                                                    $estructura["abril_ejercido"] + 
                                                    $estructura["mayo_ejercido"] + 
                                                    $estructura["junio_ejercido"] + 
                                                    $estructura["julio_ejercido"] + 
                                                    $estructura["agosto_ejercido"] + 
                                                    $estructura["septiembre_ejercido"] + 
                                                    $estructura["octubre_ejercido"] + 
                                                    $estructura["noviembre_ejercido"] + 
                                                    $estructura["diciembre_ejercido"];

            $estructura["pagado_anual"] = $estructura["enero_pagado"] + 
                                                    $estructura["febrero_pagado"] + 
                                                    $estructura["marzo_pagado"] + 
                                                    $estructura["abril_pagado"] + 
                                                    $estructura["mayo_pagado"] + 
                                                    $estructura["junio_pagado"] + 
                                                    $estructura["julio_pagado"] + 
                                                    $estructura["agosto_pagado"] + 
                                                    $estructura["septiembre_pagado"] + 
                                                    $estructura["octubre_pagado"] + 
                                                    $estructura["noviembre_pagado"] + 
                                                    $estructura["diciembre_pagado"];

            $this->ciclo_model->actualizar_partida_total($estructura);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    
    
}