<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Control_usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion genera un documento muestra para los reportes
     * para poder ver mas comandos de como personalizar un pdf, visitar la pagina de la libreria TCPDF
     */
    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al control de usuario');
        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('My Title');
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        $pdf->setFooterMargin(20);
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        $pdf->Write(5, 'Some sample text');
        $pdf->Output('My-File-Name.pdf', 'I');
    }
}