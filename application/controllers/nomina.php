<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require( './assets/datatables/scripts/ssp.class.php' );

/**
 * Este controlador se encarga de mostrar la n�mina y controlar las funciones
 **/
class Nomina extends CI_Controller {

    var $sql_details;

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        $this->sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'software2016',
            'host' => 'localhost',
        );
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla principal de la n�mina
     */
    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a n�mina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | N�mina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqu� empieza la secci�n de Empleados */
    function empleados() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, secci�n Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Empleados",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/empleados_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    function nuevo_empleado() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Nuevo Empleado",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/nuevo_empleado_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function consulta_empleados() {
            log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Lista de empleados",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/lista_empleados_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqu� empieza la secci�n de N�mina */

    function nominas() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ',  ha entrado a Recursos Humanos, secci�n N�mina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | N&oacute;mina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/nomina_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    function consulta_nomina() {
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Lista de N&oacute;minas",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/lista_nomina_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function nueva_nomina() {
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Nueva N&oacute;mina",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/nueva_nomina_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }
}


