<?php

class Funciones_consola extends CI_Controller {

    function ejecutar_funciones() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->benchmark->mark('code_start');

        // $this->resetear_todas_las_partidas();
//        $this->fechas_adecuaciones_egresos();
        $this->checar_contrarecibos();
        $this->acomodar_saldos_precomprommiso_compromiso();
        $this->corregir_presupuesto_tomado_compromisos();
        // $this->registrar_devengado();
        // $this->registrar_ejercido();
        // $this->registrar_pagado();

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function resetear_todas_las_partidas() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $sql = "SELECT COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach($resultado as $key => $value){
           // $this->debugeo->imprimir_pre($value);
            $this->resetear_partida($value["estructura"]);
            // $this->resetear_devengado_egresos($value["estructura"]);
        }

        return TRUE;
    }

    function resetear_partida($estructura_entrada) {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_begin();

        $estructura = json_decode($estructura_entrada, TRUE);

//            En esta parte se resetea el dinero de la partida a los saldos iniciales
        $estructura["enero_precompromiso"] = 0;
        $estructura["enero_compromiso"] = 0;
        // $estructura["enero_devengado"] = 0;
        // $estructura["enero_ejercido"] = 0;
        // $estructura["enero_pagado"] = 0;
        $estructura["enero_modificado"] = 0;
        $estructura["enero_saldo"] = $estructura["enero_inicial"];
        $estructura["enero_saldo_precompromiso"] = $estructura["enero_inicial"];
        $estructura["febrero_precompromiso"] = 0;
        $estructura["febrero_compromiso"] = 0;
        // $estructura["febrero_devengado"] = 0;
        // $estructura["febrero_ejercido"] = 0;
        // $estructura["febrero_pagado"] = 0;
        $estructura["febrero_modificado"] = 0;
        $estructura["febrero_saldo"] = $estructura["febrero_inicial"];
        $estructura["febrero_saldo_precompromiso"] = $estructura["febrero_inicial"];
        $estructura["marzo_precompromiso"] = 0;
        $estructura["marzo_compromiso"] = 0;
        // $estructura["marzo_devengado"] = 0;
        // $estructura["marzo_ejercido"] = 0;
        // $estructura["marzo_pagado"] = 0;
        $estructura["marzo_modificado"] = 0;
        $estructura["marzo_saldo"] = $estructura["marzo_inicial"];
        $estructura["marzo_saldo_precompromiso"] = $estructura["marzo_inicial"];
        $estructura["abril_precompromiso"] = 0;
        $estructura["abril_compromiso"] = 0;
        // $estructura["abril_devengado"] = 0;
        // $estructura["abril_ejercido"] = 0;
        // $estructura["abril_pagado"] = 0;
        $estructura["abril_modificado"] = 0;
        $estructura["abril_saldo"] = $estructura["abril_inicial"];
        $estructura["abril_saldo_precompromiso"] = $estructura["abril_inicial"];
        $estructura["mayo_precompromiso"] = 0;
        $estructura["mayo_compromiso"] = 0;
        // $estructura["mayo_devengado"] = 0;
        // $estructura["mayo_ejercido"] = 0;
        // $estructura["mayo_pagado"] = 0;
        $estructura["mayo_modificado"] = 0;
        $estructura["mayo_saldo"] = $estructura["mayo_inicial"];
        $estructura["mayo_saldo_precompromiso"] = $estructura["mayo_inicial"];
        $estructura["junio_precompromiso"] = 0;
        $estructura["junio_compromiso"] = 0;
        // $estructura["junio_devengado"] = 0;
        // $estructura["junio_ejercido"] = 0;
        // $estructura["junio_pagado"] = 0;
        $estructura["junio_modificado"] = 0;
        $estructura["junio_saldo"] = $estructura["junio_inicial"];
        $estructura["junio_saldo_precompromiso"] = $estructura["junio_inicial"];
        $estructura["julio_precompromiso"] = 0;
        $estructura["julio_compromiso"] = 0;
        // $estructura["julio_devengado"] = 0;
        // $estructura["julio_ejercido"] = 0;
        // $estructura["julio_pagado"] = 0;
        $estructura["julio_saldo"] = $estructura["julio_inicial"];
        $estructura["julio_saldo_precompromiso"] = $estructura["julio_inicial"];
        $estructura["agosto_precompromiso"] = 0;
        $estructura["agosto_compromiso"] = 0;
        // $estructura["agosto_devengado"] = 0;
        // $estructura["agosto_ejercido"] = 0;
        // $estructura["agosto_pagado"] = 0;
        $estructura["agosto_modificado"] = 0;
        $estructura["agosto_saldo"] = $estructura["agosto_inicial"];
        $estructura["agosto_saldo_precompromiso"] = $estructura["agosto_inicial"];
        $estructura["septiembre_precompromiso"] = 0;
        $estructura["septiembre_compromiso"] = 0;
        // $estructura["septiembre_devengado"] = 0;
        // $estructura["septiembre_ejercido"] = 0;
        // $estructura["septiembre_pagado"] = 0;
        $estructura["septiembre_modificado"] = 0;
        $estructura["septiembre_saldo"] = $estructura["septiembre_inicial"];
        $estructura["septiembre_saldo_precompromiso"] = $estructura["septiembre_inicial"];
        $estructura["octubre_precompromiso"] = 0;
        $estructura["octubre_compromiso"] = 0;
        // $estructura["octubre_devengado"] = 0;
        // $estructura["octubre_ejercido"] = 0;
        // $estructura["octubre_pagado"] = 0;
        $estructura["octubre_modificado"] = 0;
        $estructura["octubre_saldo"] = $estructura["octubre_inicial"];
        $estructura["octubre_saldo_precompromiso"] = $estructura["octubre_inicial"];
        $estructura["noviembre_precompromiso"] = 0;
        $estructura["noviembre_compromiso"] = 0;
        // $estructura["noviembre_devengado"] = 0;
        // $estructura["noviembre_ejercido"] = 0;
        // $estructura["noviembre_pagado"] = 0;
        $estructura["noviembre_modificado"] = 0;
        $estructura["noviembre_saldo"] = $estructura["noviembre_inicial"];
        $estructura["noviembre_saldo_precompromiso"] = $estructura["noviembre_inicial"];
        $estructura["diciembre_precompromiso"] = 0;
        $estructura["diciembre_compromiso"] = 0;
        // $estructura["diciembre_devengado"] = 0;
        // $estructura["diciembre_ejercido"] = 0;
        // $estructura["diciembre_pagado"] = 0;
        $estructura["diciembre_modificado"] = 0;
        $estructura["diciembre_saldo"] = $estructura["diciembre_inicial"];
        $estructura["diciembre_saldo_precompromiso"] = $estructura["diciembre_inicial"];
        $estructura["total_anual"] = 0;
        $estructura["modificado_anual"] = 0;
        // $estructura["ejercido_anual"] = 0;
        // $estructura["devengado_anual"] = 0;
        $estructura["comprometido_anual"] = 0;
        $estructura["precomprometido_anual"] = 0;
        // $estructura["pagado_anual"] = 0;

        $estructura["total_anual"] += $estructura["enero_inicial"];
        $estructura["total_anual"] += $estructura["febrero_inicial"];
        $estructura["total_anual"] += $estructura["marzo_inicial"];
        $estructura["total_anual"] += $estructura["abril_inicial"];
        $estructura["total_anual"] += $estructura["mayo_inicial"];
        $estructura["total_anual"] += $estructura["junio_inicial"];
        $estructura["total_anual"] += $estructura["julio_inicial"];
        $estructura["total_anual"] += $estructura["agosto_inicial"];
        $estructura["total_anual"] += $estructura["septiembre_inicial"];
        $estructura["total_anual"] += $estructura["octubre_inicial"];
        $estructura["total_anual"] += $estructura["noviembre_inicial"];
        $estructura["total_anual"] += $estructura["diciembre_inicial"];

//            Esta es la parte para tomar todas las adecuaciones en firme que afectan a la partida
        $sql_adecuaciones = "SELECT md.mes_destino, md.total, md.texto
                            FROM mov_adecuaciones_egresos_detalle md
                            JOIN mov_adecuaciones_egresos_caratula mc
                            ON md.numero_adecuacion = mc.numero
                            WHERE COLUMN_GET(md.estructura, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(md.estructura, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(md.estructura, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(md.estructura, 'capitulo' as char) = ?
                            AND COLUMN_GET(md.estructura, 'concepto' as char) = ?
                            AND COLUMN_GET(md.estructura, 'partida' as char) = ?
                            AND mc.enfirme = 1
                            AND cancelada = 0;";
        $query_adecuaciones = $this->db->query($sql_adecuaciones, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $resultado_adecuaciones = $query_adecuaciones->result_array();

        foreach($resultado_adecuaciones as $key => $value) {

            // Se revisa la operación que es
            if($value["texto"] == "Reducción") {
                // Si la operación es una reducción, primero se revisa que haya saldo disponible
                $check_mensual = $estructura[$value["mes_destino"]."_saldo_precompromiso"] - $value["total"];
                // En caso de que la operación sea menor a cero, quiere decir que no hay dinero disponible
                if($check_mensual < 0) {
                    $this->debugeo->imprimir_pre('No hay suficiencia presupuestaria en el mes de '.ucfirst($value["mes_destino"]).' para la reducción en la partida '.
                        $estructura["fuente_de_financiamiento"].' '.
                        $estructura["programa_de_financiamiento"].' '.
                        $estructura["centro_de_costos"].' '.
                        $estructura["capitulo"].' '.
                        $estructura["concepto"].' '.
                        $estructura["partida"].'');
                }
                // De lo contrario, se realiza la operación
                $estructura[$value["mes_destino"]."_saldo_precompromiso"] -= $value["total"];
                $estructura[$value["mes_destino"]."_saldo"] -= $value["total"];
                if($estructura[$value["mes_destino"]."_modificado"] != 0) {
                    $estructura[$value["mes_destino"]."_modificado"] -= $value["total"];
                } else {
                    $estructura[$value["mes_destino"]."_modificado"] = number_format($estructura[$value["mes_destino"]."_inicial"] - $value["total"], 2, '.', '');
                }
                $estructura["modificado_anual"] -= $value["total"];
                $estructura["total_anual"] -= $value["total"];

            } elseif($value["texto"] == "Ampliación") {
                $estructura[$value["mes_destino"]."_saldo_precompromiso"] += $value["total"];
                $estructura[$value["mes_destino"]."_saldo"] += $value["total"];
                if($estructura[$value["mes_destino"]."_modificado"] != 0) {
                    $estructura[$value["mes_destino"]."_modificado"] += $value["total"];
                } else {
                    $estructura[$value["mes_destino"]."_modificado"] = number_format($estructura[$value["mes_destino"]."_inicial"] + $value["total"], 2, '.', '');
                }
                $estructura["modificado_anual"] += $value["total"];
                $estructura["total_anual"] += $value["total"];
            }

        }

        unset($key);
        unset($value);

        // Esta es la parte donde se toman todos los precompromisos en firme que afectan a la partida
        $sql_precompromiso = "SELECT mpc.numero_pre, mpc.fecha_emision, mpd.*
                            FROM mov_precompromiso_detalle mpd
                            JOIN mov_precompromiso_caratula mpc
                            ON mpd.numero_pre = mpc.numero_pre
                            WHERE COLUMN_GET(mpd.nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'partida' as char) = ?
                            AND mpc.enfirme = 1
                            AND mpc.firma1 = 1
                            AND mpc.firma2 = 1
                            AND mpc.firma3 = 1
                            AND mpc.cancelada = 0;";
        $query_precompromiso = $this->db->query($sql_precompromiso, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $resultado_precompromiso = $query_precompromiso->result_array();

        foreach($resultado_precompromiso as $key => $value) {
            // $this->debugeo->imprimir_pre($valor);
            
            $query_reset_presupuesto = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_CREATE('enero', 0.0, 'febrero', 0.0, 'marzo', 0.0, 'abril', 0.0, 'mayo', 0.0, 'junio', 0.0, 'julio', 0.0, 'agosto', 0.0, 'septiembre', 0.0, 'octubre', 0.0, 'noviembre', 0.0, 'diciembre', 0.0 ) WHERE id_precompromiso_detalle = ?;";
            $this->db->query($query_reset_presupuesto, array($value["id_precompromiso_detalle"]));

            $query_devolver_presupuesto = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_ADD( presupuesto_tomado,";

            $precomprometido_total = 0;

            // Se realiza un ciclo para poder recorrer todos los meses del año
            for ($i = 1; $i <= 12; $i++) {
                // Se toma el año en curso
                $year = date("Y");

                // Se transforma el numero del mes a texto
                $fecha_interna = $this->utilerias->convertirFechaAMes($year."-".$i."-01");

                // Se toma la cantidad precomprometida y se suma a la variable que tiene la suma anual
                $precomprometido_total += $estructura[$fecha_interna."_precompromiso"];

            }

            // Se suma el precomprometido anual, mas el importe que se desea precomprometer en una nueva variable
            $check_anual = $precomprometido_total + $value["importe"];

            if($check_anual <= $estructura["total_anual"]) {

                $operacion = $value["importe"];

                // Se realiza un ciclo para poder recorrer todos los meses del año
                for ($i = 1; $i <= 12; $i++) {
                    // Se toma el año en curso
                    $year = date("Y");

                    // Se transforma el numero del mes a texto
                    $fecha_interna = $this->utilerias->convertirFechaAMes($year."-".$i."-01");

                    // Si no hay saldo en el mes
                    if($estructura[$fecha_interna."_saldo_precompromiso"] > 0) {

                        $operacion = $estructura[$fecha_interna."_saldo_precompromiso"] - $operacion;

                        $operacion = number_format($operacion, 2, '.', '');

                        if($operacion <= 0) {
                            
                            // Aqui se tiene que poner el mes_saldo en 0
                            $operacion = abs($operacion);

                            // Se agrega el saldo al precomprometido del mes
                            $estructura[$fecha_interna."_precompromiso"] += number_format($estructura[$fecha_interna."_saldo_precompromiso"], 2, '.', '');

                            // Se agrea el saldo al precomprometido anual
                            $estructura["precomprometido_anual"] += number_format($estructura[$fecha_interna."_saldo_precompromiso"], 2, '.', '');

                            // Aqui se tiene que agregar un arreglo con el mes de donde se tomó el presupuesto y el saldo
                            $query_devolver_presupuesto .= " '".$fecha_interna."', '".number_format($estructura[$fecha_interna."_saldo_precompromiso"], 2, '.', '')."',";

                            // Se pone el saldo del precompromiso en 0
                            $estructura[$fecha_interna."_saldo_precompromiso"] = 0;

                            if($operacion == 0) {
                                break;
                            }
                            
                        } elseif($operacion > 0) {

                            // $this->debugeo->imprimir_pre($operacion);

                            // Se pone la cantidad final en el mes precomprometido
                            $estructura[$fecha_interna."_precompromiso"] += number_format(abs($estructura[$fecha_interna."_saldo_precompromiso"] - $operacion), 2, '.', '');

                            // Se agrega la cantidad al precomprometido anual
                            $estructura["precomprometido_anual"] += number_format(abs($estructura[$fecha_interna."_saldo_precompromiso"] - $operacion), 2, '.', '');

                            // Aqui se tiene que agregar un arreglo con el mes de donde se tomó el presupuesto y el saldo
                            $query_devolver_presupuesto .= " '".$fecha_interna."', '".number_format(abs($estructura[$fecha_interna."_saldo_precompromiso"] - $operacion), 2, '.', '')."',";

                            // Se pone el saldo del precompromiso al saldo
                            $estructura[$fecha_interna."_saldo_precompromiso"] -= number_format(abs($estructura[$fecha_interna."_saldo_precompromiso"] - $operacion), 2, '.', '');
                            
                            break;
                        }
                    } else {
                        $query_devolver_presupuesto .= " '".$fecha_interna."', 0.0,";
                    }

                }

            } else {
                // Mandar error
                $this->debugeo->imprimir_pre('La cantidad en la estructura '.$estructura["fuente_de_financiamiento"].' '
                    .$estructura["programa_de_financiamiento"].' '
                    .$estructura["centro_de_costos"].' '
                    .$estructura["capitulo"].' '
                    .$estructura["concepto"].' '
                    .$estructura["partida"].' '
                    .' para precomprometer supera el presupuesto aprobado: '.$value["numero_pre"]);
                // $this->debugeo->imprimir_pre($estructura);
            }

            // Se quita la última coma del query
            $query_devolver_presupuesto = substr($query_devolver_presupuesto, 0, -1);

            // Se agrega la condicional al query para que actualize el presupuesto de donde fue tomado
            $query_devolver_presupuesto .= " ) WHERE id_precompromiso_detalle = ".$value["id_precompromiso_detalle"].";";
            
            // Se actualiza la partida
            $this->ciclo_model->actualizar_partida_precompromiso($estructura);

            // Se actualiza el campo del presupuesto tomado
            $this->ciclo_model->actualizar_presupuesto_tomado_precompromiso($query_devolver_presupuesto);
           
        }

        unset($key);
        unset($value);

        // Se verifica si existen compromisos asociados al precompromiso
        $sql_compromiso = "SELECT mcc.num_precompromiso, mcc.numero_compromiso, mcc.fecha_emision, mcd.*
                            FROM mov_compromiso_detalle mcd
                            JOIN mov_compromiso_caratula mcc
                            ON mcd.numero_compromiso = mcc.numero_compromiso
                            WHERE COLUMN_GET(mcd.nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'partida' as char) = ?
                            AND mcc.enfirme = 1
                            AND mcc.firma1 = 1
                            AND mcc.firma2 = 1
                            AND mcc.firma3 = 1
                            AND mcc.cancelada = 0;";
        $query_compromiso = $this->db->query($sql_compromiso, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $resultado_compromiso = $query_compromiso->result_array();

        foreach($resultado_compromiso as $key => $value) {
            // $this->debugeo->imprimir_pre($valor_compro);
            
            $meses_presupuesto_tomado_precompromiso = array(
                'enero' => 0.0,
                'febrero' => 0.0,
                'marzo' => 0.0,
                'abril' => 0.0,
                'mayo' => 0.0,
                'junio' => 0.0,
                'julio' => 0.0,
                'agosto' => 0.0,
                'septiembre' => 0.0,
                'octubre' => 0.0,
                'noviembre' => 0.0,
                'diciembre' => 0.0,
            );

            $meses_presupuesto_tomado_compromiso = array(
                'enero' => 0.0,
                'febrero' => 0.0,
                'marzo' => 0.0,
                'abril' => 0.0,
                'mayo' => 0.0,
                'junio' => 0.0,
                'julio' => 0.0,
                'agosto' => 0.0,
                'septiembre' => 0.0,
                'octubre' => 0.0,
                'noviembre' => 0.0,
                'diciembre' => 0.0,
            );

            $query_reset_presupuesto = "UPDATE mov_compromiso_detalle SET presupuesto_tomado = COLUMN_CREATE('enero', 0.0, 'febrero', 0.0, 'marzo', 0.0, 'abril', 0.0, 'mayo', 0.0, 'junio', 0.0, 'julio', 0.0, 'agosto', 0.0, 'septiembre', 0.0, 'octubre', 0.0, 'noviembre', 0.0, 'diciembre', 0.0 ) WHERE id_compromiso_detalle = ?;";
            $this->db->query($query_reset_presupuesto, array($value["id_compromiso_detalle"]));

            // Se toma el mes de donde se quiere comprometer el dinero
            $fecha_emitido = $this->utilerias->convertirFechaAMes($value["fecha_emision"]);
            
            // Se suma el importe al comprometido anual
            $estructura["comprometido_anual"] += $value["importe"];

            // Se suma el importe al mes comprometido
            $estructura[$fecha_emitido."_compromiso"] += $value["importe"];

            // Se resta el importe al total anual
            $estructura["total_anual"] -= $value["importe"];

            // Se resta el importe al disponible mensual
            $estructura[$fecha_emitido."_saldo"] -= $value["importe"];

            if($value["id_precompromiso_detalle"] != 0) {

                // $this->debugeo->imprimir_pre($value);

                // Se genera el query que toma el presupuesto tomado del ID detalle del precompromiso
                $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado', FALSE)
                            ->from('mov_precompromiso_detalle')
                            ->where('id_precompromiso_detalle', $value["id_precompromiso_detalle"]);
                $query_precompromiso_detalle = $this->db->get();
                $resultado_precompromiso_detalle = $query_precompromiso_detalle->row_array();

                // Se guarda el resultado del query en una variable y se transforma el JSON en un arreglo
                $presupuesto_tomado_precompromiso = json_decode($resultado_precompromiso_detalle["presupuesto_tomado"], TRUE);

                for ($i = 1; $i <= 12; $i++) {
                    // Se toma el año en curso
                    $year = date("Y");

                    // Se transforma el numero del mes a texto
                    $fecha_interna_presupuesto = $this->utilerias->convertirFechaAMes($year."-".$i."-01");

                    $meses_presupuesto_tomado_precompromiso[$fecha_interna_presupuesto] = $presupuesto_tomado_precompromiso[$fecha_interna_presupuesto];

                }

                // if($estructura["fuente_de_financiamiento"] == "4" &&
                //     $estructura["programa_de_financiamiento"] == "E016" &&
                //     $estructura["centro_de_costos"] == "102" &&
                //     $estructura["capitulo"] == "3000" &&
                //     $estructura["concepto"] == "339" &&
                //     $estructura["partida"] == "33903") {
                    
                //     echo "Fecha Compromiso: ";
                //     $this->debugeo->imprimir_pre($fecha_emitido);
                //     echo "Presupuesto Tomado: ";
                //     $this->debugeo->imprimir_pre($presupuesto_tomado_precompromiso);
                //     echo "Importe: ";
                //     $this->debugeo->imprimir_pre($value["importe"]);

                // }

                $check_interno = $presupuesto_tomado_precompromiso[$fecha_emitido] - $value["importe"];

                if($presupuesto_tomado_precompromiso[$fecha_emitido] > 0 && $check_interno >= 0) {

                    $estructura[$fecha_emitido."_precompromiso"] -= $value["importe"];

                    $estructura["precomprometido_anual"] -= $value["importe"];

                    $presupuesto_tomado_precompromiso[$fecha_emitido] -= $value["importe"];

                    $meses_presupuesto_tomado_precompromiso[$fecha_emitido] = $presupuesto_tomado_precompromiso[$fecha_emitido];

                    $meses_presupuesto_tomado_compromiso[$fecha_emitido] = $value["importe"];

                } else {

                    if($fecha_emitido == "febrero") {

                        if($presupuesto_tomado_precompromiso["enero"] > 0) {

                            $estructura["enero_precompromiso"] -= $value["importe"];

                            $estructura["precomprometido_anual"] -= $value["importe"];

                            $presupuesto_tomado_precompromiso["enero"] -= $value["importe"];
                            
                            $meses_presupuesto_tomado_precompromiso["enero"] = $presupuesto_tomado_precompromiso["enero"];

                            $meses_presupuesto_tomado_compromiso["enero"] = $value["importe"];

                        } else {

                        }

                    } elseif($fecha_emitido == "enero") {

                        if($presupuesto_tomado_precompromiso["febrero"] > 0 ) {

                            $estructura["febrero_precompromiso"] -= $value["importe"];

                            $estructura["precomprometido_anual"] -= $value["importe"];

                            $presupuesto_tomado_precompromiso["febrero"] -= $value["importe"];
                            
                            $meses_presupuesto_tomado_precompromiso["febrero"] = $presupuesto_tomado_precompromiso["febrero"];

                            $meses_presupuesto_tomado_compromiso["febrero"] = $value["importe"];

                        }

                    }
                    
                }

                if($estructura["enero_precompromiso"] < 0 || $estructura["febrero_precompromiso"] < 0) {
                    if($estructura["enero_precompromiso"] < 0) {
                        $estructura["febrero_precompromiso"] -= $estructura["enero_precompromiso"];
                        $estructura["enero_precompromiso"] = 0;
                    } elseif($estructura["febrero_precompromiso"] < 0) {
                        $estructura["enero_precompromiso"] -= $estructura["febrero_precompromiso"];
                        $estructura["febrero_precompromiso"] = 0;
                    }
                    echo "Precompromiso Enero: ";
                    $this->debugeo->imprimir_pre($estructura["enero_precompromiso"]);
                    echo "Precompromiso Febrero: ";
                    $this->debugeo->imprimir_pre($estructura["febrero_precompromiso"]);

                    
                }

                // if($presupuesto_tomado_precompromiso["enero"] < 0 || $presupuesto_tomado_precompromiso["febrero"] < 0) {

                //     // if($presupuesto_tomado_precompromiso["enero"] < 0 && $value["id_precompromiso_detalle"] == 62 || $value["id_precompromiso_detalle"] == 129 || $value["id_precompromiso_detalle"] == 151) {
                        
                //     //     $presupuesto_tomado_precompromiso["febrero"] += $presupuesto_tomado_precompromiso["enero"];
                //     //     $meses_presupuesto_tomado_precompromiso["febrero"] += $meses_presupuesto_tomado_precompromiso["enero"];
                //     //     $estructura["enero_precompromiso"] += $presupuesto_tomado_precompromiso["enero"];
                //     //     $presupuesto_tomado_precompromiso["enero"] = 0;
                //     //     $meses_presupuesto_tomado_precompromiso["enero"] = 0;

                //     // } elseif($presupuesto_tomado_precompromiso["febrero"] < 0) {
                //     //     $presupuesto_tomado_precompromiso["enero"] += $presupuesto_tomado_precompromiso["febrero"];
                //     //     // $meses_presupuesto_tomado_precompromiso["enero"] += $meses_presupuesto_tomado_precompromiso["febrero"];
                //     // }
                    
                //     echo "Precompromiso Detalle: ";
                //     $this->debugeo->imprimir_pre($value["id_precompromiso_detalle"]);
                //     echo "Importe: ";
                //     $this->debugeo->imprimir_pre($value["importe"]);
                //     echo "Presupuesto Precomprometido Final: ";
                //     $this->debugeo->imprimir_pre($meses_presupuesto_tomado_precompromiso);
                //     echo "Presupuesto Comprometido Final: ";
                //     $this->debugeo->imprimir_pre($meses_presupuesto_tomado_compromiso);
                //     echo "Precompromiso Enero: ";
                //     $this->debugeo->imprimir_pre($estructura["enero_precompromiso"]);
                //     echo "Precompromiso Febrero: ";
                //     $this->debugeo->imprimir_pre($estructura["febrero_precompromiso"]);
                    
                // }
                
                $query_actualizar_presupuesto_tomado_precompromiso = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_CREATE('enero', ?, 'febrero', ?, 'marzo', ?, 'abril', ?, 'mayo', ?, 'junio', ?, 'julio', ?, 'agosto', ?, 'septiembre', ?, 'octubre', ?, 'noviembre', ?, 'diciembre', ? ) WHERE id_precompromiso_detalle = ?;";
                $this->db->query($query_actualizar_presupuesto_tomado_precompromiso, array(
                    $meses_presupuesto_tomado_precompromiso["enero"],
                    $meses_presupuesto_tomado_precompromiso["febrero"],
                    $meses_presupuesto_tomado_precompromiso["marzo"],
                    $meses_presupuesto_tomado_precompromiso["abril"],
                    $meses_presupuesto_tomado_precompromiso["mayo"],
                    $meses_presupuesto_tomado_precompromiso["junio"],
                    $meses_presupuesto_tomado_precompromiso["julio"],
                    $meses_presupuesto_tomado_precompromiso["agosto"],
                    $meses_presupuesto_tomado_precompromiso["septiembre"],
                    $meses_presupuesto_tomado_precompromiso["octubre"],
                    $meses_presupuesto_tomado_precompromiso["noviembre"],
                    $meses_presupuesto_tomado_precompromiso["diciembre"],
                    $value["id_precompromiso_detalle"],
                ));

                $query_actualizar_presupuesto_tomado_compromiso = "UPDATE mov_compromiso_detalle SET presupuesto_tomado = COLUMN_CREATE('enero', ?, 'febrero', ?, 'marzo', ?, 'abril', ?, 'mayo', ?, 'junio', ?, 'julio', ?, 'agosto', ?, 'septiembre', ?, 'octubre', ?, 'noviembre', ?, 'diciembre', ? ) WHERE id_compromiso_detalle = ?;";
                $this->db->query($query_actualizar_presupuesto_tomado_compromiso, array(
                    $meses_presupuesto_tomado_compromiso["enero"],
                    $meses_presupuesto_tomado_compromiso["febrero"],
                    $meses_presupuesto_tomado_compromiso["marzo"],
                    $meses_presupuesto_tomado_compromiso["abril"],
                    $meses_presupuesto_tomado_compromiso["mayo"],
                    $meses_presupuesto_tomado_compromiso["junio"],
                    $meses_presupuesto_tomado_compromiso["julio"],
                    $meses_presupuesto_tomado_compromiso["agosto"],
                    $meses_presupuesto_tomado_compromiso["septiembre"],
                    $meses_presupuesto_tomado_compromiso["octubre"],
                    $meses_presupuesto_tomado_compromiso["noviembre"],
                    $meses_presupuesto_tomado_compromiso["diciembre"],
                    $value["id_compromiso_detalle"],
                ));

            } else {

                // Se resta el importe al saldo disponible del mes
                $estructura[$fecha_emitido."_saldo_precompromiso"] -= $value["importe"];
            }

        }

        unset($key);
        unset($value);

        $this->ciclo_model->actualizar_partida_compromiso($estructura);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function fechas_adecuaciones_egresos() {
//        $this->benchmark->mark('code_start');

        $this->db->trans_start();

        $sql = "SELECT med.*
                FROM mov_adecuaciones_egresos_detalle med
                JOIN mov_adecuaciones_egresos_caratula mac
                WHERE mac.enfirme = 1
                AND mac.cancelada != 1
                AND med.mes_destino != 'Mes del que se tomará/agregará el presupuesto'
                GROUP BY med.id_adecuaciones_egresos_detalle;";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();
        foreach($resultado as $key => $value) {
            $fecha_aplicacion = explode("-", $value["fecha_aplicacion"]);
            $mes_destino = $this->mes_a_fecha($value["mes_destino"]);
            if($fecha_aplicacion[1] != $mes_destino) {
//                $this->debugeo->imprimir_pre($resultado[$key]);
//                $this->debugeo->imprimir_pre($value["mes_destino"]);
//                $this->debugeo->imprimir_pre($value["fecha_aplicacion"]);
                $resultado[$key]["fecha_aplicacion"] = $fecha_aplicacion[0]."-".$mes_destino."-".$fecha_aplicacion[2];
//                $this->debugeo->imprimir_pre($resultado[$key]);

                $datos_actualizar = array(
                    'fecha_aplicacion' => $resultado[$key]["fecha_aplicacion"],
                );

                $this->db->where('id_adecuaciones_egresos_detalle', $resultado[$key]["id_adecuaciones_egresos_detalle"]);
                $this->db->update('mov_adecuaciones_egresos_detalle', $datos_actualizar);

            }
//            $this->debugeo->imprimir_pre($mes_destino);
        }

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $this->db->trans_complete();

        log_message('info', 'Se ejecutó la función que acomoda las adecuaciones presupuestarias conforme a su mes.');
    }

    function mes_a_fecha($mes) {
        switch($mes) {
            case 'enero':
                $mes = 1;
                break;

            case 'febrero':
                $mes = 2;
                break;

            case 'marzo':
                $mes = 3;
                break;

            case 'abril':
                $mes = 4;
                break;

            case 'mayo':
                $mes = 5;
                break;

            case 'junio':
                $mes = 6;
                break;

            case 'julio':
                $mes = 7;
                break;

            case 'agosto':
                $mes = 8;
                break;

            case 'septiembre':
                $mes = 9;
                break;

            case 'octubre':
                $mes = 10;
                break;

            case 'noviembre':
                $mes = 11;
                break;

            case 'diciembre':
                $mes = 12;
                break;
        }

        return $mes;
    }

    function fecha_a_mes($mes) {
        switch($mes) {
            case 1:
                $mes = 'enero';
                break;

            case 2:
                $mes = 'febrero';
                break;

            case 3:
                $mes = 'marzo';
                break;

            case 4:
                $mes = 'abril';
                break;

            case 5:
                $mes = 'mayo';
                break;

            case 6:
                $mes = 'junio';
                break;

            case 7:
                $mes = 'julio';
                break;

            case 8:
                $mes = 'agosto';
                break;

            case 9:
                $mes = 'septiembre';
                break;

            case 10:
                $mes = 'octubre';
                break;

            case 11:
                $mes = 'noviembre';
                break;

            case 12:
                $mes = 'diciembre';
                break;
        }

        return $mes;
    }

    function checar_contrarecibos() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha checado los contrarecibos');
        $this->db->select('id_contrarecibo_caratula, numero_compromiso')->from('mov_contrarecibo_caratula')->where('numero_compromiso !=', '')->where('enfirme', 1)->where('cancelada', 0);
        $query = $this->db->get();
        $resultado = $query->result_array();
        foreach($resultado as $key => $value) {
            $this->db->select('numero_contrarecibo')->from('mov_contrarecibo_detalle')->where('numero_contrarecibo', $value["id_contrarecibo_caratula"]);
            $query_detalle = $this->db->get();
            $resultado_detalle = $query_detalle->row_array();
            if(!$resultado_detalle) {
//                $this->debugeo->imprimir_pre($value);
                $resultado = $this->ciclo_model->copiarDatosCompromiso($value["numero_compromiso"], $value["id_contrarecibo_caratula"]);
                if($resultado){
                    echo("Exito al copiar los datos del compromiso ".$value["numero_compromiso"].", al contrarecibo ".$value["id_contrarecibo_caratula"]);
                } else {
                    echo("Hubo un error al copiar los datos del compromiso ".$value["numero_compromiso"].", al contrarecibo ".$value["id_contrarecibo_caratula"]);
                }
            }
        }

        $this->db->trans_complete();
    }

    function resetar_devengado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach($result as $fila) {

            $estructura = json_decode($fila->estructura, TRUE);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'febrero_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'marzo_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'abril_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'mayo_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'junio_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'julio_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'agosto_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'septiembre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'octubre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'noviembre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'diciembre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'devengado_anual', 0)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $this->db->query($sql_actualizar, array(
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function resetar_modificado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach($result as $fila) {

            $estructura = json_decode($fila->estructura, TRUE);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'febrero_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'marzo_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'abril_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'mayo_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'junio_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'julio_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'agosto_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'septiembre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'octubre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'noviembre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'diciembre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'total_anual', 0)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $this->db->query($sql_actualizar, array(
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function resetar_recaudado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach($result as $fila) {
//            $this->debugeo->imprimir_pre($fila);

            $estructura = json_decode($fila->estructura, TRUE);

            $sql_actualizar = "UPDATE cat_niveles SET
                                nivel = COLUMN_ADD(nivel, 'enero_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'febrero_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'marzo_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'abril_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'mayo_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'junio_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'julio_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'agosto_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'septiembre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'octubre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'noviembre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'diciembre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'recaudado_anual', 0)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $this->db->query($sql_actualizar, array(
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function acomodar_saldos_precomprommiso_compromiso() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        $data = array(
            'total_compromiso' => 0,
        );

        $this->db->update('mov_precompromiso_caratula', $data);

        $sql = "SELECT mpc.numero_pre, mpc.total, mpc.total_compromiso, mcd.numero_compromiso, mcd.importe
                    FROM mov_precompromiso_caratula mpc
                        JOIN mov_compromiso_caratula mcc
                            ON mpc.numero_pre = mcc.num_precompromiso
                        JOIN mov_compromiso_detalle mcd
                            ON mcc.numero_compromiso = mcd.numero_compromiso
                    WHERE mpc.enfirme = 1
                        AND mpc.firma1 = 1
                        AND mpc.firma2 = 1
                        AND mpc.firma3 = 1
                        AND mpc.cancelada != 1
                        AND mcc.enfirme = 1
                        AND mcc.firma1 = 1
                        AND mcc.firma2 = 1
                        AND mcc.firma3 = 1
                        AND mcc.cancelada != 1";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $key => $value) {
            $this->db->select('total_compromiso')->from('mov_precompromiso_caratula')->where('numero_pre', $value["numero_pre"]);
            $query_precompro = $this->db->get();
            $importe = $query_precompro->row_array();

//            $this->debugeo->imprimir_pre($importe["total_compromiso"]);

            $data = array(
                'total_compromiso' => $importe["total_compromiso"] + $value["importe"],
            );

            $this->db->where('numero_pre', $value["numero_pre"]);
            $this->db->update('mov_precompromiso_caratula', $data);
        }

        $this->db->trans_complete();
    }

    function acomodar_saldos_devengado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT mdd.*, COLUMN_JSON(nivel) AS estructura, mdc.fecha_solicitud AS fecha
                FROM mov_devengado_detalle mdd
                JOIN mov_devengado_caratula mdc
                ON mdd.numero_devengado = mdc.numero
                WHERE mdc.enfirme = 1;";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $fila){
            $sql_estructura = "SELECT COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE id_niveles = ?;";
            $query_estructura = $this->db->query($sql_estructura, array($fila["id_nivel"]));
            $fila_estructura = $query_estructura->row_array();
            $estructura = json_decode($fila_estructura["estructura"], TRUE);

            $fecha = explode("-",$fila["fecha"]);
            $mes_destino = $this->fecha_a_mes($fecha[1]);

//            $this->debugeo->imprimir_pre($fila);
            $devengado_mes = $estructura[$mes_destino."_devengado"] + $fila["total_importe"];
            $devengado_anual = $estructura["devengado_anual"] + $fila["total_importe"];

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_destino."_devengado', ?),
                                nivel = COLUMN_ADD(nivel, 'devengado_anual', ?)
                                WHERE id_niveles = ?;";
            $resultado_actualizar = $this->db->query($sql_actualizar, array(
                number_format($devengado_mes, 2, '.', ''),
                number_format($devengado_anual, 2, '.', ''),
                $fila["id_nivel"],
            ));

//            $this->debugeo->imprimir_pre($resultado_actualizar);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function acomodar_saldos_recaudado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT mrc.fecha_solicitud AS fecha, mrd.total_importe, COLUMN_JSON(mrd.nivel) AS estructura
                FROM mov_recaudado_detalle mrd
                JOIN mov_recaudado_caratula mrc
                ON mrd.numero_recaudado = mrc.numero
                WHERE mrc.enfirme = 1
                AND mrc.cancelada = 0;";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $key => $value){
//            $this->debugeo->imprimir_pre($value);

            $estructura_inicial = json_decode($value["estructura"], TRUE);
            $sql_estructura = "SELECT COLUMN_JSON(nivel) AS estructura
                                FROM cat_niveles
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $query_estructura = $this->db->query($sql_estructura, array(
                $estructura_inicial["gerencia"],
                $estructura_inicial["centro_de_recaudacion"],
                $estructura_inicial["rubro"],
                $estructura_inicial["tipo"],
                $estructura_inicial["clase"],
                ));

            $fila_estructura = $query_estructura->row_array();

            $estructura = json_decode($fila_estructura["estructura"], TRUE);

            $fecha = explode("-", $value["fecha"]);
            $mes_destino = $this->fecha_a_mes($fecha[1]);

//            $this->debugeo->imprimir_pre($estructura[$mes_destino."_recaudado"]);
//            $this->debugeo->imprimir_pre($estructura["recaudado_anual"]);

            $recaudado_mes = $estructura[$mes_destino."_recaudado"] + $value["total_importe"];
            $recaudado_anual = $estructura["recaudado_anual"] + $value["total_importe"];

//            $this->debugeo->imprimir_pre($recaudado_mes);
//            $this->debugeo->imprimir_pre($recaudado_anual);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_destino."_recaudado', ?),
                                nivel = COLUMN_ADD(nivel, 'recaudado_anual', ?)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $resultado_actualizar = $this->db->query($sql_actualizar, array(
                number_format($recaudado_mes, 2, '.', ''),
                number_format($recaudado_anual, 2, '.', ''),
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));

        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function tomar_facturas() {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_begin();

        $DB1 = $this->load->database('pedidos', TRUE);

        $sql_facturas = "SELECT ssscta1, fecha, folio, modulo,
                            no_mov, edocfd, efecto,
                            cant AS cantidad, impbru AS importe, descto AS descuento,
                            iva, total, noclie,
                            receprfc, recepnombre
                            FROM fe_facturas
                            LEFT JOIN fe_claves
                            ON (fe_facturas.modulo=fe_claves.libreria)
                            WHERE fecha >= DATE(NOW())
                            AND efecto = 'I'
                            AND ssscta1 <> ''
                            AND ssscta1 <> '103270'
                            AND ssscta1 <> '103126';";

        $query_factura = $DB1->query($sql_facturas);

        $resultados_facturas = $query_factura->result_array();

        $conteo = 0;

        $total_facturas = 1;

        try {

            foreach($resultados_facturas as $key => $value) {

                // $this->debugeo->imprimir_pre($value);

                $sql_check_no_movimiento = "SELECT numero 
                                            FROM mov_devengado_caratula
                                            WHERE no_movimiento = ?;";
                $query_check_no_movimiento = $this->db->query($sql_check_no_movimiento, array($value["folio"]));
                $check_no_movimiento = $query_check_no_movimiento->row_array();

                if($check_no_movimiento) {
                    $this->debugeo->imprimir_pre("Factura [".$value["folio"]."] repetida");
                    $total_facturas += 1;
                    continue;
                }

                $gerencia = explode(".", $value["ssscta1"]);

                $conteo += $value["importe"];

                $last = 0;

                //Se toma el numero del ultimo devengado
                $ultimo = $this->recaudacion_model->ultimo_devengado();

                if($ultimo) {
                    $last = $ultimo->ultimo + 1;
                }
                else {
                    $last = 1;
                }

                $sql_centro_recaudacion = "SELECT COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                                            FROM cat_niveles
                                            WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                            GROUP BY COLUMN_GET(nivel, 'centro_de_recaudacion' as char);";
                $query_centro_recaudacion = $this->db->query($sql_centro_recaudacion, array($value["ssscta1"]));
                $nombre_centro_de_recaudacion = $query_centro_recaudacion->row_array();

                $value["ultimo"] = $last;
                $value["fecha_detalle"] = $value["fecha"];
                $value["subsidio"] = "Ingresos Propios";
                $value["descripcion_detalle"] = "Venta de la librería: ".$value["modulo"];
                $value["tipo_pago"] = "No Identificado";
                $value["nivel1"] = $gerencia[0];
                $value["nivel2"] = $value["ssscta1"];
                $value["nivel3"] = 7;
                $value["nivel4"] = 72;
                $value["nivel5"] = 72001;
                $value["clasificacion"] = "Productos y/o Bienes";
                $value["num_movimiento"] = $value["folio"];
                $value["clave_cliente"] = $value["ssscta1"];
                $value["cliente"] = $nombre_centro_de_recaudacion["descripcion"];
                $value["fecha_solicitud"] = $value["fecha"];
                $value["descripcion"] = "Venta de la librería: ".$value["modulo"];
                $value["estatus_recaudado"] = 0;

                $value["check_firme"] = 1;

                if($value["edocfd"] == 0) {
                    $value["estatus"] = "Cancelada";
                    $value["cancelada"] = 1;
                    $value["fecha_cancelada"] = $value["fecha"];
                } else {
                    $value["estatus"] = "Activo";
                    $value["cancelada"] = 0;
                    $value["fecha_cancelada"] = "0000-00-00";
                }

                $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();

                $nombres_ingresos = $this->recaudacion_model->obtener_nombre_niveles();

                $nombre = array();

                foreach($nombres_ingresos as $fila) {
                    array_push($nombre, $fila->descripcion);
                }

                $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

                for($i = 1; $i < $total_ingresos->conteo; $i++){
                    $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
                }

                $existe = $this->recaudacion_model->existeEstructura(array(
                    'nivel1' => $gerencia[0],
                    'nivel2' => $value["ssscta1"],
                    'nivel3' => 7,
                    'nivel4' => 72,
                    'nivel5' => 72001,
                ), $query);

                if(!isset($existe->id_niveles)) {
                    throw new Exception('No existe la estructura ingresada del Centro de Recaudación '.$value["ssscta1"]);
                }

                $this->recaudacion_model->apartarDevengado($last);

                $value["id_nivel"] = $existe->id_niveles;

                $value["total_importe"] = number_format( ($value["importe"] - $value["descuento"]) + $value["iva"], 2, '.', '');
                $value["importe_total"] = $value["total_importe"];

                $query_insertar = "INSERT INTO mov_devengado_detalle (numero_devengado, id_nivel, fecha_aplicacion, subsidio, cantidad, importe, descuento, iva, total_importe, descripcion_detalle, fecha_sql, hora_aplicacion, tipo_pago, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";

                for ($i = 0; $i < $total_ingresos->conteo; $i++) {
                    $query_insertar .= "'" . strtolower(str_replace(' ', '_', $nombre[$i])) . "', ?, ";
                }

                $query_insertar .= "'dato', ?));";

                $resultado_detalle = $this->recaudacion_model->insertar_detalle_devengado($value, $query_insertar);

                if ($resultado_detalle) {

                    $resultado_insertar_caratula = $this->recaudacion_model->insertar_caratula_devengado_automatico($value);

                    if($resultado_insertar_caratula) {
                        $this->recaudacion_model->marcarDevengado($value);

                        // $this->debugeo->imprimir_pre($value);

                        $this->debugeo->imprimir_pre("El devengado con folio ".$value["folio"]." se insertó correctamente.");

                        $this->db->trans_commit();

                        $total_facturas += 1;
                    } else {
                        throw new Exception('Hubo un error al insertar la caratula con folio '.$value["folio"]);
                    }

                } else {
                    throw new Exception('Hubo un error el detalle con folio '.$value["folio"]);
                }

            }

        } catch (Exception $e) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre($e->getMessage());
        }

        $this->debugeo->imprimir_pre($$total_facturas);

        $this->tomar_facturas_egresos();
        $this->tomar_facturas_canceladas();
        $this->ejecutar_funciones();

    }

    function tomar_facturas_egresos() {
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha tomado facturas');

        $DB1 = $this->load->database('pedidos', TRUE);

        $sql_facturas = "SELECT ssscta1, fecha, folio, modulo,
                            no_mov, edocfd, efecto,
                            cant AS cantidad, impbru AS importe, descto AS descuento,
                            iva, total, noclie,
                            receprfc, recepnombre, mov
                            FROM fe_facturas
                            LEFT JOIN fe_claves
                            ON (fe_facturas.modulo=fe_claves.libreria)
                            WHERE fecha >= DATE(NOW())
                            AND efecto = 'E'
                            AND ssscta1 <> ''
                            AND ssscta1 <> '103270'
                            AND ssscta1 <> '103126';";
        $query_factura = $DB1->query($sql_facturas);

        $resultados_facturas = $query_factura->result_array();

        foreach($resultados_facturas as $key => $value) {

            foreach ($value as $key_valores => $value_valores) {
                $value[$key_valores]=trim(utf8_encode($value_valores));
            }

            unset($key_valores);
            unset($value_valores);

            try {

                $this->db->trans_begin();

                $sql_check_no_movimiento = "SELECT id_factura 
                                            FROM facturas_adjuntas
                                            WHERE folio = ?;";
                $query_check_no_movimiento = $this->db->query($sql_check_no_movimiento, array($value["folio"]));
                $check_no_movimiento = $query_check_no_movimiento->row_array();

                if($check_no_movimiento) {
                    $this->debugeo->imprimir_pre("Factura [".$value["folio"]."] repetida de egresos");
                    $total_facturas += 1;
                    continue;
                }

                if($value["mov"] == "R" || $value["mov"] == "D" || $value["mov"] == "C") {
                    $this->utilerias->generar_poliza_facturas_egresos_directa($value);
                }

                if($value["no_mov"] == 0) {
                    $value["no_mov"] == "";
                } 

                $datos_insertar = array(
                    'no_mov' => $value["no_mov"],
                    'folio' => $value["folio"],
                    'modulo' => $value["modulo"],
                    'fecha' => $value["fecha"],
                    'impbru' => $value["importe"],
                    'descto' => $value["descuento"],
                    'iva' => $value["iva"],
                    'no_mov' => $value["no_mov"],
                    'total' => number_format( ($value["importe"] - $value["descuento"]) + $value["iva"], 2, '.', ''),
                    'efecto' => "E",
                    'mov' => $value["mov"],
                    'recepnombre' => $value["recepnombre"],
                    'receprfc' => $value["receprfc"],
                    'centro_recaudacion' => $value["ssscta1"],
                );

                $resultado_insertar = $this->db->insert('facturas_adjuntas', $datos_insertar);

                if ($resultado_insertar) {

                    $this->db->trans_commit();

                    $this->debugeo->imprimir_pre("El la factura de egresos con folio ".$value["folio"]." se insertó correctamente.");

                } else {
                    throw new Exception('Hubo un error al insertar la factura de egresos con folio '.$value["folio"]);
                }

            }
            catch (Exception $e) {
                $this->db->trans_rollback();
                $this->debugeo->imprimir_pre($e->getMessage());
            }

        }

    }

    function tomar_facturas_canceladas() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha tomado facturas canceladas');

        $this->db->trans_begin();

        $DB1 = $this->load->database('pedidos', TRUE);

        $sql_facturas = "SELECT ssscta1, fecha, folio, modulo,
                            no_mov
                            FROM fe_cancelados
                            LEFT JOIN fe_claves
                            ON (fe_cancelados.modulo=fe_claves.libreria)
                            WHERE fecha >= DATE(NOW())
                            AND ssscta1 <> ''
                            AND ssscta1 <> '103270'
                            AND ssscta1 <> '103126';";
        $query_factura = $DB1->query($sql_facturas);

        $resultados_facturas = $query_factura->result_array();

        foreach($resultados_facturas as $key => $value) {

            foreach ($value as $key_valores => $value_valores) {
                $value[$key_valores]=trim(utf8_encode($value_valores));
            }

            unset($key_valores);
            unset($value_valores);

            try {

                $sql_por_cancelar = "SELECT numero, fecha_solicitud FROM mov_devengado_caratula WHERE no_movimiento = ?;";
                $query_por_cancelar = $this->db->query($sql_por_cancelar, array($value["folio"]));
                $result_por_cancelar = $query_por_cancelar->row_array();

                if(isset($result_por_cancelar["numero"])) {
                    $sql_por_cancelar_detalle = "SELECT total_importe, COLUMN_JSON(nivel) AS estructura FROM mov_devengado_detalle WHERE numero_devengado = ?;";
                    $query_por_cancelar_detalle = $this->db->query($sql_por_cancelar_detalle, array($result_por_cancelar["numero"]));
                    $result_por_cancelar_detalle = $query_por_cancelar_detalle->result_array();

                    foreach ($result_por_cancelar_detalle as $key_interno => $value_interno) {
                        $estructura = json_decode($value_interno["estructura"], TRUE);

                        $resultado_cancelar_caratula = $this->recaudacion_model->insertar_caratula_devengado_automatico_cancelado($result_por_cancelar["numero"]);

                        if($resultado_cancelar_caratula) {
                            $this->recaudacion_model->desMarcarDevengado($estructura, $result_por_cancelar["fecha_solicitud"], $value_interno["total_importe"]);

                            $this->debugeo->imprimir_pre("El devengado con folio ".$value["folio"]." se canceló correctamente.");

                            $this->db->trans_commit();
                        } else {
                            throw new Exception('Hubo un error al cancelar la factura con folio '.$value["folio"]);
                        }

                    }
                    
                }

                
            }
            catch (Exception $e) {
               $this->db->trans_rollback();
               $this->debugeo->imprimir_pre($e->getMessage());
            }

        }

    }

    function acomodar_modificado_ingresos() {
        try {

//            $mes_anterior = date("m", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")));
            $mes_anterior = 2;
            $ultimo_dia_mes_anterior = cal_days_in_month(CAL_GREGORIAN, $mes_anterior, 2015);
            $mes_calcular = $this->fecha_a_mes($mes_anterior);
            $fecha = "2015-".$mes_anterior."-".$ultimo_dia_mes_anterior;

            $sql = "SELECT COLUMN_JSON(nivel) AS estructura
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != '';";
            $query = $this->db->query($sql);
            $result = $query->result_array();

            $this->db->trans_begin();

            foreach($result as $key => $value) {

                $estructura = json_decode($value["estructura"], TRUE);

                if($estructura[$mes_calcular . "_devengado"] == 0) {
                    continue;
                }

                if ($estructura[$mes_calcular . "_inicial"] > $estructura[$mes_calcular . "_devengado"]) {
//                    $this->debugeo->imprimir_pre("Reducción");
//                $this->debugeo->imprimir_pre("Cantidades Inicial [".$estructura[$mes_calcular."_inicial"]."] Devengado [".$estructura[$mes_calcular."_devengado"]."]");
                    $value["tipo"] = "Reducción";
                    $value["total_hidden"] = number_format($estructura[$mes_calcular . "_inicial"] - $estructura[$mes_calcular . "_devengado"], 2, '.', '');

                } elseif ($estructura[$mes_calcular . "_inicial"] < $estructura[$mes_calcular . "_devengado"]) {
//                    $this->debugeo->imprimir_pre("Ampliación");
//                $this->debugeo->imprimir_pre("Cantidades Inicial [".$estructura[$mes_calcular."_inicial"]."] Devengado [".$estructura[$mes_calcular."_devengado"]."]");
                    $value["tipo"] = "Ampliación";
                    $value["total_hidden"] = number_format($estructura[$mes_calcular . "_devengado"] - $estructura[$mes_calcular . "_inicial"], 2, '.', '');
                } elseif ($estructura[$mes_calcular . "_inicial"] == $estructura[$mes_calcular . "_devengado"]) {
                    continue;
                }

                $value["fecha"] = $fecha;
                $value["mes_destino"] = $mes_calcular;
                $value["cantidad"] = 1;

                $resultado_crear = $this->agregar_adecuacion($value);

                if ($resultado_crear == "exito") {
//                    $this->debugeo->imprimir_pre("La adecuación se agregó con éxito");
                } elseif ($resultado_crear == "error_caratula") {
                    throw new Exception('Hubo un erro al insertar los datos de la carátula en la adecuación de la estructura siguiente,  Gerencia ['.$estructura["gerencia"]."] Centro de Recaudación [".$estructura["centro_de_recaudacion"]."] Rubro [".$estructura["rubro"]."] Tipo [".$estructura["clase"]."] Clase [".$estructura["clase"]."] <br /> Del mes [".$mes_calcular."]");
                } elseif ($resultado_crear == "error_detalle") {
                    throw new Exception('Hubo un erro al insertar los datos del detalle en la adecuación de la estructura siguiente,  Gerencia ['.$estructura["gerencia"]."] Centro de Recaudación [".$estructura["centro_de_recaudacion"]."] Rubro [".$estructura["rubro"]."] Tipo [".$estructura["clase"]."] Clase [".$estructura["clase"]."] <br /> Del mes [".$mes_calcular."]");
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
            }
            else {
                $this->db->trans_commit();
                $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
            }

        } catch (Exception $e) {
            $this->debugeo->imprimir_pre($e->getMessage());
        }

    }

    private function agregar_adecuacion($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado adecuaciones');

//        $this->db->trans_begin();

        $estructura = json_decode($datos["estructura"], TRUE);

        $last = 0;

        $ultimo = $this->ingresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $datos["check_firme"] = 1;
        $datos["estatus"] = "activo";
        $datos["fecha_autoriza"] = $datos["fecha"];
        $datos["fecha_solicitud"] = $datos["fecha"];
        $datos["fecha_aplicar_caratula"] = $datos["fecha"];
        $datos["clasificacion"] = "Productos y/o Bienes";
        $datos["ultimo"] = $last;
        $datos["texto"] = $datos["tipo"];
        $datos["fecha_aplicar_detalle"] = $datos["fecha"];
        $datos["enlace"] = 0;
        $datos["nivel1_inferior"] = $estructura["gerencia"];
        $datos["nivel2_inferior"] = $estructura["centro_de_recaudacion"];
        $datos["nivel3_inferior"] = $estructura["rubro"];
        $datos["nivel4_inferior"] = $estructura["tipo"];
        $datos["nivel5_inferior"] = $estructura["clase"];
        $datos["tipo_radio"] = 1;
        $datos["descripcion"] = "Modificado del mes: ".ucfirst($datos["mes_destino"]);

//        $this->debugeo->imprimir_pre($datos);

        $this->ingresos_model->apartarAdecuacion($last, $datos["tipo"]);

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $resultado_insertar_detalle = $this->ingresos_model->insertarDetalle($datos);

        if($resultado_insertar_detalle) {
            $resultado_insertar_caratula = $this->ingresos_model->insertarCaratula($datos);
            if($resultado_insertar_caratula) {
//                $this->db->trans_commit();

//                $this->db->trans_begin();
                if($datos["tipo"] == "Ampliación") {
                    $this->actualizar_saldo_ampliacion($last);

//                    $this->db->trans_commit();
                } elseif($datos["tipo"] == "Reducción"){
                    $this->actualizar_saldo_reduccion($last);

//                    $this->db->trans_commit();
                }
                return "exito";
            } else {
//                $this->db->trans_rollback();
                return "error_caratula";
            }
        } else {
//            $this->db->trans_rollback();
            return "error_detalle";
        }

    }

    private function actualizar_saldo_ampliacion($id) {
//        $this->debugeo->imprimir_pre($id);
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado saldo de ampliación');
        $datos_caratula = $this->ingresos_model->get_datos_caratula_modificado($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula["numero"]);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($fila->mes_destino, $fila->id_nivel);

            if($saldo_mes["mes_saldo"] == 0) {
                $nuevo_saldo_mes = $saldo_mes["mes_inicial"] + $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] + $fila->total;
            } else {
                $nuevo_saldo_mes = $saldo_mes["mes_saldo"] + $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] + $fila->total;
            }

            $nuevo_saldo_mes -= 1;

//            Se genera el movimiento
            $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    private function actualizar_saldo_reduccion($id) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado saldo de reducción');
        $datos_caratula = $this->ingresos_model->get_datos_caratula_modificado($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula["numero"]);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($fila->mes_destino, $fila->id_nivel);

            if($saldo_mes["mes_saldo"] == 0) {
                $nuevo_saldo_mes = $saldo_mes["mes_inicial"] - $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] - $fila->total;
            } else {
                $nuevo_saldo_mes = $saldo_mes["mes_saldo"] - $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] - $fila->total;
            }

//            Se genera el movimiento
            $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    function registrar_devengado() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        $this->db->select('numero_compromiso, fecha_pago AS fecha, importe')->from('mov_contrarecibo_caratula')->where('enfirme', 1)->where('cancelada', 0)->where('numero_compromiso !=', 0);
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {
            $sql_consulta = "SELECT mcc.enfirme, mcc.fecha_emision, mcc.firma1, mcc.firma2, mcc.firma3, mcc.estatus, mcd.importe, COLUMN_JSON(mcd.nivel) AS estructura
                                FROM mov_compromiso_detalle mcd
                                JOIN mov_compromiso_caratula mcc
                                ON mcd.numero_compromiso = mcc.numero_compromiso
                                WHERE mcd.numero_compromiso = ?;";
            $query_consulta = $this->db->query($sql_consulta, array($value["numero_compromiso"]));
            $resultado_consulta = $query_consulta->result_array();

            foreach($resultado_consulta as $llave => $valor) {
                if($valor["enfirme"] == 1 && $valor["firma1"] == 1 && $valor["firma2"] == 1 && $valor["firma3"] == 1 && $valor["estatus"] == "activo") {
                    $estructura = json_decode($valor["estructura"], TRUE);

                    $mes_devengado = $this->utilerias->convertirFechaAMes($value["fecha"]);

                    $sql_nivel = "SELECT COLUMN_GET(nivel, '".$mes_devengado."_devengado' as char) AS mes_devengado,
                                        COLUMN_GET(nivel, 'devengado_anual' as char) AS devengado_anual
                                        FROM cat_niveles
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_nivel = $this->db->query($sql_nivel, array(
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                        ));
                    $resultado_nivel = $query_nivel->row_array();

                    $resultado_nivel["mes_devengado"] += number_format($valor["importe"], 2, '.', '');
                    $resultado_nivel["devengado_anual"] += number_format($valor["importe"], 2, '.', '');
//
//                    $this->debugeo->imprimir_pre($resultado_nivel);

                    $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_devengado."_devengado', ?),
                                        nivel = COLUMN_ADD(nivel, 'devengado_anual', ?)
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_actualizar = $this->db->query($sql_actualizar, array(
                        $resultado_nivel["mes_devengado"],
                        $resultado_nivel["devengado_anual"],
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));

//                    $this->debugeo->imprimir_pre($query_actualizar);
                }
            }

        }

        $this->db->trans_complete();

    }

    function registrar_ejercido() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha registrado un ejercicio');
//        $this->benchmark->mark('code_start');
        $total_ejercido = 0;

        $this->db->select('movimiento, fecha_emision AS fecha, contrarecibo')->from('mov_bancos_movimientos')->where('enfirme', 1)->where('cancelada =', 0)->where('t_movimiento', "presupuesto");
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {

            if(!isset($value["contrarecibo"]) || $value["contrarecibo"] == NULL || $value["contrarecibo"] == "") {
                $this->debugeo->imprimir_pre($value["movimiento"]);
                continue;
            }

            $sql_consulta = "SELECT numero_compromiso
                                FROM mov_contrarecibo_caratula
                                WHERE id_contrarecibo_caratula = ?;";
            $query_consulta = $this->db->query($sql_consulta, array($value["contrarecibo"]));
            $resultado_consulta = $query_consulta->row_array();

//            $this->debugeo->imprimir_pre($resultado_consulta);

            $sql_consulta_compromiso = "SELECT mcc.enfirme, mcc.fecha_emision, mcc.firma1, mcc.firma2, mcc.firma3, mcc.estatus, mcd.importe, COLUMN_JSON(mcd.nivel) AS estructura
                                FROM mov_compromiso_detalle mcd
                                JOIN mov_compromiso_caratula mcc
                                ON mcd.numero_compromiso = mcc.numero_compromiso
                                WHERE mcd.numero_compromiso = ?;";
            $query_consulta_compromiso = $this->db->query($sql_consulta_compromiso, array($resultado_consulta["numero_compromiso"]));
            $resultado_consulta_compromiso = $query_consulta_compromiso->result_array();

            foreach($resultado_consulta_compromiso as $llave => $valor) {

                if ($valor["enfirme"] == 1 && $valor["firma1"] == 1 && $valor["firma2"] == 1 && $valor["firma3"] == 1 && $valor["estatus"] == "activo") {
                    $estructura = json_decode($valor["estructura"], TRUE);
                    $mes_ejercido = $this->utilerias->convertirFechaAMes($value["fecha"]);

                    $sql_nivel = "SELECT COLUMN_GET(nivel, '" . $mes_ejercido . "_ejercido' as char) AS mes_ejercido,
                                        COLUMN_GET(nivel, 'ejercido_anual' as char) AS ejercido_anual
                                        FROM cat_niveles
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_nivel = $this->db->query($sql_nivel, array(
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                    $resultado_nivel = $query_nivel->row_array();

                    $resultado_nivel["mes_ejercido"] += number_format($valor["importe"], 2, '.', '');
                    $resultado_nivel["ejercido_anual"] += number_format($valor["importe"], 2, '.', '');

                    $total_ejercido += number_format($valor["importe"], 2, '.', '');
//
//                    $this->debugeo->imprimir_pre($resultado_nivel);

                    $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . $mes_ejercido . "_ejercido', ?),
                                        nivel = COLUMN_ADD(nivel, 'ejercido_anual', ?)
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_actualizar = $this->db->query($sql_actualizar, array(
                        $resultado_nivel["mes_ejercido"],
                        $resultado_nivel["ejercido_anual"],
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                }
            }
        }

        $this->db->trans_complete();

//        $this->debugeo->imprimir_pre($total_ejercido);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function registrar_pagado() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha registrado un ejercicio');
//        $this->benchmark->mark('code_start');
        $total_ejercido = 0;

        $this->db->select('movimiento, fecha_conciliado AS fecha, contrarecibo')->from('mov_bancos_movimientos')->where('enfirme', 1)->where('cancelada', 0)->where('fecha_conciliado !=', '');
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {

            if(!isset($value["contrarecibo"]) || $value["contrarecibo"] == NULL || $value["contrarecibo"] == "") {
                $this->debugeo->imprimir_pre($value["movimiento"]);
                continue;
            }

            $sql_consulta = "SELECT numero_compromiso
                                FROM mov_contrarecibo_caratula
                                WHERE id_contrarecibo_caratula = ?;";
            $query_consulta = $this->db->query($sql_consulta, array($value["contrarecibo"]));
            $resultado_consulta = $query_consulta->row_array();

//            $this->debugeo->imprimir_pre($resultado_consulta);

            $sql_consulta_compromiso = "SELECT mcc.enfirme, mcc.fecha_emision, mcc.firma1, mcc.firma2, mcc.firma3, mcc.estatus, mcd.importe, COLUMN_JSON(mcd.nivel) AS estructura
                                FROM mov_compromiso_detalle mcd
                                JOIN mov_compromiso_caratula mcc
                                ON mcd.numero_compromiso = mcc.numero_compromiso
                                WHERE mcd.numero_compromiso = ?;";
            $query_consulta_compromiso = $this->db->query($sql_consulta_compromiso, array($resultado_consulta["numero_compromiso"]));
            $resultado_consulta_compromiso = $query_consulta_compromiso->result_array();

            foreach($resultado_consulta_compromiso as $llave => $valor) {

                if ($valor["enfirme"] == 1 && $valor["firma1"] == 1 && $valor["firma2"] == 1 && $valor["firma3"] == 1 && $valor["estatus"] == "activo") {
                    $estructura = json_decode($valor["estructura"], TRUE);
                    $mes_ejercido = $this->utilerias->convertirFechaAMes($value["fecha"]);

                    $sql_nivel = "SELECT COLUMN_GET(nivel, '" . $mes_ejercido . "_pagado' as char) AS mes_ejercido,
                                        COLUMN_GET(nivel, 'pagado_anual' as char) AS ejercido_anual
                                        FROM cat_niveles
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_nivel = $this->db->query($sql_nivel, array(
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                    $resultado_nivel = $query_nivel->row_array();

                    $resultado_nivel["mes_ejercido"] += number_format($valor["importe"], 2, '.', '');
                    $resultado_nivel["ejercido_anual"] += number_format($valor["importe"], 2, '.', '');

                    $total_ejercido += number_format($valor["importe"], 2, '.', '');
//
//                    $this->debugeo->imprimir_pre($resultado_nivel);

                    $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . $mes_ejercido . "_pagado', ?),
                                        nivel = COLUMN_ADD(nivel, 'pagado_anual', ?)
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_actualizar = $this->db->query($sql_actualizar, array(
                        $resultado_nivel["mes_ejercido"],
                        $resultado_nivel["ejercido_anual"],
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                }
            }
        }

        $this->db->trans_complete();

//        $this->debugeo->imprimir_pre($total_ejercido);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function acomodar_contrarecibos_movimientos_bancarios() {

        $this->db->select('movimiento, numero')->from('mov_bancos_pagos_contrarecibos');
        $query = $this->db->get();
        foreach($query->result_array() as $key => $value) {
//            $this->debugeo->imprimir_pre($value);

            $datos_actualizar = array(
                'contrarecibo' => $value["numero"],
            );

            $this->db->where('movimiento', $value["movimiento"]);
            $this->db->update('mov_bancos_movimientos', $datos_actualizar);
        }
    }

    function revisar_cuentas_existen() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->select('cuenta')->from('checar_cuentas_existen');
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach($resultado as $key => $value) {
            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', $value["cuenta"]);
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();
            if(!$existe_cuenta) {
                $datos_insertar = array(
                    'cuenta_no_existe' => $value["cuenta"],
                );

                $this->db->insert('cuentas_no_existen', $datos_insertar);
                $this->debugeo->imprimir_pre("La cuenta: ".$value["cuenta"]." no existe dentro del plan de cuentas");
            }
        }

    }

    function generar_polizas_egresos($datos = NULL) {
        $this->db->select('*')->from('facturas_adjuntas')->where('mov !=', '');
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach($resultado as $key => $value) {
            $this->debugeo->imprimir_pre($value);
            $value["descuento"] = $value["descto"];
            $value["ssscta1"] = $value["centro_recaudacion"];
            $poliza = $this->utilerias->generar_poliza_facturas_egresos_directa($value);
            $this->debugeo->imprimir_pre($poliza);
        }
    }

    function generar_conciliacion_por_fechas() {
        $fecha_inicial = "2015-01-01";
        $fecha_final = "2015-07-31";

        $this->db->select('fecha_pago AS fecha, movimiento')
            ->from('mov_bancos_movimientos')
            ->where('fecha_pago >=', $fecha_inicial)
            ->where('fecha_pago <=', $fecha_final)
            ->where('enfirme', 1)
            ->where('cancelada', 0);

        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach($resultado as $key => $value) {
            $this->db->trans_begin();

            $datos_actualizar = array(
                'sifirme' => 1,
                'fecha_conciliado' => $value["fecha"],
            );

            $this->db->where('movimiento', $value["movimiento"]);
            $this->db->update('mov_bancos_movimientos', $datos_actualizar);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->debugeo->imprimir_pre("Hubo un error en la transaccion con el movimiento ".$value["movimiento"]);
            }
            else {
                $this->db->trans_commit();
                $this->debugeo->imprimir_pre("Se ha realizado con exito la transaccion del movimiento ".$value["movimiento"]);
            }
        }
    }

    function resetear_devengado_egresos($id_nivel = NULL) {
        $this->db->trans_start();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id_nivel));
        $resultado = $query->row_array();

        $estructura = json_decode($resultado["estructura"], TRUE);

        $estructura["enero_devengado"] = 0;
        $estructura["febrero_devengado"] = 0;
        $estructura["marzo_devengado"] = 0;
        $estructura["abril_devengado"] = 0;
        $estructura["mayo_devengado"] = 0;
        $estructura["junio_devengado"] = 0;
        $estructura["julio_devengado"] = 0;
        $estructura["agosto_devengado"] = 0;
        $estructura["septiembre_devengado"] = 0;
        $estructura["octubre_devengado"] = 0;
        $estructura["noviembre_devengado"] = 0;
        $estructura["diciembre_devengado"] = 0;
        $estructura["devengado_anual"] = 0;
        
        $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'devengado_anual', ?)
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(nivel, 'partida' as char) = ?;";
        $query_actualizar = $this->db->query($sql_actualizar, array(
            $estructura["enero_devengado"],
            $estructura["febrero_devengado"],
            $estructura["marzo_devengado"],
            $estructura["abril_devengado"],
            $estructura["mayo_devengado"],
            $estructura["junio_devengado"],
            $estructura["julio_devengado"],
            $estructura["agosto_devengado"],
            $estructura["septiembre_devengado"],
            $estructura["octubre_devengado"],
            $estructura["noviembre_devengado"],
            $estructura["diciembre_devengado"],
            $estructura["devengado_anual"],
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $this->db->trans_complete();
    }

    function corregir_presupuesto_tomado_compromisos() {
        $this->db->trans_begin();

        // Se toman los compromisos que esten en firme
        $sql = "SELECT mcc.fecha_emision AS fecha,
                    COLUMN_JSON(mcd.presupuesto_tomado) AS presupuesto_tomado,
                    mcd.importe,
                    mcd.id_compromiso_detalle AS ID,
                    mcd.numero_compromiso
                    FROM mov_compromiso_detalle mcd
                    JOIN mov_compromiso_caratula mcc
                    ON mcd.numero_compromiso = mcc.numero_compromiso
                    WHERE mcc.enfirme = 1
                    AND mcc.firma1 = 1
                    AND mcc.firma2 = 1
                    AND mcc.firma3 = 1
                    AND mcc.cancelada != 1;";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        // Se recorren cada uno de ellos
        foreach ($resultado as $key => $value) {
            // Se crea un arreglo que contiene los meses para poder ingresar el presupuesto tomado
            $meses = array(
                'enero' => 0.0,
                'febrero' => 0.0,
                'marzo' => 0.0,
                'abril' => 0.0,
                'mayo' => 0.0,
                'junio' => 0.0,
                'julio' => 0.0,
                'agosto' => 0.0,
                'septiembre' => 0.0,
                'octubre' => 0.0,
                'noviembre' => 0.0,
                'diciembre' => 0.0,
            );
            // Se toma la fecha en la que fueron generados
            $fecha = $this->utilerias->convertirFechaAMes($value["fecha"]);
            // Se acomoda el importe en el arreglo de los meses
            $meses[$fecha] = $value["importe"];
            // Se inserta el presupuesto tomado al id del compromiso que le corresponde
            $sql_actualizar = "UPDATE mov_compromiso_detalle
                                SET presupuesto_tomado =
                                COLUMN_CREATE(
                                'enero', ?,
                                'febrero', ?,
                                'marzo', ?,
                                'abril', ?,
                                'mayo', ?,
                                'junio', ?,
                                'julio', ?,
                                'agosto', ?,
                                'septiembre', ?,
                                'octubre', ?,
                                'noviembre', ?,
                                'diciembre', ?
                                )
                                WHERE id_compromiso_detalle = ?;";
            $this->db->query($sql_actualizar, array(
                                $meses["enero"],
                                $meses["febrero"],
                                $meses["marzo"],
                                $meses["abril"],
                                $meses["mayo"],
                                $meses["junio"],
                                $meses["julio"],
                                $meses["agosto"],
                                $meses["septiembre"],
                                $meses["octubre"],
                                $meses["noviembre"],
                                $meses["diciembre"],
                                $value["ID"],
                                ));
            
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error al generar la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al generar la transaccion");
        }

    }

}